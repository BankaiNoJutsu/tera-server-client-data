﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/CustomAskDialog.Master" AutoEventWireup="true" CodeBehind="ATO_AddVipStoreEvent.aspx.cs" Inherits="WebApp.VipSystem.ATO_AddVipStoreEvent" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="server">
    <form id="Form1" runat="server" class="nolineTable">
        <table class="lineTable">
            <tr>
                <th>
                    <%=WebApp.DisplayString.ServerName %>
                </th>
                <td>
                     <asp:Label ID="ServerName" runat="server" visible="true"/>
                </td>
            </tr>
        </table><br />
        
       <!-- 잘못 입력 된 값에 대한 안내 메세지  -->
       <p style="color:red; font-weight:bold"><asp:Label ID="ErrorInfo" runat="server" visible="true"/></p><br />

        <asp:Panel id="EditPanel" runat="server" visible="true">
            <table class="nolineTable">
                <tr>
                    <td>
                        <%=WebApp.DisplayString.EventPeriod %>
                    </td>
                    <td>
                         <asp:TextBox ID="StartTime" runat="server" visible="true"/> ~ <asp:TextBox ID="EndTime" runat="server" visible="true"/>
                    </td>
                </tr>
                <tr>
                    <td>
                        <%=WebApp.DisplayString.SlotId %>
                    </td>
                    <td>
                        <asp:DropDownList runat="server" ID="SlotIdList" DataTextField="Text" DataValueField="Value" EnableViewState="true" /> 
                    </td>
                </tr>
                <tr>
                    <td>
                        <%=WebApp.DisplayString.DiscountRate %>
                    </td>
                    <td>
                         <asp:TextBox ID="DiscountRate" runat="server" visible="true"/> (%)
                    </td>
                </tr>
            </table><br />
            <asp:TextBox runat="server" width="250px" height="100px" ID="MemoBox" TextMode="MultiLine" style="overflow:auto"/><br /><br />  
            <asp:Button runat="server" ID="ConfirmButton" text="할인 이벤트 추가" OnClick="ConfirmButton_Click" />
        </asp:Panel>

        <asp:Button runat="server" ID="CloseButton" text="닫기" OnClick="CloseButton_Click" Visible="false"/><br />
    </form>

</asp:Content>
