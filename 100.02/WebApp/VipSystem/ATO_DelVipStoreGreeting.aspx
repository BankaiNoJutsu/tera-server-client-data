﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/CustomAskDialog.Master" AutoEventWireup="true" CodeBehind="ATO_DelVipStoreGreeting.aspx.cs" Inherits="WebApp.VipSystem.ATO_DelVipStoreGreeting" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="server">
    <form id="Form1" runat="server" class="nolineTable">          
        <!-- 잘못 입력 된 값에 대한 안내 메세지  -->
        <p style="color:red; font-weight:bold"><asp:Label ID="ErrorInfo" runat="server" visible="true"/></p><br />
        <asp:Label ID="ServerName" runat="server" Visible="true"/><br />
        <asp:Label ID="GreetingBox" runat="server" Visible="true"/><br />
        <asp:Panel id="EditPanel" runat="server" visible="true">
            <!-- 서버 리스트 -->
            <asp:CheckBoxList ID="ServerChList" runat="server" DataTextField="Text" DataValueField="Value" EnableViewState="true" RepeatColumns="6"/> <br />
            <asp:TextBox runat="server" width="250px" height="100px" ID="MemoBox" TextMode="MultiLine" style="overflow:auto"/><br /><br />  
            <asp:Button runat="server" Text='<%# WebApp.DisplayString.DeleteEvent%>' ID="ConfirmButton" OnClick="ConfirmButton_Click" />
        </asp:Panel>

        <br /><asp:Button runat="server" ID="CloseButton" text="닫기" OnClick="CloseButton_Click" Visible="false"/><br />
    </form>

</asp:Content>