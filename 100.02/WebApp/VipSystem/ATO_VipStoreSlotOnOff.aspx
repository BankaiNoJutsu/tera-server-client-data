﻿<%@ Page MasterPageFile="~/AppCode/CustomAskDialog.Master" Language="C#" AutoEventWireup="true" CodeBehind="ATO_VipStoreSlotOnOff.aspx.cs" Inherits="WebApp.VipSystem.ATO_VipStoreSlotOnOff" %>

<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentBody" runat="server">
    <form id="Form1" runat="server" class="nolineTable">
        <table class="lineTable">
            <tr>
                <th>
                    <%=WebApp.DisplayString.ServerName %>
                </th>
                <td>
                     <asp:Label ID="ServerName" runat="server" visible="true"/>
                </td>
            </tr>
        </table><br />

       <!-- 잘못 입력 된 값에 대한 안내 메세지  -->
       <p style="color:red; font-weight:bold"><asp:Label ID="ErrorInfo" runat="server" visible="false"/></p><br />
       <asp:Panel id="EditPanel" runat="server" visible="true">
       <table class="lineTable">
            <tr>
                <th>
                    <%=WebApp.DisplayString.SlotId %>
                </th>
                <th>
                     <asp:Label ID="SlotId" runat="server" visible="true"/>
                </th>
            </tr>
            <tr>
               <td>
                   <%=WebApp.DisplayString.State %>    
               </td>
               <td>
                   <asp:Label ID="CurrentState" runat="server" visible="true"/>
               </td>
           </tr>
           <tr>
               <td>
                   <%=WebApp.DisplayString.ChangeState %>    
               </td>
               <td>
                   <asp:Label ID="ChangeState" runat="server" visible="true"/>
               </td>
           </tr>
        </table><br />

        <asp:TextBox runat="server" width="250px" height="100px" ID="MemoBox" TextMode="MultiLine" style="overflow:auto"/><br />
        <asp:Button runat="server" ID="ConfirmButton" text="" OnClick="ConfirmButton_Click" Visible="true"/><br />
        </asp:Panel>

        <asp:Button runat="server" ID="CloseButton" text="" OnClick="CloseButton_Click" Visible="false"/><br />
    </form>
</asp:Content>