﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/CustomAskDialog.Master" AutoEventWireup="true" CodeBehind="ATO_DelVipStoreEvent.aspx.cs" Inherits="WebApp.VipSystem.ATO_DelVipStoreEvent" %>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentBody" runat="server">
    <form id="Form1" runat="server" class="nolineTable">
        <table class="lineTable">
            <tr>
                <th>
                    <%=WebApp.DisplayString.ServerName %>
                </th>
                <td>
                     <asp:Label ID="ServerName" runat="server" visible="true"/>
                </td>
            </tr>
        </table><br />
        
        <asp:Label ID="EventInfo" runat="server" Visible="true"/><br /><br />

       <!-- 잘못 입력 된 값에 대한 안내 메세지  -->
       <p style="color:red; font-weight:bold"><asp:Label ID="ErrorInfo" runat="server" visible="true"/></p><br />

        <asp:Panel id="EditPanel" runat="server" visible="true">
            <asp:TextBox runat="server" width="250px" height="100px" ID="MemoBox" TextMode="MultiLine" style="overflow:auto"/><br /><br />  
            <asp:Button runat="server" Text='<%# WebApp.DisplayString.DeleteEvent%>' ID="ConfirmButton" OnClick="ConfirmButton_Click" />
        </asp:Panel>

        <asp:Button runat="server" ID="CloseButton" text="닫기" OnClick="CloseButton_Click" Visible="false"/><br />
    </form>

</asp:Content>
