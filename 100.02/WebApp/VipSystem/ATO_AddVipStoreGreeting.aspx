﻿<%@ Page MasterPageFile="~/AppCode/CustomAskDialog.Master"  Language="C#" AutoEventWireup="true" CodeBehind="ATO_AddVipStoreGreeting.aspx.cs" Inherits="WebApp.VipSystem.ATO_AddVipStoreGreeting" %>

<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentBody" runat="server">
    <form id="Form1" runat="server" class="nolineTable">   
        <!-- 잘못 입력 된 값에 대한 안내 메세지  -->
        <p style="color:red; font-weight:bold"><asp:Label ID="ErrorInfo" runat="server" visible="false"/></p><br />

        <asp:Panel id="EditPanel" runat="server" visible="true">
            <!-- 서버 리스트 -->
            <asp:CheckBoxList ID="ServerChList" runat="server" DataTextField="Text" DataValueField="Value" EnableViewState="true" RepeatColumns="6"/>
            <br /><br />
            <!-- 인사말 입력란 -->
            <p style="font-weight:bold">[<%= WebApp.DisplayString.Greeting %>]</p><br />
            <asp:TextBox runat="server" width="400px" height="200px" ID="GreetingBox" TextMode="MultiLine" style="overflow:auto"/><br /><br />

            <!-- 기간 -->
            <%=WebApp.DisplayString.EventPeriod %> <asp:TextBox ID="StartTime" runat="server" visible="true"/> ~ <asp:TextBox ID="EndTime" runat="server" visible="true"/><br/><br />
     
            <!-- 메모 입력란 -->
            <p style="font-weight:bold">[<%= WebApp.DisplayString.MemoStr %>]</p><br />
            <asp:TextBox runat="server" width="250px" height="100px" ID="MemoBox" TextMode="MultiLine" style="overflow:auto"/><br /><br />    

            <!-- 확인 버튼 -->
            <asp:Button runat="server" ID="ConfirmButton" text="" OnClick="ConfirmButton_Click" />
        </asp:Panel>

        <asp:Button runat="server" ID="CloseButton" text="닫기" OnClick="CloseButton_Click" Visible="false"/><br />
    </form>
</asp:Content>