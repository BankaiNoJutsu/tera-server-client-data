﻿<%@ Page MasterPageFile="~/AppCode/Operate.Master" EnableViewState="false" Language="C#" AutoEventWireup="true" CodeBehind="UserVipSystemInfo.aspx.cs" Inherits="WebApp.Users.UserVipSystemInfo" %>

<asp:Content ID="Content1" runat="server" contentplaceholderid="mainContentHolder">
    
    <asp:Panel runat="server" ID="CommonUserHeader" Visible="true"/> <br/>

    <asp:Panel runat="server" ID="InfoBody" Visible="false">
    <form runat="server" id ="resultForm" >

    <p style="font-weight:bold"> [<%= WebApp.DisplayString.VipSystemInfo%>] </p><br/>
    
    <table class="lineTable">
        <tr> 
            <th><%= WebApp.DisplayString.VipLevel%></th>
            <td><asp:Label ID="VipLevel" runat="server" /></td>
            <td style="border-style:hidden; border-left-style:inherit">&nbsp;<%= WebApp.DisplayString.VipLevelGuideToPublisher%></td>
        </tr>
        <tr>
            <th><%= WebApp.DisplayString.VipExp%></th>
            <td><asp:Label ID="VipExp" runat="server" /></td>
            <td style="border-style:hidden; border-left-style:inherit">&nbsp;<%= WebApp.DisplayString.VipExpGuideToPublisher%></td>
        </tr>
        <tr>
            <th><%= WebApp.DisplayString.VipGameExp%></th>
            <td><asp:Label ID="VipGameExp" runat="server" /></td>
            <td style="border-style:hidden; border-left-style:inherit">&nbsp;<asp:Button runat="server" Text="EDIT" ID="EditVipGameExp" OnClick="EditVipGameExp_Click" /></td>
        </tr>
    </table>
    <br/>
    
    <p style="font-weight:bold"> [<%= WebApp.DisplayString.VipStoreInfo%>] </p><br/>
     
    <table class="lineTable">
        <tr> 
            <th><%= WebApp.DisplayString.VipStoreResetCount%></th>
            <td><asp:Label ID="VipStoreResetCount" runat="server" /></td>
            <td style="border-style:hidden; border-left-style:inherit">&nbsp;<asp:Button runat="server" Text="EDIT" ID="EditVipStoreResetCount" OnClick="EditVipStoreResetCount_Click" /></td>
        </tr>
        <tr>
            <th><%= WebApp.DisplayString.VipTokenAmount%></th>
            <td><asp:Label ID="VipTokenAmount" runat="server" /></td>
            <td style="border-style:hidden; border-left-style:inherit">&nbsp;<asp:Button runat="server" Text="EDIT" ID="EditVipTokenAmount" OnClick="EditVipTokenAmount_Click" /></td>
        </tr>
    </table>

    </form >
    </asp:Panel>
</asp:Content>