﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_CancelServantAdventure.aspx.cs" Inherits="WebApp.Users.ATO_CancelServantAdventure" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">
    <p><%# WebApp.DisplayString.CancelAdventureAndWait %></p>
    <br/>
    <table class="lineTable">
        <tr>
            <th><%# WebApp.DisplayString.AdventureFieldName %></th>
            <th><%# WebApp.DisplayString.AdventureCompleteTime %></th>
        </tr>
        <tr>
            <td><asp:Label runat="server" ID="FieldName"/></td>
            <td><asp:Label runat="server" ID="CompleteTime"/></td>
        </tr>
    </table>
</asp:Content>
