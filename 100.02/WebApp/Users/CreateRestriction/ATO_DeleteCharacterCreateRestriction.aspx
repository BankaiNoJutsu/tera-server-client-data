﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/AppCode/DataSetAskDialog.master" CodeBehind="ATO_DeleteCharacterCreateRestriction.aspx.cs" Inherits="WebApp.Users.CreateRestriction.ATO_DeleteCharacterCreateRestriction" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
	<table class="lineTable width-full">
		<asp:Repeater runat="server" ID="RestrictionList">
			<HeaderTemplate>
				<tr>
					<th><%# WebApp.DisplayString.ServerName %></th>
					<th><%# WebApp.DisplayString.Class %></th>
					<th><%# WebApp.DisplayString.Race %></th>
					<th><%# WebApp.DisplayString.StartTime %></th>
				</tr>
			</HeaderTemplate>
			<ItemTemplate>
				<tr>
					<td><%# Eval("ServerName") %></td>
					<td><%# Eval("Class") %></td>
					<td><%# Eval("Race") %></td>
					<td><%# Eval("StartTime") %></td>
				</tr>
			</ItemTemplate>
		</asp:Repeater>
	</table>
</asp:Content>
