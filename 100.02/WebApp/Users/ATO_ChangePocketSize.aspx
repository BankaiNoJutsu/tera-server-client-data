﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_ChangePocketSize.aspx.cs" Inherits="WebApp.Users.ATO_ChangePocketSize" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">
    <table class="lineTable">
        <asp:Repeater runat="server" ID="InvenPocketList" OnItemCommand="InvenPocketList_OnItemCommand">
            <HeaderTemplate>
                <tr>
                    <th><%# WebApp.DisplayString.Type %></th>
                    <th><%# WebApp.DisplayString.SlotCount %></th>
                    <th><%# WebApp.DisplayString.RowCount %></th>
                    <th><%# WebApp.DisplayString.SelectPocketToExpand %></th>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><%# Eval("LabelString") %></td>
                    <td><%# Eval("SlotCount") %></td>
                    <td><%# Eval("RowCountString") %></td>
                    <td><asp:Button runat="server" Enabled=<%# Eval("CanExpand") %> Text="<%# WebApp.DisplayString.Selected %>" CommandName="SelectedTabIndex" CommandArgument='<%# Eval("TabIndex") %>' /></td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
    <asp:Label ID="CurrStatus" runat="server" Text="<%# WebApp.DisplayString.ChangePocketSizeDescription %>" /><br />
</asp:Content>
