﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_SetCardCollectionBookPoint.aspx.cs" Inherits="WebApp.Users.Card.ATO_SetCardCollectionBookPoint" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">

    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell><%# WebApp.DisplayString.CardCurrentCollectionBookPoint + ":" %></asp:TableCell>
            <asp:TableCell><asp:Label runat="server" ID="CurrentCollectionBookPoint" /></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><%# WebApp.DisplayString.CardCollectionBookPoint + ":" %></asp:TableCell>
            <asp:TableCell>
                <asp:TextBox runat="server" ID="CollectionBookPoint" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>

</asp:Content>
