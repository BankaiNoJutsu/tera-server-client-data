﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Users/Card/BaseCardPage.master" EnableEventValidation="false" AutoEventWireup="true" CodeBehind="SearchCardCollectionBook.aspx.cs" Inherits="WebApp.Users.Card.SearchCardCollectionBook" %>
<%@ MasterType VirtualPath="~/Users/Card/BaseCardPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SubPageContents" runat="server">

    <table class="lineTable width-full">

        <asp:HiddenField runat="server" ID="ServerNo" />
        <asp:HiddenField runat="server" ID="AccountDbId" />

        <asp:Repeater runat="server" ID="CardList" OnItemCommand="CardList_OnItemCommand">
            <HeaderTemplate>
                <tr>
                    <th><%# WebApp.DisplayString.CardProperty %></th>
                    <th><%# WebApp.DisplayString.CardName %></th>
                    <th><%# WebApp.DisplayString.CardLevel %></th>
                    <th><%# WebApp.DisplayString.CardCollectionStatus %></th>
                    <th></th>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><%# Eval("Property") %></td>
                    <td><%# Eval("Name") %></td>
                    <td><%# Eval("Level") %></td>
                    <td><%# string.Format("{0}/{1}", Eval("CollectionStatus"), Eval("NeedAmountForActivation")) %></td>
                    <td>
                        <asp:Button runat="server" CssClass="width-full" ID="SetCollectionStatusButton" Text="<%# WebApp.DisplayString.SET_CARD_AMOUNT %>"
                            CommandName="CollectionStatus" CommandArgument='<%# Eval("TemplateId") + "|" + Eval("CollectionStatus") %>' />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>

    </table>

</asp:Content>
