﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_SetCardCollectionBookLevel.aspx.cs" Inherits="WebApp.Users.Card.ATO_SetCardCollectionBookLevel" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">

    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell><%# WebApp.DisplayString.CardCurrentCollectionBookLevel + ":" %></asp:TableCell>
            <asp:TableCell><asp:Label runat="server" ID="CurrentCollectionBookLevel" /></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><%# WebApp.DisplayString.CardCollectionBookLevel + ":" %></asp:TableCell>
            <asp:TableCell>
                <asp:TextBox runat="server" ID="CollectionBookLevel" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>

</asp:Content>
