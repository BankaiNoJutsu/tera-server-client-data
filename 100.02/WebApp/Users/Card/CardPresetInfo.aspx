﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Users/Card/BaseCardPage.master" AutoEventWireup="true" CodeBehind="CardPresetInfo.aspx.cs" Inherits="WebApp.Users.Card.CardPresetInfo" %>
<%@ MasterType VirtualPath="~/Users/Card/BaseCardPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="SubPageContents" runat="server">

    <table class="lineTable width-full">

        <asp:Repeater runat="server" ID="CardPresetList">
            <HeaderTemplate>
                <tr>
                    <th><%# WebApp.DisplayString.PresetNumber %></th>
                    <th><%# WebApp.DisplayString.MountingCard %></th>
                    <th><%# WebApp.DisplayString.CardPassivity %></th>
                    <th><%# WebApp.DisplayString.CardCombine %></th>
                    <th><%# WebApp.DisplayString.CardCombinePassivity %></th>
                    <th><%# WebApp.DisplayString.CardCost %></th>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><%# Eval("PresetIndex") %></td>
                    <td><%# Eval("MountingCardList") %></td>
                    <td><%# Eval("PassivityList") %></td>
                    <td><%# Eval("CombineName") %></td>
                    <td><%# Eval("CombinePassivityList") %></td>
                    <td><%# Eval("CurrentCardCost") + "/" + Eval("MaxCardCost") %></td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>

    </table>

</asp:Content>
