﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_DungeonPhaseUserModify.aspx.cs" Inherits="WebApp.Users.ATO_DungeonPhaseUserModify" %>

<asp:Content runat="server" ID="content0" ContentPlaceHolderID="ContentHeader">
	<style type="text/css">
		div#charinfo {
			margin-bottom: 20px;
		}

		div#charinfo strong {
			display: inline;
		}

		div#charinfo ul {
			padding: 10px 0;
			list-style: none;
			display: inline;
		}

		div#charinfo ul li {
			display: inline;
			margin-right: 10px;
		}

		div#dungeon-data {
			margin: 10px 0;
		}

		div#dungeon-data div {
			margin-bottom: 5px;
		}
	</style>
</asp:Content>

<asp:Content runat="server" ID="content1" ContentPlaceHolderID="TERA_ATO">
	<div id="charinfo">
		<strong>[<%# WebApp.DisplayString.UserInfo %>]</strong>
		<ul>
			<li><span class="charinfo-title"><%# WebApp.DisplayString.UserDBID %></span>:<asp:Label runat="server" ID="UserDbId"/></li>
			<li><span class="charinfo-title"><%# WebApp.DisplayString.UserName %></span>:<asp:Label runat="server" ID="UserName"/></li>
			<li><span class="charinfo-title"><%# WebApp.DisplayString.ServerName %></span>:<asp:Label runat="server" ID="ServerName"/></li>
			<li><span class="charinfo-title"><%# WebApp.DisplayString.AccountDBID %></span>:<asp:Label runat="server" ID="AccountDbId"/></li>
			<li><span class="charinfo-title"><%# WebApp.DisplayString.AccountName %></span>:<asp:Label runat="server" ID="AccountName"/></li>
		</ul>
	</div>

	<strong>던전 페이즈 레벨을 설정하세요.</strong>

	<div id="dungeon-data">
		<div>
			<span><%# WebApp.DisplayString.DungeonName %>:</span>
			<asp:Label runat="server" ID="DungeonName"/>
		</div>
		<div>
			<span><%# WebApp.DisplayString.PhaseLevel %></span>
			<asp:TextBox runat="server" ID="PhaseLevel" OnTextChanged="PhaseLevel_TextChanged"/>
		</div>
	</div>
</asp:Content>