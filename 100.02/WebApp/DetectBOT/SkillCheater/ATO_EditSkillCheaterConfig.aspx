﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_EditSkillCheaterConfig.aspx.cs" Inherits="WebApp.DetectBOT.SkillCheater.ATO_EditSkillCheaterConfig" %>
<%@ Import Namespace="WebApp" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">
	<div class="ato-main">
		<div style="margin-bottom: 10px; font-weight: bold; text-decoration: underline;">모든 서버에 일괄 적용됩니다.</div>
		<ul class="form-style">
			<li><label class="width150"><%# DisplayString.ServerType %></label><asp:DropDownList ID="ServerType" class="servertype" OnSelectedIndexChanged="ServerType_SelectedIndexChanged" AutoPostBack="true" runat="server" /></li>
			<li><label class="width150"><%# DisplayString.OnOff %></label>
				<asp:RadioButtonList ID="TurnOn" RepeatLayout="UnorderedList" class="radio-group" runat="server">
					<asp:ListItem Text="On" Value="True"/>
					<asp:ListItem Text="Off" Value="False" Selected="True"/>
				</asp:RadioButtonList>
			</li>
			<li><label class="width150"><%# DisplayString.CheckTermSeconds %></label><asp:TextBox ID="CheckTermSeconds" placeholder="" runat="server" /></li>
			<li><label class="width150"><%# DisplayString.CountThreshold %></label><asp:TextBox ID="SkillCountThresholdStr" style="width: 300px;" placeholder="" runat="server" /></li>
			<li><label class="width150"><%# DisplayString.KickUserByAdmin %></label>
				<asp:RadioButtonList ID="KickCheater" RepeatLayout="UnorderedList" class="radio-group" runat="server">
					<asp:ListItem Text="Enable" Value="True"/>
					<asp:ListItem Text="Disable" Value="False" Selected="True"/>
				</asp:RadioButtonList>
			</li>
			<li class="world-only"><label class="width150"><%# DisplayString.SkipDamage %></label>
				<asp:RadioButtonList ID="SkipDamage" RepeatLayout="UnorderedList" class="radio-group" runat="server">
					<asp:ListItem Text="Enable" Value="True"/>
					<asp:ListItem Text="Disable" Value="False" Selected="True"/>
				</asp:RadioButtonList>
			</li>
		</ul>
	</div>

	<script type="text/javascript">
		$(function () {
			var insertMode = '<%=InsertMode %>' == 'True';

			if (insertMode == true) {
				$('.insert-only').show();
				$('.edit-only').hide();
			}
			else {
				$('.insert-only').hide();
				$('.edit-only').show();
			}

			CheckServerType();

			$('.servertype').change(function () {
				CheckServerType();
			})
		})

		function CheckServerType() {
			var serverType = $('select').first().val();
			if (serverType == 1) {
				$('.arbiter-only').show();
				$('.world-only').hide();
			} else {
				$('.arbiter-only').hide();
				$('.world-only').show();
			}
		}
	</script>
</asp:Content>
