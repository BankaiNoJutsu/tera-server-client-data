﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="ResetManagement.aspx.cs" Inherits="WebApp.Server.ResetManagement.ResetManagement" %>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">
<form runat="server" id="form1">
    <table class="nolineTable">
        <tr>
            <td> <%= WebApp.DisplayString.ServerChoice %> </td>
            <td><asp:DropDownList runat="server" ID="ServerSelect" DataSource="<%# WebApp.WebAdminDB.ServerListWithAll.Select(x => new { ServerName = x.mName, ServerNo = x.mNo }) %>" DataTextField="ServerName" DataValueField="ServerNo"/></td>
			<td><asp:Button runat="server" ID="SelectOption" Text="Search" OnClick="SelectOption_Click" /></td>
        </tr>
    </table><br/>

    <table class="lineTable">
        <asp:Repeater runat="server" ID="OnOffStateList">
            <HeaderTemplate>
                <tr>
                    <th><%# WebApp.DisplayString.ServerName %></th>
                    <th><%# WebApp.DisplayString.DungeonEnterCount %></th>
                    <th><%# WebApp.DisplayString.Control%></th>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><%# Eval("ServerName") %></td>
                    <td><%# (bool)Eval("DungeonEnterCountReset") ? "On" : "Off" %></td>
                    <td>
                        <asp:Button runat="server" ID="DungeonEnterCountResetOnOff" 
                            Text='<%# (bool)Eval("DungeonEnterCountReset") ? "Off" : "On"%>'
                            CommandName="DungeonEnterCountResetOnOff"
                            CommandArgument='<%# Eval("ServerNo") + " " + ((bool)Eval("DungeonEnterCountReset") ? "Off" : "On") %>'
                            OnCommand="DungeonEnterCountResetOnOff_Command" />
                    </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
</form>
</asp:Content>