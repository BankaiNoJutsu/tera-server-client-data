﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="AchievementSeason.aspx.cs" Inherits="WebApp.Server.AchievementSeason" %>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">
<form runat="server" id="form1">
    <table class="nolineTable">
        <tr>
            <td> <%= WebApp.DisplayString.ServerChoice %> </td>
            <td> <asp:DropDownList runat="server" ID="ServNoDDL" DataTextField="Text" DataValueField="Value" /> </td>
            <td> &nbsp<asp:Button runat="server" ID="SearchButton" OnClick="SearchButton_Click" /> </td>
            <td> &nbsp; </td>
        </tr>
    </table><br/>

    <p style="font-weight:bold">[CurrentSeasonInfo]</p><br/>

    <asp:GridView CssClass="lineTable" ID="CurrentSeasonGrid" runat="server" AutoGenerateColumns="False" ShowHeader="true" ShowHeaderWhenEmpty="true">
    <Columns>
        <asp:TemplateField   HeaderStyle-Width="100">
            <HeaderTemplate><%=WebApp.DisplayString.ServerName%></HeaderTemplate>
            <ItemTemplate><%# GetServerName(Convert.ToInt16(Eval("serverNo"))) %></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <HeaderTemplate>Current Season</HeaderTemplate>
            <ItemTemplate><%# Eval("seasonId").ToString()%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField >
            <HeaderTemplate><%=WebApp.DisplayString.StartTime%></HeaderTemplate>
            <ItemTemplate><%# Eval("startDate").ToString()%></ItemTemplate>
        </asp:TemplateField>
    </Columns>
    </asp:GridView><br /><br />

    <p style="font-weight:bold">[Achievement Season Management]</p><br/>
    <asp:Button runat="server" ID="AddButton" text="" OnClick="AddButton_Click" Visible="true"/>&nbsp;&nbsp;
    <asp:Button runat="server" ID="DelButton" text="" OnClick="DelButton_Click" Visible="true"/><br /><br />

    <asp:GridView CssClass="lineTable" ID="SeasonInfoGrid" runat="server" AutoGenerateColumns="False" ShowHeader="true" ShowHeaderWhenEmpty="true" OnRowCommand="OnRowCommand">
    <Columns>
        <asp:TemplateField>
            <HeaderTemplate></HeaderTemplate>
            <ItemTemplate><asp:CheckBox ID="chkRow" runat="server"/></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField Visible="false">
            <HeaderTemplate></HeaderTemplate>
            <ItemTemplate><%# Eval("serverNo").ToString() %></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField   HeaderStyle-Width="100">
            <HeaderTemplate><%=WebApp.DisplayString.ServerName%></HeaderTemplate>
            <ItemTemplate><%# GetServerName(Convert.ToInt16(Eval("serverNo"))) %></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField>
            <HeaderTemplate>Season ID</HeaderTemplate>
            <ItemTemplate><%# Eval("seasonId").ToString()%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField >
            <HeaderTemplate><%=WebApp.DisplayString.StartTime%></HeaderTemplate>
            <ItemTemplate><%# Eval("startDate").ToString()%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField >
            <HeaderTemplate></HeaderTemplate>
            <ItemTemplate><asp:Button runat="server" Text='<%# WebApp.DisplayString.EditItem%>' ID="EditButton" CommandArgument='<%# Eval("serverNo").ToString() +"," + Eval("seasonId").ToString()%>'/></ItemTemplate>
        </asp:TemplateField>
    </Columns>
    </asp:GridView>
</form>
</asp:Content>