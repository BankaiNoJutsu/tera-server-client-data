﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/CustomAskDialog.Master" AutoEventWireup="true" CodeBehind="ATO_AddAchievementSeason.aspx.cs" Inherits="WebApp.Server.ATO_AddAchievementSeason" %>
<asp:Content ID="ContentHeader" ContentPlaceHolderID="ContentHeader" runat="server">
    <link href="/Scripts/jquery.simple-dtpicker.css" rel="stylesheet"/>
</asp:Content>

<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentBody" runat="server">
    <script type="text/javascript">
            $(function () {
                $("[id$=StartDate]").appendDtpicker({ "minuteInterval": 5 });
            });
    </script>

    <form id="Form2" runat="server" class="nolineTable">   
        <!-- 잘못 입력 된 값에 대한 안내 메세지  -->
        <p style="color:red; font-weight:bold"><asp:Label ID="ErrorInfo" runat="server" visible="false"/></p><br />

        <asp:Panel id="EditPanel" runat="server" visible="true">
            <!-- 서버 리스트 -->
            <%=WebApp.DisplayString.ServerChoice %> <asp:DropDownList runat="server" ID="ServNoDDL" DataTextField="Text" DataValueField="Value" />
            <br /><br />

            <%=WebApp.DisplayString.AchievementSeasonID %> <asp:TextBox runat="server" type="text" id="SeasonId" style="text-align:center"/>
            <br /><br />

            <!-- 기간 -->
            <%=WebApp.DisplayString.StartDate %> <asp:TextBox runat="server" type="text" id="StartDate" style="text-align:center" readonly/>
            <br /><br />

            <!-- 메모 입력란 -->
            <p style="font-weight:bold">[<%= WebApp.DisplayString.MemoStr %>]</p><br />
            <asp:TextBox runat="server" width="250px" height="100px" ID="MemoBox" TextMode="MultiLine" style="overflow:auto"/><br /><br />    

            <!-- 확인 버튼 -->
          <asp:Button runat="server" ID="ConfirmButton" text='<%# WebApp.DisplayString.AddAchievementSeason%>' OnClick="ConfirmButton_Click" />
        </asp:Panel>

        <asp:Button runat="server" ID="CloseButton" text="<%# WebApp.DisplayString.Close%>" OnClick="CloseButton_Click" Visible="false"/><br />
    </form>
</asp:Content>
