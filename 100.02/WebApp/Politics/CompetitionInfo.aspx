﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="CompetitionInfo.aspx.cs" Inherits="WebApp.Politics.CompetitionInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form runat="server" id="resultForm" >

        <asp:Table runat="server" ID="inputTable" CssClass="outerlineTable">
            <asp:TableRow>
                <asp:TableCell> <%= WebApp.DisplayString.ServerChoice %> </asp:TableCell>
                <asp:TableCell> <asp:DropDownList runat="server" ID="ServNoDropDownList" DataTextField="Text" DataValueField="Value" /> </asp:TableCell>
            </asp:TableRow>
            <asp:TableRow>
                <asp:TableCell> <asp:Button runat="server" ID="SubmitButton" OnClick="SubmitButton_Click" /> </asp:TableCell>
            </asp:TableRow>       
        </asp:Table>

        <asp:Table runat="server" ID="resultTable" CssClass="nolineTable">   
            <asp:TableRow> <asp:TableCell> &nbsp; </asp:TableCell> </asp:TableRow>        
            <asp:TableRow>                                
                <asp:TableCell Font-Bold="true"> <%= WebApp.DisplayString.CurrentStep%> &nbsp; &nbsp; : &nbsp; &nbsp; <asp:Literal runat="server" ID="currentStep" />  </asp:TableCell>
            </asp:TableRow>

            <asp:TableRow> <asp:TableCell> &nbsp; </asp:TableCell> </asp:TableRow>
            <asp:TableRow>                                
                <asp:TableCell Font-Bold="true"> <%= WebApp.DisplayString.VoteCandidateList%> </asp:TableCell>                
            </asp:TableRow>            
            <asp:TableRow>    
                <asp:TableCell>
                    <table class="lineTable">
                        <tr>
                            <th> <%= WebApp.DisplayString.Politics_Rank %></th>
                            <th> <%= WebApp.DisplayString.GainVoteCount%></th>
                            <th> <%= WebApp.DisplayString.GuildName %></th>
                            <th> <%= WebApp.DisplayString.UserName %></th>
                            <th> <%= WebApp.DisplayString.ExpectedGuard%></th>
                            <th> <%= WebApp.DisplayString.Pledge%></th>
                            <th />
                        </tr>
                        <asp:Repeater runat="server" ID="voteCandidateListRepeater">
                        <ItemTemplate>                            
                            <tr>
                                <td> <%# DataBinder.GetPropertyValue(Container.DataItem, "No") %> </td> 
                                <td> <%# DataBinder.GetPropertyValue(Container.DataItem, "Point") %> </td> 
                                <td> <%# DataBinder.GetPropertyValue(Container.DataItem, "GuildName") %> </td> 
                                <td> <%# DataBinder.GetPropertyValue(Container.DataItem, "Name") %> </td> 
                                <td> <asp:TextBox runat="server" ID="TB1" BorderWidth="0" ForeColor="0" Enabled = "false" Text='<%# DataBinder.GetPropertyValue(Container.DataItem, "GuardShort") %>' ToolTip='<%# DataBinder.GetPropertyValue(Container.DataItem, "GuardLong") %>' /> </td>
                                <td style="width:250px"> 
                                    <asp:TextBox runat="server" ID="TB2" BorderWidth="0" ForeColor="0" Enabled = "false" Text='<%# DataBinder.GetPropertyValue(Container.DataItem, "PledgeShort") %>' ToolTip='<%# DataBinder.GetPropertyValue(Container.DataItem, "PledgeLong") %>' /> 
                                </td>
                                <td>
                                    <a href="<%# DataBinder.GetPropertyValue(Container.DataItem, "ChangePledgeURL")%>"> <font color="red">[<%=WebApp.DisplayString.ChangePledge%>]</font> </a>
                                </td>
                            </tr>
                        </ItemTemplate>
                        </asp:Repeater>
                    </table>
                 </asp:TableCell>                                
            </asp:TableRow>

            <asp:TableRow> <asp:TableCell> &nbsp; </asp:TableCell> </asp:TableRow>
            <asp:TableRow>                                
                <asp:TableCell Font-Bold="true"> <%= WebApp.DisplayString.BattleFieldCandidateList%> </asp:TableCell>                
            </asp:TableRow>            
            <asp:TableRow>    
                <asp:TableCell>
                    <table class="lineTable">
                        <tr>
                            <th> <%= WebApp.DisplayString.Politics_Rank %></th>
                            <th> <%= WebApp.DisplayString.CompetitionPoint %></th>
                            <th> <%= WebApp.DisplayString.GuildName %></th>
                            <th> <%= WebApp.DisplayString.UserName %></th>
                            <th> <%= WebApp.DisplayString.ExpectedGuard%></th>
                            <th> <%= WebApp.DisplayString.Pledge%></th>
                            <th />
                        </tr>
                        <asp:Repeater runat="server" ID="battleFieldCandidateListRepeater">
                        <ItemTemplate>                            
                            <tr>
                                <td> <%# DataBinder.GetPropertyValue(Container.DataItem, "No") %> </td> 
                                <td> 
                                    <%# DataBinder.GetPropertyValue(Container.DataItem, "Point") %> 
                                    <a href="<%# DataBinder.GetPropertyValue(Container.DataItem, "ChangePointURL")%>"> <font color="red">[<%=WebApp.DisplayString.Modify%>]</font> </a>
                                </td> 
                                <td> <%# DataBinder.GetPropertyValue(Container.DataItem, "GuildName") %> </td> 
                                <td> <%# DataBinder.GetPropertyValue(Container.DataItem, "Name") %> </td> 
                                <td> <asp:TextBox runat="server" ID="TB1" BorderWidth="0" ForeColor="0" Enabled = "false" Text='<%# DataBinder.GetPropertyValue(Container.DataItem, "GuardShort") %>' ToolTip='<%# DataBinder.GetPropertyValue(Container.DataItem, "GuardLong") %>' /> </td>
                                <td style="width:250px"> 
                                    <asp:TextBox runat="server" ID="TB2" BorderWidth="0" ForeColor="0" Enabled = "false" Text='<%# DataBinder.GetPropertyValue(Container.DataItem, "PledgeShort") %>' ToolTip='<%# DataBinder.GetPropertyValue(Container.DataItem, "PledgeLong") %>' /> 
                                </td>
                                <td>
                                    <a href="<%# DataBinder.GetPropertyValue(Container.DataItem, "ChangePledgeURL")%>"> <font color="red">[<%=WebApp.DisplayString.ChangePledge%>]</font> </a>
                                </td>
                            </tr>
                        </ItemTemplate>
                        </asp:Repeater>
                    </table>
                 </asp:TableCell>                                
            </asp:TableRow>

            <asp:TableRow> <asp:TableCell> &nbsp; </asp:TableCell> </asp:TableRow>
            <asp:TableRow>                                
                <asp:TableCell Font-Bold="true"> <%= WebApp.DisplayString.GuildWarCandidateList%> </asp:TableCell>                
            </asp:TableRow>            
            <asp:TableRow>    
                <asp:TableCell>
                    <table class="lineTable">
                        <tr>
                            <th> <%= WebApp.DisplayString.Politics_Rank %></th>
                            <th> <%= WebApp.DisplayString.CompetitionPoint %></th>
                            <th> <%= WebApp.DisplayString.GuildName %></th>
                            <th> <%= WebApp.DisplayString.UserName %></th>
                            <th> <%= WebApp.DisplayString.ExpectedGuard%></th>
                            <th> <%= WebApp.DisplayString.Pledge%></th>
                            <th />
                            <th> <%= WebApp.DisplayString.LordGuildWarId%></th>
                        </tr>
                        <asp:Repeater runat="server" ID="guildWarCandidateListRepeater">
                        <ItemTemplate>                            
                            <tr>
                                <td> <%# DataBinder.GetPropertyValue(Container.DataItem, "No") %> </td> 
                                <td> 
                                    <%# DataBinder.GetPropertyValue(Container.DataItem, "Point") %> 
                                    <a href="<%# DataBinder.GetPropertyValue(Container.DataItem, "ChangePointURL")%>"> <font color="red">[<%=WebApp.DisplayString.Modify%>]</font> </a>
                                </td> 
                                <td> <%# DataBinder.GetPropertyValue(Container.DataItem, "GuildName") %> </td> 
                                <td> <%# DataBinder.GetPropertyValue(Container.DataItem, "Name") %> </td> 
                                <td> <asp:TextBox runat="server" ID="TB1" BorderWidth="0" ForeColor="0" Enabled = "false" Text='<%# DataBinder.GetPropertyValue(Container.DataItem, "GuardShort") %>' ToolTip='<%# DataBinder.GetPropertyValue(Container.DataItem, "GuardLong") %>' /> </td>
                                <td style="width:250px"> 
                                    <asp:TextBox runat="server" ID="TB2" BorderWidth="0" ForeColor="0" Enabled = "false" Text='<%# DataBinder.GetPropertyValue(Container.DataItem, "PledgeShort") %>' ToolTip='<%# DataBinder.GetPropertyValue(Container.DataItem, "PledgeLong") %>' /> 
                                </td>
                                <td>
                                    <a href="<%# DataBinder.GetPropertyValue(Container.DataItem, "ChangePledgeURL")%>"> <font color="red">[<%=WebApp.DisplayString.ChangePledge%>]</font> </a>
                                </td>
                                <td> <%# DataBinder.GetPropertyValue(Container.DataItem, "LordGuildWarId") %> </td> 
                            </tr>
                        </ItemTemplate>
                        </asp:Repeater>
                    </table>
                 </asp:TableCell>                                
            </asp:TableRow>

        </asp:Table>

    </form>
    
</asp:Content>
