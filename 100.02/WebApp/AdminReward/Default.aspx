﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApp.AdminReward.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form runat="server" class="nolineTable">

        <table class="nolineTable">
            <tr>
                <td> <%= WebApp.DisplayString.ServerChoice %> </td>
                <td> <asp:DropDownList runat="server" ID="ServNoDDL" DataTextField="Text" DataValueField="Value" /> </td>
            </tr>
            <tr>
                <td> <asp:Button runat="server" ID="SubmitButton" OnClick="SubmitButton_Click" /> </td>
            </tr>     
        </table>

        <asp:Panel runat="server" ID="resultPanel" Visible="false">

        &nbsp;

        <table class="lineTable">        
            <tr>
                <th> <b> <%= WebApp.DisplayString.AdminRewardId%> </b> </th>
                <th> <b> <%= WebApp.DisplayString.AdminRewardDesc%> </b> </th>
                <th> <b> <%= WebApp.DisplayString.AdminRewardBeginTime%> </b> </th>
                <th> <b> <%= WebApp.DisplayString.AdminRewardEndTime%> </b> </th>
                <th> <b> </b> </th>                
            </tr>
            <asp:Repeater runat="server" ID="rewardStatus">
            <ItemTemplate>
            <tr>
                <td> <%# Eval("RewardId") %> </td>
                <td> <%# Eval("RewardDesc") %> </td>
                <td> <%# Eval("RewardBeginTime") %> </td>
                <td> <%# Eval("RewardEndTime") %> </td>
                <td> <a href="<%# Eval("ExecuteRewardURL") %>"> <%# Eval("ExecuteRewardLinkText") %> </a> </td>
            </tr>
            </ItemTemplate>
            </asp:Repeater>
        </table>

        </asp:Panel>

    </form>

</asp:Content>
