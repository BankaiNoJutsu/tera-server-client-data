﻿<%@ Page MasterPageFile="~/AppCode/WebAppPage.Master" Language="C#" AutoEventWireup="true" CodeBehind="UpdateNotification.aspx.cs" Inherits="WebApp.Announce.UpdateNotification" %>
<%@ Register TagPrefix="webApp" TagName="SearchForm" Src="~/SearchForm.ascx" %>

<asp:Content ID="Content1" runat="server" 
    contentplaceholderid="mainContentHolder">

    <form runat="server" id ="resultForm" >
    
    <table class="nolineTable">
        <tr>
            <td> <%= WebApp.DisplayString.ServerChoice %> </td>
            <td> <asp:DropDownList runat="server" ID="ServNoDDL" DataTextField="Text" DataValueField="Value" /> </td>
        </tr>
        <tr>
            <td> <asp:Button runat="server" ID="SearchButton" OnClick="SearchButton_Click" /> </td><td><%=WriteButtonStyle%></td>
        </tr>
    </table>
    <br />
    [<%= UpdateNotificationTitle%>]
    <br />
    <asp:GridView CssClass="lineTable" ID="GVNotiList" runat="server" AutoGenerateColumns="False" ShowHeader="true" ShowHeaderWhenEmpty="true">
    <Columns>
        <asp:TemplateField HeaderStyle-Width="150">
            <HeaderTemplate><%=WebApp.DisplayString.Date%></HeaderTemplate>
            <ItemTemplate><%# SetUTCTime(Convert.ToDateTime(Eval("registerTime")))%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="100">
            <HeaderTemplate><%=WebApp.DisplayString.ServerName%></HeaderTemplate>
            <ItemTemplate><%# SetSeverName(Convert.ToInt16(Eval("serverNo")))%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="250">
            <HeaderTemplate><%=WebApp.DisplayString.Title%></HeaderTemplate>
            <ItemTemplate><%# Eval("title")%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="80">
            <HeaderTemplate><%=WebApp.DisplayString.DetailInfo%></HeaderTemplate>
            <ItemTemplate><%# SetButton(Convert.ToInt16(Eval("serverNo")), Convert.ToInt16(Eval("messageId")))%></ItemTemplate>
        </asp:TemplateField>
    </Columns>
    </asp:GridView>
    <%=UpdateNotificationPaging%>
    </form>
</asp:Content>


