﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="VipStoreGreeting.aspx.cs" Inherits="WebApp.Announce.VipStoreGreeting" %>
<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form id="Form2" runat="server" class="nolineTable">
        <p style="font-weight:bold">[<%= WebApp.DisplayString.VipStoreGreeting %>]</p><br/>
        <table class="nolineTable">
            <tr>
                <td> <%= WebApp.DisplayString.ServerChoice %> </td>
                <td> <asp:DropDownList runat="server" ID="ServNoDDL" DataTextField="Text" DataValueField="Value" /> </td>
                <td> &nbsp<asp:Button runat="server" ID="SearchButton" OnClick="SearchButton_Click" /> </td>
                <td> &nbsp; </td>
            </tr>
        </table><br/>
        <asp:Button runat="server" ID="AddButton" text="" OnClick="AddButton_Click" Visible="true"/>&nbsp;&nbsp;
        <asp:Button runat="server" ID="DelAllButton" text="" OnClick="DelAllButton_Click" Visible="true"/><br /><br />

        <asp:GridView CssClass="lineTable" ID="EventList" runat="server" AutoGenerateColumns="False" ShowHeader="true" ShowHeaderWhenEmpty="true" OnRowCommand="OnRowCommand" AllowPaging="true" OnPageIndexChanging="EventList_PageIndexChanging">
        <Columns>
            <asp:TemplateField   HeaderStyle-Width="100">
                <HeaderTemplate><%=WebApp.DisplayString.ServerName%></HeaderTemplate>
                <ItemTemplate><%# Eval("serverName").ToString()%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-Width="300">
                <HeaderTemplate><%=WebApp.DisplayString.Greeting%></HeaderTemplate>
                <ItemTemplate><%# Eval("greeting").ToString()%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField >
                <HeaderTemplate><%=WebApp.DisplayString.StartTime%></HeaderTemplate>
                <ItemTemplate><%# Eval("startTime").ToString()%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField >
                <HeaderTemplate><%=WebApp.DisplayString.EndTime%></HeaderTemplate>
                <ItemTemplate><%# Eval("endTime").ToString()%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField >
                <HeaderTemplate></HeaderTemplate>
                <ItemTemplate><asp:Button runat="server" Text='<%# WebApp.DisplayString.DeleteGreeting%>' ID="DelButton" CommandArgument='<%# Eval("serverNo").ToString() +"," + Eval("eventId").ToString()%>'/></ItemTemplate>
            </asp:TemplateField>
        </Columns>
        </asp:GridView>
    </form>
</asp:Content>
