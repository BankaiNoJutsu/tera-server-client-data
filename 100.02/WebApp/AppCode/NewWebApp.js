﻿function InvokeWebServiceSync(url, param, onResult) {
    var ret = null;
    var ajaxParam = {
        url: url,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: false,
        success: function(jsonData) {
            if (jsonData.hasOwnProperty('d'))
                jsonData = JSON.parse(jsonData.d);

            if (onResult)
                onResult(jsonData);

            ret = jsonData;
        }
    };

    if (param)
        ajaxParam.data = JSON.stringify(param);
            
    $.ajax(ajaxParam);
            
    return ret;
}

function InvokeWebServiceSyncByUri(url, param, onResult) {
    var ret = null;
    var ajaxParam = {
        url: url,
        type: 'GET',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        async: false,
        success: function(jsonData) {
            if (jsonData.hasOwnProperty('d'))
                jsonData = JSON.parse(jsonData.d);

            if (onResult)
                onResult(jsonData);

            ret = jsonData;
        }
    };

    if (param)
        ajaxParam.data = param;
    
    $.ajax(ajaxParam);
            
    return ret;
}
        
function InvokeWebService(url, param, onSuccess, onError) {            
    var ajaxParam = {
        url: url,
        type: 'POST',
        contentType: 'application/json; charset=utf-8',
        dataType: 'json',
        success: function(jsonData) {
            if (jsonData.hasOwnProperty('d'))
                jsonData = JSON.parse(jsonData.d);

            if (onSuccess)
                onSuccess(jsonData);
        },
        error: function(xhr, status, e) {
            if (onError)
                onError(e);
        }
    };

    if (param)
        ajaxParam.data = JSON.stringify(param);
            
    $.ajax(ajaxParam);
}

function GetDisplayString() {
    return InvokeWebServiceSyncByUri('/api/rest/DisplayString');
}
        
function RegisterMemoDialog(messageBoxId, memoDialogId) {
    var messageBoxSelector = '#' + messageBoxId;
    $('<div/>', {
        id: messageBoxId,
    }).appendTo('body');
            
    $(messageBoxSelector).append($('<label/>', {
        id: messageBoxId + '-label'
    }));

    var memoTextId = memoDialogId + '-text';
    var memoDialogSkeleton = ['<div id="' + memoDialogId + '">',
        '<form>',
        '<fieldset>',
        '<textarea id="' + memoTextId + '" class="text ui-widget-content ui-corner-all" rows="10" cols="50"/>',
        '</fieldset>',
        '</form>',
        '</div>'].join('\r\n');
    $(memoDialogSkeleton).appendTo('body');
}

$.fn.memoDialog = function (displayString, commitAction) {
    var thisDialog = this;
    var messageBox = this.find('#message-box');
    var messageTitle = this.find('#message-title');
    if(messageTitle.length == 0)
        messageTitle = displayString.MemoRegister;

    var messageBoxButtons = {};
    messageBoxButtons[displayString.OK] = function () {
        $(this).dialog("close");
        thisDialog.dialog("close");
        location.reload();
    };

    messageBox.dialog({
        title: displayString.Result,
        modal: true,
        autoOpen: false,
        buttons: messageBoxButtons
    });

    var dialogFormButtons = {};
    dialogFormButtons[displayString.OK] = function () {
        if (confirm(displayString.ConfirmInput)) {
            commitAction();
        }
    };

    dialogFormButtons[displayString.Cancel] = function () {
        $(this).dialog("close");
    };

    this.dialog({
        title: messageTitle,
        autoOpen: false,
        height: 500,
        width: 500,
        modal: true,
        buttons: dialogFormButtons
    });
}

function GetUrlParam(param)
{
    var pageUrl = window.location.search.substring(1);
    var urlVariable = pageUrl.split('&');
    for (var i = 0 ; i< urlVariable.length; i++)
    {
        var paramName = urlVariable[i].split('=');
        if (paramName[0] == param)
        {
            return paramName[1];
        }   
    }

    return '';
}

Date.prototype.GetNormalFormat = function () {
	var yyyy = this.getFullYear();
	var MM = this.getMonth() + 1;
	var dd = this.getDate();
	var HH = this.getHours();
	var mm = this.getMinutes();

	// yyyy-MM-dd HH:mm
	return [yyyy, (MM <= 9 ? '0' : '') + MM, (dd <= 9 ? '0' : '') + dd].join('-') + ' ' + [(HH <= 9 ? '0' : '') + HH, (mm <= 9 ? '0' : '') + mm].join(':');
};