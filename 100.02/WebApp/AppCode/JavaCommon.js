(function() {
	window.spawn = window.spawn || function(gen) {
		function continuer(verb, arg) {
			var result;
			try {
				result = generator[verb](arg);
			} catch (err) {
				return Promise.reject(err);
			}
			if (result.done) {
				return result.value;
			} else {
				return Promise.resolve(result.value).then(onFulfilled, onRejected);
			}
		}
		var generator = gen();
		var onFulfilled = continuer.bind(continuer, 'next');
		var onRejected = continuer.bind(continuer, 'throw');
		return onFulfilled();
	};
	window.showModalDialog = window.showModalDialog || function(url, arg, opt) {
		url = url || '';
		arg = arg || null;
		opt = (opt || 'dialogWidth:800px;dialogHeight:600px') + ';max-width:70%;max-height:70%';
		var caller = showModalDialog.caller ? showModalDialog.caller.toString() : "function _FakeCaller(){}";
		var dialog = document.body.appendChild(document.createElement('dialog'));
		dialog.setAttribute('id', 'dialog-dialog');
		dialog.setAttribute('style', opt.replace(/dialog/gi, '').replace(/=/gi, ':'));
		dialog.innerHTML = '<a href="#" id="dialog-close" style="position: absolute; top: 0; right: 5px; font-size: 20px; color: #000; text-decoration: none; outline: none;">&times;</a><iframe id="dialog-body" src="' + url + '" style="border: 0; width: 100%; height: 100%;"></iframe>';
		document.getElementById('dialog-body').contentWindow.dialogArguments = arg;
		document.getElementById('dialog-close').addEventListener('click', function(e) {
			e.preventDefault();
			dialog.close();
		});
		dialog.showModal();
		if(caller.indexOf('yield') >= 0 || caller.indexOf('await') >= 0) {
			return new Promise(function(resolve, reject) {
				dialog.addEventListener('close', function() {
					parent.window.location = parent.window.location.href;
					var returnValue = document.getElementById('dialog-body').contentWindow.returnValue;
					document.body.removeChild(dialog);
					resolve(returnValue);
				});
			});
		}
		var isNext = false;
		var nextStmts = caller.split('\n').filter(function(stmt) {
			if(isNext || stmt.indexOf('showModalDialog(') >= 0)
				return isNext = true;
			return false;
		});
		dialog.addEventListener('close', function() {
			parent.window.location = parent.window.location.href;
			var returnValue = document.getElementById('dialog-body').contentWindow.returnValue;
			document.body.removeChild(dialog);
			nextStmts[0] = nextStmts[0].replace(/(window\.)?showModalDialog\(.*\)/g, JSON.stringify(returnValue));
			eval('{\n' + nextStmts.join('\n'));
		});
		throw 'Execution stopped until showModalDialog is closed';
	};
	window.close = function() {
		var dialog = parent.document.getElementById('dialog-dialog');
		if (dialog) {
			dialog.close();
		}
	}
})();

function StringCompare(left, right)
{
    var leftstring = String(left).toLowerCase();
    var rightstring = String(right).toLowerCase();
    
    if ( leftstring == rightstring)
        return true;

    return false;
}

//function Navigate( url )
//{
//    var confirmed = window.confirm("Delete?" );
//    window.confirm(confirmed);
//    
//    
//    //window.navigate(url);
//    
//    //this.location.reload();
//    //window.location = url;
//    //this.location = url;
//    //javascript:window.open(url);
//}


function ConfirmDelete() 
{
    var confirmed = windows.confirm("Delete?" );
    return confirmed; 
}

function exit_page()
{

} 

function NavigateModalDialog( url )
{
    var args = new Array();
    
    window.showModalDialog(url, args,  "dialogHeight:800px; dialogWidth:1280px")
}

//function NavigateModallessDialog( url )
//{
//    window.showModelessDialog(url, window,  "dialogHeight:600px; dialogWidth:800px")
//}


function NavigateMe( url ) {

    window.location = url;

    /// window.navigate(url);

    //this.location.reload();
    //window.location = url;
    //this.location = url;
    //javascript:window.open(url);
}



///// tid amount 가 form으로 만들어져있다고 보고 url에 추가하게 해서 넘겨준다
//function NavigateMeAddItem( url )
//{

//    var obj_additem_tid = document.getElementsByName("tid");
//    var obj_additem_amount = document.getElementsByName("amount");
//    
//    if ( (obj_additem_tid.length == 1) && (obj_additem_amount.length == 1) )
//    {
//        var tid = obj_additem_tid[0].value;
//        var amount = obj_additem_amount[0].value;
//        
//        var targetUrl = url + "&tid=" + tid + "&amount=" + amount;
//        
//        window.navigate(targetUrl);
//        
//    }
//    
//    
////    windows.confirm(document.getElementById('additem_tid').value);
//    //this.location.reload();
//    //window.location = url;
//    //this.location = url;
//    //javascript:window.open(url);
//}


///// tid amount 가 form으로 만들어져있다고 보고 url에 추가하게 해서 넘겨준다
//function NavigateModalDialogAddItem( url )
//{
//    var obj_additem_tid = document.getElementsByName("tid");
//    var obj_additem_amount = document.getElementsByName("amount");
//    
//    if ( (obj_additem_tid.length == 1) && (obj_additem_amount.length == 1) )
//    {
//        var tid = obj_additem_tid[0].value;
//        var amount = obj_additem_amount[0].value;
//        var targetUrl = url + "&tid=" + tid + "&amount=" + amount;
//        
//        window.showModalDialog(targetUrl, false,  "dialogHeight:600px; dialogWidth:800px")
//        
//    }
//        
//    

//}


/// 공지사항 보내기 내용들은 form으로 만들어져있다고 보고 url에 추가하게 해서 넘겨준다
function NavigateModalDialogAnnounce(namePrefix, url )
{


    var selectedServerNo = "";

    for(i = 0; i < document.all.length; i++)
    {
        var onenode = document.all[i];
        
        var nodename = String(onenode.name).toLowerCase();
        
        if ( nodename.search(namePrefix + "server_") == 0)
        {


            if ( onenode.checked == true && onenode.name != "everything__")
            {
                var ss ;
                ss = nodename.split("_");
                if (ss.length == 2)
                {
                    if ( selectedServerNo.length > 0)
                        selectedServerNo  += ",";
                    selectedServerNo += ss[1];
                }
                    
                //confirm(selectedServerNo);
            }

        }
        
    }


    var obj_announce = document.getElementsByName(namePrefix + "announce");
    var obj_color = document.getElementsByName(namePrefix + "color");
    var obj_anninterval = document.getElementsByName(namePrefix + "anninterval");
    var obj_anntype = document.getElementsByName(namePrefix + "anntype");
    var obj_anncenter = document.getElementsByName(namePrefix + "anncenter");
    var obj_annchat = document.getElementsByName(namePrefix + "annchat");
    var obj_annScheduledAnnounceTimeYear = document.getElementsByName(namePrefix + "annScheduledAnnounceTimeYear");
    var obj_annScheduledAnnounceTimeMonth = document.getElementsByName(namePrefix + "annScheduledAnnounceTimeMonth");
    var obj_annScheduledAnnounceTimeDay = document.getElementsByName(namePrefix + "annScheduledAnnounceTimeDay");
    var obj_annScheduledAnnouncceTimeHour = document.getElementsByName(namePrefix + "annScheduledAnnouncceTimeHour");
    var obj_annScheduledAnnounceTimeMinutes = document.getElementsByName(namePrefix + "annScheduledAnnounceTimeMinutes");
    var obj_annScheduledAnnounceTimeSeconds = document.getElementsByName(namePrefix + "annScheduledAnnounceTimeSeconds");

    var appendingStr = "&annid=" + namePrefix;

    if ( obj_announce.length == 1)
    {
        var str = new String(obj_announce[0].value);
        
        /// \b \t로 바꾼다
        str = UrlSafeString(str);
        
        appendingStr += "&announce=" + encodeURI(str);
    }


    if ( selectedServerNo.length > 0)
    {
        appendingStr += "&server=" + selectedServerNo;
    }
    
    if ( obj_color.length == 1)
    {
        appendingStr += "&color=" + obj_color[0].value;
    }
    
    if ( obj_anncenter.length == 1)
    {
        appendingStr += "&center=" + obj_anncenter[0].checked;
    }
    
    if ( obj_annchat.length == 1)
    {
        appendingStr += "&chat=" + obj_annchat[0].checked;
    }
    


    if ( obj_anninterval.length == 1)
    {
        appendingStr += "&interval=" + obj_anninterval[0].value;
    }

    if (obj_annScheduledAnnounceTimeYear.length == 1) 
    {
        appendingStr += "&year=" + obj_annScheduledAnnounceTimeYear[0].value;
    }

    if (obj_annScheduledAnnounceTimeMonth.length == 1)
    {
        appendingStr += "&month=" + obj_annScheduledAnnounceTimeMonth[0].value;
    }

    if (obj_annScheduledAnnounceTimeDay.length == 1) 
    {
        appendingStr += "&day=" + obj_annScheduledAnnounceTimeDay[0].value;
    }

    if (obj_annScheduledAnnouncceTimeHour.length == 1) 
    {
        appendingStr += "&hour=" + obj_annScheduledAnnouncceTimeHour[0].value;
    }

    if (obj_annScheduledAnnounceTimeMinutes.length == 1) 
    {
        appendingStr += "&minutes=" + obj_annScheduledAnnounceTimeMinutes[0].value;
    }

    if (obj_annScheduledAnnounceTimeSeconds.length == 1) 
    {
        appendingStr += "&seconds=" + obj_annScheduledAnnounceTimeSeconds[0].value
    }




    var targetUrl = url + appendingStr;
    
    //window.confirm(targetUrl);
    
    window.showModalDialog(targetUrl, false,  "dialogHeight:800px; dialogWidth:1280px")        
    
     //window.confirm(appendingStr);
     
     
     this.location.reload();
     
     
     
//     
//    window.showModalDialog(targetUrl, false,  "dialogHeight:600px; dialogWidth:800px")
//        
//    this.location.reload();


 }

 function NavigateModalDialogReservedAnnounce(namePrefix, url) 
 {
     var selectedServerNo = "";

     for (i = 0; i < document.all.length; i++) 
     {
         var onenode = document.all[i];

         var nodename = String(onenode.name).toLowerCase();

         if (nodename.search(namePrefix + "server_") == 0) {


             if (onenode.checked == true && onenode.name != "everything__") {
                 var ss;
                 ss = nodename.split("_");
                 if (ss.length == 2) {
                     if (selectedServerNo.length > 0)
                         selectedServerNo += ",";
                     selectedServerNo += ss[1];
                 }
                 //confirm(selectedServerNo);
             }
         }
     }

     var appendingStr = "";

     if (selectedServerNo.length > 0) {
         appendingStr += "&server=" + selectedServerNo;
     }


     var targetUrl = url + appendingStr;

     //window.confirm(targetUrl);

     window.showModalDialog(targetUrl, false, "dialogHeight:800px; dialogWidth:1280px")

     //window.confirm(appendingStr);


     this.location.reload();

 }

 function java_SearchAccountDbId(url, warningMsg1, warningMsg2) {

     var obj_ServerList = document.getElementsByName("ServerNo");

    var targetUrl = url + "?";
    var objTemp;

    objTemp = document.getElementsByName("ServerNo");
    if (objTemp[0].value.length >= 1)
        targetUrl += "&ServerNo=" + objTemp[0].value;

    var checkIdx = 0;
    var bValid = false;
    objTemp = document.getElementsByName("AccDBId");
    if (objTemp[0].value.length >= 1) {
        targetUrl += "&AccDBId=" + encodeURI(objTemp[0].value);
        bValid = true;
        checkIdx = 0;
    }
    else {
        window.alert(warningMsg2);
    }
    targetUrl += "&startYear=" + document.getElementsByName("startYear")[0].value +
            "&startMonth=" + document.getElementsByName("startMonth")[0].value +
            "&startDay=" + document.getElementsByName("startDay")[0].value +
            "&endYear=" + document.getElementsByName("endYear")[0].value +
            "&endMonth=" + document.getElementsByName("endMonth")[0].value +
            "&endDay=" + document.getElementsByName("endDay")[0].value;

    if (bValid) {
        NavigateMe(targetUrl);
    }
 }


function java_SearchAccount(url, warningMsg1, warningMsg2, warningMsg3, warningMsg4, warningMsg5, useSearchDate) {

    var obj_ServerList = document.getElementsByName("ServerNo");
    var obj_SearchType = document.getElementsByName("SearchType");
    
    // 전체 서버 + 캐릭터 검색은 막는다.
    if( obj_ServerList[0].selectedIndex == 0 && (obj_SearchType[2].checked || obj_SearchType[3].checked))
    {
       window.alert(warningMsg1);
    }
    else
    {
      var targetUrl = url + "?" ;
      var objTemp;
      
      objTemp = document.getElementsByName("ServerNo");
      if( objTemp[0].value.length >= 1 )
        targetUrl += "&ServerNo=" + objTemp[0].value;
        
      var checkIdx = 0;
      var bValid = false;
      
      if( obj_SearchType[0].checked )
      {
          objTemp = document.getElementsByName("AccDBId");
          if( objTemp[0].value.length >= 1 )
          {
             targetUrl += "&AccDBId=" + encodeURI(objTemp[0].value);
             bValid = true;
             checkIdx = 0;
          }
          else
          {
            window.alert(warningMsg2);
          }
      }
      else if( obj_SearchType[1].checked )
      {
          objTemp = document.getElementsByName("AccName");
          if( objTemp[0].value.length >= 1 )
          {
            targetUrl += "&AccName=" + encodeURI(objTemp[0].value);
            bValid = true;
            checkIdx = 1;
          }
          else
          {
            window.alert(warningMsg3);
          }
      }
      else if( obj_SearchType[2].checked )
      {
         objTemp = document.getElementsByName("UserName");
         if( objTemp[0].value.length >= 1 )
         {
            targetUrl += "&UserName=" + encodeURI(objTemp[0].value);
            bValid = true;
            checkIdx = 2;
         }
         else
         {
           window.alert(warningMsg4);
         }
       }
       else if (obj_SearchType[3].checked) {
           objTemp = document.getElementsByName("UserDBId");
           if (objTemp[0].value.length >= 1) {
               targetUrl += "&UserDBID=" + encodeURI(objTemp[0].value);
               bValid = true;
               checkIdx = 3;
           }
           else {
               window.alert(warningMsg5);
           }
       }
      
      objTemp = document.getElementsByName("SearchType");
      targetUrl += "&SearchType=" + (checkIdx);

      /// 시간정보 추가
      if (useSearchDate == "True")
      {
          targetUrl += "&startYear=" + document.getElementsByName("startYear")[0].value +
                    "&startMonth=" + document.getElementsByName("startMonth")[0].value +
                    "&startDay=" + document.getElementsByName("startDay")[0].value +
                    "&endYear=" + document.getElementsByName("endYear")[0].value +
                    "&endMonth=" + document.getElementsByName("endMonth")[0].value +
                    "&endDay=" + document.getElementsByName("endDay")[0].value;
      }

      if( bValid ) {
          NavigateMe(targetUrl);
      }
      
    }
}

function java_WatchUser( url , Act ,nAccIdx )
{
   url += ("&" + Act + "=" + nAccIdx);
   
   NavigateMe(url);
}

function java_AskAndRedirect( askMsg, url )
{
    var confirmed = window.confirm(askMsg);
    if ( StringCompare(confirmed, 'true') == true)
    {
    
        NavigateMe(url);
        
    }
    
}

function java_UpdateAdminLev( url )
{
   var obj_val = document.getElementsByName("AdminLev");
   if( obj_val[0].value.length >= 1 )
      url += ("&AdminLev="+obj_val[0].value);

   NavigateMe(url);
}

function java_UpdateUserLimit( url , servNo , askMsg , reUrl )
{
    var confirmed = window.confirm(askMsg);
    if ( StringCompare(confirmed, 'true') == true)
    {
        url += "&Confirm=true";
        
        var keyText = "ServerNo" + servNo;

        var obj_val = document.getElementsByName(keyText);
        if( obj_val[0].value.length >= 1 )
            url += ("&UserLimit="+obj_val[0].value);

        NavigateMe(url);
    }
    else
    {
        NavigateMe(reUrl);
    }
}


//function java_MemoInsert( url , accID )
//{
//    var obj_SearchType = document.getElementsByName("SearchType");
//    if( obj_SearchType[0].checked )
//    {
//        url += ("?&AccDBId="+accID);
//        url += ("?&SearchType="+accID);
//    }
//    else if( obj_SearchType[1].checked )
//    {
//        
//    }
//   
//   var obj_memo = document.getElementsByName("MemoContent");
//   if( obj_memo[0].value.length >= 1 )
//      url += ("&MemoContent="+obj_memo[0].value);
//   
//   window.navigate(url);
//}

function java_MemoDelete()
{
    var url = this.location;
    
    var checkBoxes = document.getElementsByName("MemoCheckBox");
    
    var delIds = "&MemoDelIds=";
    for( var i = 0 ; i < checkBoxes.length; i++ )
    {
        if( checkBoxes[i].checked )
        {
            delIds += checkBoxes[i].value + ",";
        }
    }
    
    if( checkBoxes.length > 0 )
        url += delIds;
        
        
        

    
   NavigateMe(url);
}

function java_MemoDelete2( url )
{
    url += "?";
    
    var objVal;
    
    objVal = document.getElementsByName("AccDBId");
    if( objVal[0].value.length >= 1 )
        url += "&AccDBId=" + encodeURI(objVal[0].value);

    objVal = document.getElementsByName("ServerNo");
    if( objVal[0].value.length >= 1 )
        url += "&ServerNo=" + encodeURI(objVal[0].value);

    objVal = document.getElementsByName("UserName");
    if( objVal[0].value.length >= 1 )
        url += "&UserName=" + encodeURI(objVal[0].value);

    objVal = document.getElementsByName("GMAccount");
    if( objVal[0].value.length >= 1 )
        url += "&GMAccount=" + encodeURI(objVal[0].value);
    
    var checkBoxes = document.getElementsByName("MemoCheckBox");
    
    var delIds = "&MemoDelIds=";
    for( var i = 0 ; i < checkBoxes.length; i++ )
    {
        if( checkBoxes[i].checked )
        {
            delIds += checkBoxes[i].value + ",";
        }
    }
    
    if( checkBoxes.length > 0 )
        url += delIds;
        
   NavigateMe(url);
}


function java_MemoModify( baseUrl, memoID, inputType , warnMsg1,warnMsg2,warnMsg3,warnMsg4 )
{
    var url = baseUrl;
    
    url += ("?&MemoID=" + memoID);
    
    var bValid1 = false;
    
    var objVal = document.getElementsByName("AccDBId");
    if( objVal[0].value.length >= 1 )
    {
        if( objVal[0].value > 0 )
        {
            url += "&AccDBId=" + objVal[0].value;
            bValid1 = true;
        }
    }
    
    
        
    var bValid3 = false;

    objVal = document.getElementsByName("ServerName");
    if( objVal[0].value.length >= 1 )
        url += "&ServerName=" + encodeURI(objVal[0].value);

    

    objVal = document.getElementsByName("UserName");
    if( objVal[0].value.length >= 1 )
    {
        url += "&UserName=" + encodeURI(objVal[0].value);
        bValid3 = true;
    }
    
    /// 캐릭이름만 들어가면 거기서 계정 dbid는 알수있으니까  반대로 계정에다가 메모를 남길수도 있으니까
    /// 캐릭이름이나 계정 dbid나 둘중 하나만 있으면 된다
    if ( bValid3 == false && bValid1 == false)
        window.alert(warnMsg1); ///< 이건 계정 dbid가 없다는 에라.
        
//    if( !bValid3 )
//        window.alert(warnMsg3); ///< 이건 캐릭터 이름이 이상하다는 에라.
    
    objVal = document.getElementsByName("SanctionType");
    if( objVal.length >= 1 && objVal[0].value.length >= 1 )
        url += "&SanctionType=" + encodeURI(objVal[0].value);

    var bValid2 = false;

    objVal = document.getElementsByName("SanctionContent");
    if( objVal[0].value.length >= 1 )
    {
    
        var convertStr = new String(objVal[0].value);
        
        /// \b \t로 해줘야 하네
        convertStr = UrlSafeString(convertStr);
        
        url += "&SanctionContent=" + encodeURI(convertStr);
        bValid2 = true;
    }
    else
    {
        url += "&SanctionContent=";
    }

    /// 제재 내용은 없을수도 있다    
//    if( !bValid2 )
//        window.alert(warnMsg2);

    var bValid4 = false;

    objVal = document.getElementsByName("MemoContent");
    if( objVal[0].value.length >= 1 )
    {
        var convertStr = new String(objVal[0].value);

        /// \b \t로 해줘야 하네
        convertStr = UrlSafeString(convertStr);

        url += "&MemoContent=" + encodeURI(convertStr);
        
        bValid4 = true;
    }
    
    if( !bValid4 )
        window.alert(warnMsg4);


    /// 계정 dbid나 캐릭이름은 둘중하나만 있으면 된다
        /// 제재 내용은 없을수도 있다   if( (bValid1 || bValid3 ) && bValid2 && bValid4 )
    if( (bValid1 || bValid3 ) && bValid4 )
    {
        url += ("&InputType=" + inputType);
        NavigateMe(url);
    }
    else 
    {
        NavigateMe(baseUrl);
    }
}


function java_SearchUser( url, warningMsg1,warningMsg2 )
{
    var obj_ServerList = document.getElementsByName("ServerNo");
    var obj_SearchType = document.getElementsByName("SearchType");
    
    {
      var targetUrl = url + "?" ;
      var objTemp;
      
      objTemp = document.getElementsByName("ServerNo");
      if( objTemp[0].value.length >= 1 )
        targetUrl += "&ServerNo=" + objTemp[0].value;
        
      var checkIdx = 0;
      var bValid = false;
      
      if( obj_SearchType[0].checked )
      {
          objTemp = document.getElementsByName("UserDBId");
          if( objTemp[0].value.length >= 1 )
          {
             targetUrl += "&UserDBId=" + encodeURI(objTemp[0].value);
             bValid = true;
             checkIdx = 0;
          }
          else
          {
            window.alert(warningMsg1);
          }
      }
      else if( obj_SearchType[1].checked )
      {
          objTemp = document.getElementsByName("UserName");
          if( objTemp[0].value.length >= 1 )
          {
            targetUrl += "&UserName=" + encodeURI(objTemp[0].value);
            bValid = true;
            checkIdx = 1;
          }
          else
          {
            window.alert(warningMsg2);
          }
      }
      
      objTemp = document.getElementsByName("SearchType");
      targetUrl += "&SearchType=" + (checkIdx);
      
      if( bValid )
      {
          NavigateMe(targetUrl);
      }
      
    }
}

/// 아이템 더하기 물어보기 창 같은 곳에서 질문 해보고 true면 navigate하기
function java_NavigateAskConfirm(url , askMsg )
{
    var converUrl = new String(url);
    
    /// 여기서 변환은 필요없다 이 거 불리기 앞에서 c#에서 해줬을거다    

    var confirmed = window.confirm(askMsg);
    if ( StringCompare(confirmed, 'true') == true)
    {
        converUrl += "&Confirm=true";
        
        NavigateMe(converUrl);

    }
}

/// 아이템 더하기 물어보기 창 같은 곳에서 질문 해보고 true면 navigate하기
function java_NavigateAskConfirmDialog(url , askMsg )
{
    var converUrl = new String(url);
    
    /// 여기서 변환은 필요없다 이 거 불리기 앞에서 c#에서 해줬을거다    

    var confirmed = window.confirm(askMsg);
    if ( StringCompare(confirmed, 'true') == true)
    {
        converUrl += "&Confirm=true";
        document.all.lnkNavigate.href = converUrl;
        document.all.lnkNavigate.click();
    }
}



/// AskToOperator 창에서 한번더 물어보고 서버로 confirm 보내자
function java_NavigateAskToOperatorDialog(url , askMsg )
{
    var converUrl = new String(url);
    
    /// 여기서 변환은 필요없다 이 거 불리기 앞에서 c#에서 해줬을거다    

    var confirmed = window.confirm(askMsg);
    if ( StringCompare(confirmed, 'true') == true)
    {
        converUrl += "&Confirm=1";
        document.all.lnkNavigate.href = converUrl;
        document.all.lnkNavigate.click();
    }
}

/// 아이템 더하기 물어보기 창 같은 곳에서 질문 해보고 true면 navigate하기
function java_AskConfirmDoOperator(url, askMsg) {
    var targetUrl = new String(url);

    /// 여기서 변환은 필요없다 이 거 불리기 앞에서 c#에서 해줬을거다    

    var confirmed = window.confirm(askMsg);
    if (StringCompare(confirmed, 'true') == true) {
        targetUrl += "&Confirm=true";
        window.showModalDialog(targetUrl, false, "dialogHeight:800px; dialogWidth:1280px; resizable: on")
        this.location.reload();
    }
    else
        this.location.reload();
}



/// 아이템 삭제 물어보고 같은 창에 navigate - 단 체크 된 것이 없으면 경고창 띄우고 리턴
function java_DeleteAskConfirmDialog(url , askMsg )
{


    /// 저장할 memo content
    var strMemoContent = GetElementValue("MemoContent");
    
    url += "&MemoContent=" + encodeURI(strMemoContent);
    
    
    
    // 체크된것이 있는지 확인

    // 체크된 것이 ...
    var confirmed = window.confirm(askMsg);
    if ( StringCompare(confirmed, 'true') == true)
    {
        url += "&Confirm=true";
        document.all.lnkNavigate.href = url;
        document.all.lnkNavigate.click();
    }
}



//function java_ShowModal( returnUrl, targetUrl )
//{
//    window.showModalDialog(targetUrl, false,  "dialogHeight:600px; dialogWidth:800px")
//    
//    
//    window.navigate(returnUrl);
//    

//}


/// modal dialog를 띄워주고 그 게 닫힐때는 띄웠던 원 창을 reload해준다
function java_ShowModalAndReload( targetUrl )
{
    window.showModalDialog(targetUrl, false,  "dialogHeight:800px; dialogWidth:1280px; resizable: on")
            
    this.location.href = window.location.href;
}

function java_ShowModalAndRedirect(targetUrl, redirect) {
    window.showModalDialog(targetUrl, false, "dialogHeight:800px; dialogWidth:1280px; resizable: on")

    this.location.replace(redirect);
}

/// targetUrl로 window를 하나 더 열고 기존의 창은 reload해준다
function java_OpenUrlAndReload(targetUrl) {
    window.open(targetUrl);

    this.location.reload();
}

function java_CloseMe()
{
    window.close();
}

function ReloadMe()
{
    this.location.reload();
}

function java_AskDeleteItem( targetUrl , paramName )
{
    var checkBoxes = document.getElementsByName(paramName);
    
    var checkedIds = "";
    for( var i = 0 ; i < checkBoxes.length; i++ )
    {
        if( checkBoxes[i].checked )
            checkedIds += checkBoxes[i].value + ",";
    }
    
    if( checkedIds.length > 0 )
    {
        targetUrl += ( "&CheckedItems=" + checkedIds );
        java_ShowModalAndReload( targetUrl );
    }
    else
    {
        // 일단 체크된거 없으면 그냥 refresh
        this.location.reload();
    }
}

function java_AskDeleteItemAndRedirect(targetUrl, redirect, paramName) {
    var checkBoxes = document.getElementsByName(paramName);

    var checkedIds = "";
    for (var i = 0 ; i < checkBoxes.length; i++) {
        if (checkBoxes[i].checked)
            checkedIds += checkBoxes[i].value + ",";
    }

    if (checkedIds.length > 0) {
        targetUrl += ("&Ck=" + checkedIds);
        java_ShowModalAndRedirect(targetUrl, redirect);
    }
    else {
        // 일단 체크된거 없으면 그냥 refresh
        this.location.reload();
    }

}


/// 계정 복구 기능에서 사용. 복구 해줄 삭제된 아이템/돈 선택하기
function java_AskRestoreDeletedItem(targetUrl, paramName) {



    java_AskDeleteItem(targetUrl, paramName);

}


/// checkBox가 없을때 결재창 열어 주는 dialog 실행
function java_OpenAskToOperatorNoCheckBox(targetUrl ) {

    java_ShowModalAndReload(targetUrl);

}


/// checkBox가 있을때 결재창 열어 주는 dialog 실행
function java_OpenAskToOperator(targetUrl, paramName) {
    java_AskDeleteItem(targetUrl, paramName);
}

/// checkBox가 있을때 결재창 열어 주고 지정된 url로 Reload하도록 한다.
function java_OpenAskToOperatorAndRedirection(targetUrl, redirect, paramName) {
    java_AskDeleteItemAndRedirect(targetUrl, redirect, paramName);
}

function java_OpenAskToChangeAttendance(targetUrl, paramName) {
    var checkBoxes = document.getElementsByName(paramName);
    
    var checkedIds = "";
    for( var i = 0 ; i < checkBoxes.length; i++ )
    {
        if( checkBoxes[i].checked )
            checkedIds += checkBoxes[i].value + ",";
    }
    

    targetUrl += ( "&CheckedItems=" + checkedIds );
    java_ShowModalAndReload( targetUrl );

}

function java_AskDeleteSkill( targetUrl , paramName )
{
    var checkBoxes = document.getElementsByName(paramName);
    
    var checkedIds = "";
    for( var i = 0 ; i < checkBoxes.length; i++ )
    {
        if( checkBoxes[i].checked )
            checkedIds += checkBoxes[i].value + ",";
    }
    
    if( checkedIds.length > 0 )
    {
        targetUrl += ( "&CheckedSkills=" + checkedIds );
        java_ShowModalAndReload( targetUrl );
    }
    else
    {
        // 일단 체크된거 없으면 무시
        this.location.reload();
    }
}


function java_AskDeleteTradeBrokerItem( targetUrl , paramName )
{
    var checkBoxes = document.getElementsByName(paramName);
    
    var checkedIds = "";
    for( var i = 0 ; i < checkBoxes.length; i++ )
    {
        if (checkBoxes[i].checked)
        {
            if (checkedIds != "")
                checkedIds += ",";

            checkedIds += checkBoxes[i].value;
        }
    }
    
    if( checkedIds.length > 0 )
    {
        targetUrl += ( "&TBCheckedIds=" + checkedIds );
        java_ShowModalAndReload( targetUrl );
    }
    else
    {
        // 일단 체크된거 없으면 무시
        this.location.reload();
    }
}


function java_AskAddSkill( targetUrl )
{

    java_ShowModalAndReload( targetUrl );

}

function java_AskDisableTerritory(targetUrl, serverNoParamName, maxList, huntingZoneIdParamNamePrefix, territoryIdParamNamePrefix, npcTemplateIdParamNamePrefix)
{
    var count = parseInt(maxList);    
    
    for (var i = 0; i < count; ++i)
    {
        var huntingZoneId = huntingZoneIdParamNamePrefix + "_" + i.toString();
        var territoryId = territoryIdParamNamePrefix + "_" + i.toString();
        var npcTemplateId = npcTemplateIdParamNamePrefix +  "_" + i.toString();
        
        targetUrl += "&" + huntingZoneId + "=" + GetElementValue(huntingZoneId);
        targetUrl += "&" + territoryId + "=" + GetElementValue(territoryId);
        targetUrl += "&" + npcTemplateId + "=" + GetElementValue(npcTemplateId);            
    }
    
    targetUrl += "&" + serverNoParamName + "=" + GetElementValue(serverNoParamName);
    
    java_ShowModalAndReload(targetUrl);
}

function java_AskSetRespawnTime(targetUrl, serverNoPN, huntingZoneIdPN, territoryIdPN, requestTypePN, requestIdPN)
{
    targetUrl += "&" + serverNoPN + "=" + GetElementValue(serverNoPN);
    targetUrl += "&" + huntingZoneIdPN + "=" + GetElementValue(huntingZoneIdPN);
    targetUrl += "&" + territoryIdPN + "=" + GetElementValue(territoryIdPN);
    targetUrl += "&" + requestTypePN + "=" + GetElementValue(requestTypePN);
    targetUrl += "&" + requestIdPN + "=" + GetElementValue(requestIdPN);
    
    java_ShowModalAndReload(targetUrl);
}


/// div 영역을 펼치고 숨기고 기능
function layer_toggle(obj)
{

    if(obj.style.display == 'none')
        obj.style.display = 'block';
    else if ( obj.style.display == 'block')
        obj.style.display = 'none';
        

}

function ClickCheckboxInputAndUncheckOthers(idName)
{  
    if (window.event.srcElement.checked) {
        SetInputChecked(idName);
    }
    else {
        SetInputUnChecked(idName);
    }

    for (i = 0; i < document.all.length; i++) {
        var onenode = document.all[i];

        var nodeid = String(onenode.id).toLowerCase();

        if (nodeid.search(idName) != 0) {
            onenode.checked = false;
        }
    }
}

/// chechbox input을 클릭할때 이벤트 처리해줄거다
function ClickCheckboxInput(idName)
{


    if ( window.event.srcElement.checked  )
    {
        SetInputChecked(idName);
    }
    else
    {
        SetInputUnChecked(idName);
    }
}


/// chechbox input을 클릭할때 이벤트 처리해줄거다
function ClickCheckboxAllInput(idName, changeParentIdName)
{

    if ( window.event.srcElement.checked  )
    {
        SetInputChecked(idName);
        
        SetInputChecked(changeParentIdName);
    }
    else
    {
        SetInputUnChecked(idName);
        
        SetInputUnChecked(changeParentIdName);
    }
}


/// chechbox input을 클릭할때 이벤트 처리해줄거다
function ClickCheckboxDivInput(idName, changeIdName)
{

//window.alert(idName);

//window.alert(window.event.srcElement);
//window.alert(window.event.srcElement.checked);

    var found = document.getElementById(idName);
    
    if ( found)
    {
        if ( found.checked == true)
        {
            found.checked = false;
            SetInputUnChecked(changeIdName);
        }
        else
        {
            found.checked = true;
            SetInputChecked(changeIdName);
        }
    }

}



/// chechbox input을 클릭할때 이벤트 처리해줄거다
function ClickCheckboxAllDivInput(idName, changeIdName, changeParentIdName)
{

//window.alert(idName);

//window.alert(window.event.srcElement);
//window.alert(window.event.srcElement.checked);

    var found = document.getElementById(idName);
    
    if ( found)
    {
        if ( found.checked == true)
        {
            found.checked = false;
            SetInputUnChecked(changeIdName);
            
            SetInputUnChecked(changeParentIdName);
        }
        else
        {
            found.checked = true;
            SetInputChecked(changeIdName);
            
            SetInputChecked(changeParentIdName);
        }
    }

}

/// checkbox 로 된 input들 중에서 idName으로 시작하는 것들을 다 checked로 해놓자
function SetInputChecked(idName)
{

//window.alert('SetInputChecked');
//window.alert(idName);

/// id checked 
    for(i = 0; i < document.all.length; i++)
    {
        var onenode = document.all[i];
        
        var nodeid = String(onenode.id).toLowerCase();
        
        if ( nodeid.search(idName) == 0)
        {
            onenode.checked = true;
        }
        
    }


}

function SetInputUnCheckedIfNameDiffersFrom(idName)
{
    for (i = 0; i < document.all.length; i++) {
        var onenode = document.all[i];

        var nodeid = String(onenode.id).toLowerCase();

        if (nodeid.search(idName) != 0) {
            onenode.checked = false;
        }

    }
}

/// checkbox 로 된 input들 중에서 idName으로 시작하는 것들을 다 checked를 해제
function SetInputUnChecked(idName)
{

//window.alert('SetInputUnChecked');
//window.alert(idName);

/// id checked 
    for(i = 0; i < document.all.length; i++)
    {
        var onenode = document.all[i];
        
        var nodeid = String(onenode.id).toLowerCase();
        
        if ( nodeid.search(idName) == 0)
        {
            onenode.checked = false;
        }
        
    }


}


/// chechBoxIdName에 해당하는 녀석을 찾아서 그게 checked == false면 idName에 해당하는걸 checked==false로 한다
function ClickSearchActionDivCheckBox(checkBoxIdName, idName)
{

//window.alert(checkBoxIdName);
//window.alert(window.event.srcElement);
//window.alert(window.event.srcElement.checked);

    var checkBoxNode = document.getElementById(checkBoxIdName);
    if ( checkBoxNode )
    {
        var foundnode = document.getElementById(idName);
        if ( checkBoxNode.checked == true )
        {
            checkBoxNode.checked = false;
            foundnode.checked = false;
        }
        else
        {
            checkBoxNode.checked = true;
        }
    
    }

}



/// 이 checkbox input를 클릭하면 idName에 해당하는 checkbox를 unchecked 로 만들어주자
function ClickSearchActionCheckBox(idName)
{

//window.alert(idName);
//window.alert(window.event.srcElement);
//window.alert(window.event.srcElement.checked);
    if ( window.event.srcElement.checked == false )
    {

        var foundnode = document.getElementById(idName);
        
        if ( foundnode)
        {
            foundnode.checked = false;
        }

    }

}




/// form에서 elementName에 해당하는 것의 내용을 찾아준다
function GetElementValue(elementName)
{
    var obj_found = document.getElementsByName(elementName);
    
    if( obj_found.length == 1)
    {
        return obj_found[0].value;
        
    }
    
    
    return "";
}

function replaceAll(srcStr,replaceStr, with_this)
{
    return srcStr.replace(new RegExp(replaceStr, 'g'), with_this);
}

/// window.showModalDialog 할때는 url에 < 나 >  가 있으면 안되네 \b \t 로 바꾸자
function UrlSafeString(convertStr)
{
    convertStr = replaceAll(convertStr, "<", "\b");
    convertStr = replaceAll(convertStr, ">", "\t");
    convertStr = replaceAll(convertStr, "#", "%23");
    convertStr = replaceAll(convertStr, "&", "%26");
    convertStr = replaceAll(convertStr, "\\+", "%2B"); // 정규표현식을 사용하기 때문에 슬래쉬 2개 필요
      
    return convertStr;
    
}
    
    
function onSelectQuest(selectVal,Url)
{
    document.all.lnkNavigate.href = Url + selectVal;
    document.all.lnkNavigate.click();
}
        
        
function java_AskCheckQuest( targetUrl , paramName )
{
    var checkBoxes = document.getElementsByName(paramName);
    
    var checkedIds = "";
    for( var i = 0 ; i < checkBoxes.length; i++ )
    {
        if( checkBoxes[i].checked )
            checkedIds += checkBoxes[i].value + ",";
    }
    
    if( checkedIds.length > 0 )
    {
        targetUrl += ( "&CheckedQuests=" + checkedIds );
        java_ShowModalAndReload( targetUrl );
    }
    else
    {
        // 일단 체크된거 없으면 무시
        this.location.reload();
    }
}

function java_LoadDropdownAndShowModal(targetUrl, dropdowns, radios )
{
    var dropDownLists = [];
    paramNames = dropdowns.split(",");
    for (var i = 0; i < paramNames.length; i++)
    {
        var d = document.getElementsByName(paramNames[i])[0];
        dropDownLists[i] = d;
    }

    for (var i = 0; i < dropDownLists.length; i++)
    {
        targetUrl += ("&" + paramNames[i] + "=" + dropDownLists[i].options[dropDownLists[i].selectedIndex].value);
    }

    var radioButtons = document.getElementsByName(radios);
    var selected = -1;
    for (var i = 0; i < radioButtons.length; i++) {
        if (radioButtons[i].checked) {
            selected = radioButtons[i].value;
        }
    }

    targetUrl += ("&" + radios + "=" + selected);

    java_ShowModalAndReload(targetUrl);
}
       
function java_EditAskConfirmDialog(url , askMsg ,paramName)
{
    /// 저장할 memo content
    var strMemoContent = GetElementValue("MemoContent");    
    url += "&MemoContent=" + encodeURI(strMemoContent);
        
    var selects = document.getElementsByName(paramName);
    
    url+= "&SelectTask=";
    for( var i = 0 ; i < selects.length; i++ )
    {        
        
        url += selects[i].key +"/"+selects[i].options[selects[i].selectedIndex].value+",";
    }
    
    var confirmed = window.confirm(askMsg);
    if ( StringCompare(confirmed, 'true') == true)
    {
        url += "&Confirm=true";
        document.all.lnkNavigate.href = url;
        document.all.lnkNavigate.click();
    }
}




function buttonClicked(objButton)
{



    var objForm = objButton.form;
    objButton.disabled = true;
    

}

function askBeforeSubmitForm(objButton, askMessage)
{


alert("askBeforeSubmitForm");

    
    var objForm = objButton.form;
    objButton.disabled = true;

    var confirmed = window.confirm(askMessage);
    if ( StringCompare(confirmed, 'true') == true)
    {
    

        if ( objForm != null)
            objForm.submit();

    }
    else
    {

        window.event.returnValue = false;
        objButton.disabled = false;
       
    }


}


function NeedMessageAskBeforeSubmitForm(contentName, alertMsg, objButton, askMessage)
{

    
    var messageObject = document.getElementsByName(contentName);
    
    
    var isValidMessage = true;
    if ( messageObject != null && messageObject.length == 1)
    {
        if ( messageObject[0].value.length < 1)
        {
            isValidMessage = false;
        }        
    }
    else
    {
        isValidMessage = false;
    }
    
    
    
    if ( isValidMessage == false)
    {
        window.alert(alertMsg);

        window.event.returnValue = false;
        objButton.disabled = false;
        return;
    
    }
   
    
    
    var objForm = objButton.form;
    objButton.disabled = true;

    var confirmed = window.confirm(askMessage);
    if ( StringCompare(confirmed, 'true') == true)
    {
    

        if ( objForm != null)
            objForm.submit();

    }
    else
    {

        window.event.returnValue = false;
        objButton.disabled = false;
       
    }


}


function AskBeforeSubmitForm(contentName, alertMsg, askMessage) {
    var messageObject = document.getElementsByName(contentName);

    var isValidMessage = true;
    if ( messageObject != null && messageObject.length == 1)
    {
        if ( messageObject[0].value.length < 1)
        {
            isValidMessage = false;
        }        
    }
    else
    {
        isValidMessage = false;
    }
    
    if ( isValidMessage == false)
    {
        window.alert(alertMsg);

        window.event.returnValue = false;
        return false;    
    }    
    
    var confirmed = window.confirm(askMessage);
    if ( StringCompare(confirmed, 'true') == false)
    {
        window.event.returnValue = false;
        return false;       
    }

    return true;
}

function DoubleAskBeforeSubmitForm(contentName1, alertMsg1, contentName2, alertMsg2, askMessage) {
    var obj1 = document.getElementsByName(contentName1);
    var obj2 = document.getElementsByName(contentName2);

    var isValidMessage1 = true;
    var isValidMessage2 = true;

    if (obj1 != null && obj1.length == 1) {
        if (obj1[0].value.length < 1) {
            isValidMessage1 = false;
        }
    }
    else {
        isValidMessage1 = false;
    }

    if (obj2 != null && obj2.length == 1) {
        if (obj2[0].value.length < 1) {
            isValidMessage2 = false;
        }
    }
    else {
        isValidMessage2 = false;
    }

    if (isValidMessage1 == false) {
        window.alert(alertMsg1);

        window.event.returnValue = false;
        return false;
    }

    if (isValidMessage2 == false) {
        window.alert(alertMsg2);

        window.event.returnValue = false;
        return false;
    }

    var confirmed = window.confirm(askMessage);
    if (StringCompare(confirmed, 'true') == false) {
        window.event.returnValue = false;
        return false;
    }

    return true;
}

function TypeOfTwoAskBeforeSubmitForm(contentName1, alertMsg1, contentName2, alertMsg2, askMessage) {
    var obj1 = document.getElementsByName(contentName1);
    var obj2 = document.getElementsByName(contentName2);

    var isValidMessage1 = true;
    var isValidMessage2 = true;

    if (obj1 != null && obj1.length == 1) {
        if (obj1[0].value.length < 1) {
            isValidMessage1 = false;
        }
    }
    else {
        isValidMessage1 = false;
    }

    if (obj2 != null && obj2.length == 1) {
        if (obj2[0].value.length < 1) {
            isValidMessage2 = false;
        }
    }
    else {
        isValidMessage2 = false;
    }

    if (isValidMessage1 == false) {
        window.alert(alertMsg1);

        window.event.returnValue = false;
        return false;
    }

    if (isValidMessage2 == false) {
        window.alert(alertMsg2);

        window.event.returnValue = false;
        return false;
    }

    var confirmed = window.confirm(askMessage);
    if (StringCompare(confirmed, 'true') == false) {
        window.event.returnValue = false;
        return false;
    }

    return true;
}

function UNotificationAskBeforeSubmitForm(contentName1, alertMsg1, contentName2, alertMsg2, contentName3, alertMsg3, askMessage) {
    var obj1 = document.getElementsByName(contentName1);
    var obj2 = document.getElementsByName(contentName2);
    var obj3 = document.getElementsByName(contentName3);

    var isValidMessage1 = true;
    var isValidMessage2 = true;
    var isValidMessage3 = true;

    if (obj1 != null && obj1.length == 1) {
        if (obj1[0].value.length < 1) {
            isValidMessage1 = false;
        }
    }
    else {
        isValidMessage1 = false;
    }

    if (obj2 != null && obj2.length == 1) {
        if (obj2[0].value.length < 1) {
            isValidMessage2 = false;
        }
    }
    else {
        isValidMessage2 = false;
    }

    if (obj3 != null && obj3.length == 1) {
        if (obj3[0].value.length < 1) {
            isValidMessage3 = false;
        }
    }
    else {
        isValidMessage3 = false;
    }

    if (isValidMessage1 == false) {
        window.alert(alertMsg1);

        window.event.returnValue = false;
        return false;
    }

    if (isValidMessage2 == false) {
        window.alert(alertMsg2);

        window.event.returnValue = false;
        return false;
    }

    if (isValidMessage3 == false) {
        window.alert(alertMsg3);

        window.event.returnValue = false;
        return false;
    }

    var confirmed = window.confirm(askMessage);
    if (StringCompare(confirmed, 'true') == false) {
        window.event.returnValue = false;
        return false;
    }

    return true;
}

function TypeOfFourAskBeforeSubmitForm(contentName1, alertMsg1, contentName2, alertMsg2, contentName3, alertMsg3, contentName4, alertMsg4, askMessage) {
    var obj1 = document.getElementsByName(contentName1);
    var obj2 = document.getElementsByName(contentName2);
    var obj3 = document.getElementsByName(contentName3);
    var obj4 = document.getElementsByName(contentName4);

    var isValidMessage1 = true;
    var isValidMessage2 = true;
    var isValidMessage3 = true;
    var isValidMessage4 = true;

    if (obj1 != null && obj1.length == 1) {
        if (obj1[0].value.length < 1) {
            isValidMessage1 = false;
        }
    }
    else {
        isValidMessage1 = false;
    }

    if (obj2 != null && obj2.length == 1) {
        if (obj2[0].value.length < 1) {
            isValidMessage2 = false;
        }
    }
    else {
        isValidMessage2 = false;
    }

    if (obj3 != null && obj3.length == 1) {
        if (obj3[0].value.length < 1) {
            isValidMessage3 = false;
        }
    }
    else {
        isValidMessage3 = false;
    }

    if (obj4 != null && obj4.length == 1) {
        if (obj4[0].value.length < 1) {
            isValidMessage4 = false;
        }
    }
    else {
        isValidMessage4 = false;
    }

    if (isValidMessage1 == false) {
        window.alert(alertMsg1);

        window.event.returnValue = false;
        return false;
    }

    if (isValidMessage2 == false) {
        window.alert(alertMsg2);

        window.event.returnValue = false;
        return false;
    }

    if (isValidMessage3 == false) {
        window.alert(alertMsg3);

        window.event.returnValue = false;
        return false;
    }

    if (isValidMessage4 == false) {
        window.alert(alertMsg4);

        window.event.returnValue = false;
        return false;
    }

    var confirmed = window.confirm(askMessage);
    if (StringCompare(confirmed, 'true') == false) {
        window.event.returnValue = false;
        return false;
    }

    return true;
}

function submitButtonClicked(objButton)
{   
    var objForm = objButton.form;
    objButton.disabled = true;


    if ( objForm != null)
        objForm.submit();
        
/// window.alert("aaa");
//this.disabled = true;
//window.event.srcElement.disabled = true;
//window.event.returnValue = true;

//return true;
}

function submitEnableButtonClicked(objButton) {
    var objForm = objButton.form;
    if (objForm != null)
        objForm.submit();

    // 이거 안 하면, 다른 버튼 눌러도 계속 "엑셀 다운로드"하라고 뜬다. 
    var downloadButtons = document.getElementsByName("ExcelDownload");
    if (downloadButtons.length > 0 && downloadButtons[0].value == "1")
    {
        downloadButtons[0].value = "0";
    }

    // 프리셋 submit 초기화
    var presetActionButtons = document.getElementsByName("pa");
    if (presetActionButtons.length > 0 && presetActionButtons[0].value != "") {
        presetActionButtons[0].value = "";
    }
}


function java_RequestDoTask( targetUrl , paramName )
{
    var checkBoxes = document.getElementsByName(paramName);
    
    var checkedIds = "";
    for( var i = 0 ; i < checkBoxes.length; i++ )
    {
        if( checkBoxes[i].checked )
            checkedIds += checkBoxes[i].value + ",";
    }
    

    if( checkedIds.length > 0 )
    {
        targetUrl += ( "&tack=" + checkedIds );
        java_ShowModalAndReload( targetUrl );
    }
    else
    {
        // 일단 체크된거 없으면 다시 refresh
        
        this.location.reload();
    }
}


function java_Redirect( url )
{
   NavigateMe(url);
}

function java_RedirectAdd1Param(url, param1)
{
    url += "&" + param1 + "=" + GetElementValue(param1);

    java_ShowModalAndReload(url);
    window.close();
}

function java_RedirectAdd2Param(url, param1, param2)
{
    url += "&" + param1 + "=" + GetElementValue(param1);
    url += "&" + param2 + "=" + GetElementValue(param2);

    java_ShowModalAndReload(url);
    window.close();
}

/// 이거 불리면 Confirm 은 0 으로 해서 다시 form을 submit해준다
function selectChanged(objSelect)
{
    var objConfirm = document.getElementsByName("Confirm");
    if (objConfirm != null && objConfirm[0] != null) objConfirm[0].value = "0";

    var objAfterForm = document.getElementsByName("afsb");
    if (objAfterForm != null && objAfterForm[0] != null)  objAfterForm[0].value = "0";

    var objForm = objSelect.form;
    objForm.submit();
}

function submitWithReset(objSelect) {
    var objConfirm = document.getElementsByName("Confirm");
    if (objConfirm != null && objConfirm[0] != null) objConfirm[0].value = "0";

    var objAfterForm = document.getElementsByName("afsb");
    if (objAfterForm != null && objAfterForm[0] != null) objAfterForm[0].value = "0";

    var objReset = document.getElementsByName("reset");
    if (objReset != null && objReset[0] != null) objReset[0].value = "1";

    var objForm = objSelect.form;
    objForm.submit();
}

function removeClientElement(removeElementId) {
    var objElement = document.getElementById(removeElementId);
    if (objElement == null) {
        /// dialogContentHolder_ 를 masterPage에서 사용해서 아래처럼 해도 된다
        objElement = document.getElementById("dialogContentHolder_" + removeElementId);
    }
    if (objElement == null) {
        /// mainContentHolder_ 를 masterPage에서 사용해서 아래처럼 해도 된다
        objElement = document.getElementById("mainContentHolder_" + removeElementId);
    }
    if (objElement != null) {
        objElement.parentNode.removeChild(objElement);
    }
}

function java_AskCheckBox( targetUrl , paramName )
{

    var checkBoxes = document.getElementsByName(paramName);


    var checkedIds = "";
    for( var i = 0 ; i < checkBoxes.length; i++ )
    {
        if( checkBoxes[i].checked )
            checkedIds += checkBoxes[i].value + ",";
    }
    
    if( checkedIds.length > 0 )
    {
        targetUrl += ( "&checked=" + checkedIds );
        java_ShowModalAndReload( targetUrl );
    }
    else
    {
        // 일단 체크된거 없으면 무시
        this.location.reload();
    }
}


function CheckAnnounceToAll(form)
{
    //var elements = document.getElementsByTagName("input");

    var elements = form.elements;

    for (var i = 0; i < elements.length; ++i)
    {
        if (elements[i].type == "checkbox" && elements[i].name.search("server_") >= 0)
        {
            elements[i].checked = form.everything__.checked;
        }
    }
}

function java_EventStartStop(url, serverNoStr, serverNo, selectServerNoStr, selectServerNo, eventIdStr, eventId, starstopStr, starstop) 
{
    url += "?" + serverNoStr + "=" + serverNo;
    url += "&" + selectServerNoStr + "=" + selectServerNo;
    url += "&" + eventIdStr + "=" + eventId;
    url += "&" + starstopStr + "=" + starstop;

    java_Redirect(url);
}

function java_EventUpdate(form, url, serverNoStr, serverNo, selectServerNoStr, selectServerNo, eventIdStr, eventId, paramTypeStr, paramType, paramValueStr)
{
    var elements = form.elements;
    var paramValue;
    for (var i = 0; i < elements.length; ++i) {
        if (elements[i].type == "text" && elements[i].name == selectServerNo.toString()) {
            paramValue = elements[i].value;
            break;
        }
    }

    url += "?" + serverNoStr + "=" + serverNo;
    url += "&" + selectServerNoStr + "=" + selectServerNo;
    url += "&" + eventIdStr + "=" + eventId;
    url += "&" + paramTypeStr + "=" + paramType;
    url += "&" + paramValueStr + "=" + paramValue;

    java_Redirect(url);
}

function java_ExcelDownload(obj1, obj2, obj3, obj4, obj5, obj6) {
    document.getElementsByName("ExcelDownload")[0].value = obj1;
    return true;
}

function logPresetAction(actionValue)
{
    document.getElementsByName("pa")[0].value = actionValue;
    return true;
}

function java_OnLeagueSelect(leagueValue)
{
    if (leagueValue == 0) {
        $("select[name=contentId]").children('option').prop('disabled', false);
        return true;
    }

    $("select[name=contentId]").val('0');
    $("select[name=contentId]").children('option').prop('disabled', true);
    $("select[name=contentId]").children("option[league*=" + leagueValue + "]").prop('disabled', false);

    return true;
}

function java_DoPostMethod(paramKey, paramValue) {
    document.getElementsByName(paramKey)[0].value = paramValue;
    return true;
}