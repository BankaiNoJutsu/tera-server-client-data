﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="SearchChangeNameByOldName.aspx.cs" Inherits="WebApp.Account.SearchChangeNameByOldName" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form runat="server" id="resultForm">

        <table class="nolineTable" >
        <tr>
            <td> <%= WebApp.DisplayString.ServerChoice %> </td>
            <td> <asp:DropDownList runat="server" ID="NewServerDDL" DataTextField="ServerName" DataValueField="ServerNo" />
        </tr>
        <tr>
            <td> <%= WebApp.DisplayString.UserName %> </td>
            <td> <asp:TextBox runat="server" ID="OldNameTB" />
        </tr>
        <tr>
             <td> <asp:Button runat="server" ID="SubmitButton" OnClick="SubmitButton_Click" /> </td>
        </tr>
        </table>

        <asp:GridView CssClass="lineTable" ID="ChangeNameHistoryGV" runat="server" AutoGenerateColumns="false" ShowHeader="true">
        <Columns>
            <asp:TemplateField>
                <HeaderTemplate> <%= WebApp.DisplayString.OldUserName%> </HeaderTemplate>
                <ItemTemplate> <%# Eval("OldUserName") %> </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate> <%= WebApp.DisplayString.NewUserName%> </HeaderTemplate>
                <ItemTemplate> <a href="<%# Eval("UserURL") %>"><%# Eval("NewUserName")%></a> </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate> <%= WebApp.DisplayString.AccountName%> </HeaderTemplate>
                <ItemTemplate> <a href="<%# Eval("AccountURL") %>"><%# Eval("AccountName")%></a> </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate> <%= WebApp.DisplayString.RenameTime%> </HeaderTemplate>
                <ItemTemplate> <%# Eval("RenameTime")%> </ItemTemplate>
            </asp:TemplateField>
        </Columns>
        </asp:GridView>

    </form>

</asp:Content>
