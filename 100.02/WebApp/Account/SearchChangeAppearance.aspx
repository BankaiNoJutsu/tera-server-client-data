﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="SearchChangeAppearance.aspx.cs" Inherits="WebApp.Account.SearchChangeAppearance" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form runat="server" id="resultForm" >

        <table class="nolineTable">
            <tr>
                <td> <%= WebApp.DisplayString.ServerChoice %> </td>
                <td> <asp:DropDownList runat="server" ID="ServNoDropDownList" DataTextField="Text" DataValueField="Value" /> </td>
            </tr>
            <tr>
                <td> <%= WebApp.DisplayString.ChangeAppearanceType %></td>
                <td> <asp:DropDownList runat="server" ID="ChangeAppearanceTypeDropDownList" DataTextField="Text" DataValueField="Value"/> </td>
            </tr>
            <tr>
                <td> <%= WebApp.DisplayString.UserDBID %></td>
                <td> <asp:TextBox runat="server" ID="UserDbIdTextBox" /> </td>
            </tr>
            <tr>
                <td> <asp:Button runat="server" ID="SubmitButton" OnClick="SubmitButton_Click" /> </td>
            </tr>       
        </table>

        <asp:Panel runat="server" ID="resultPanel">
        
        <table class="lineTable">
            <tr>
                <th> <%= WebApp.DisplayString.UserDBID %></th>
                <th> <%= WebApp.DisplayString.ChangeAppearanceType %></th>
                <th> <%= WebApp.DisplayString.ChangeDate %></th>
                <th> <%= WebApp.DisplayString.OldRace %></th>
                <th> <%= WebApp.DisplayString.NewRace %></th>
                <th> <%= WebApp.DisplayString.OldGender %></th>
                <th> <%= WebApp.DisplayString.NewGender %></th>
                <!--
                <th style = "width:180px"> <%= WebApp.DisplayString.OldLooks %></th>
                <th style = "width:180px"> <%= WebApp.DisplayString.NewLooks%></th>
                -->
            </tr>
            <asp:Repeater runat="server" ID="historyRepeater">
            <ItemTemplate>
            <tr>
                <td> <%# DataBinder.GetPropertyValue(Container.DataItem, "UserDbIdStr") %> </td>
                <td> <%# DataBinder.GetPropertyValue(Container.DataItem, "ChangeTypeStr")%> </td>
                <td> <%# DataBinder.GetPropertyValue(Container.DataItem, "ChangeDateStr") %> </td>
                <td> <%# DataBinder.GetPropertyValue(Container.DataItem, "OldRaceStr") %> </td>
                <td> <%# DataBinder.GetPropertyValue(Container.DataItem, "NewRaceStr") %> </td>
                <td> <%# DataBinder.GetPropertyValue(Container.DataItem, "OldGenderStr") %> </td>
                <td> <%# DataBinder.GetPropertyValue(Container.DataItem, "NewGenderStr") %> </td>
                <!--
                <td style = "width:180px"> <%# DataBinder.GetPropertyValue(Container.DataItem, "OldCustomizingStr") %> </td>
                <td style = "width:180px"> <%# DataBinder.GetPropertyValue(Container.DataItem, "NewCustomizingStr") %> </td>
                -->
            </tr>
            </ItemTemplate>
            </asp:Repeater>
        </table>

        </asp:Panel>

    </form>

</asp:Content>
