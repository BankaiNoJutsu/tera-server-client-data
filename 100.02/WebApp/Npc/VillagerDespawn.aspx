﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="VillagerDespawn.aspx.cs" Inherits="WebApp.Npc.VillagerDespawn" %>
<asp:Content ID="Content3" ContentPlaceHolderID="headerContentHolder" runat="server">
	<style type="text/css">
	</style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="mainContentHolder" runat="server">
	<form id="Form1" runat="server">
		<table class="lineTable" style="margin-bottom:10px;">
			<tr>
				<th><%# WebApp.DisplayString.ServerChoice %></th>
				<th><%# WebApp.DisplayString.Search %></th>
			</tr>
			<tr>
				<td><asp:DropDownList runat="server" ID="ServerSelect" DataSource="<%# WebApp.WebAdminDB.ServerListWithAll.Select(x => new { ServerName = x.mName, ServerNo = x.mNo }) %>" DataTextField="ServerName" DataValueField="ServerNo"/></td>
				<td><asp:Button runat="server" ID="SelectOption" Text="Search" OnClick="SelectOption_Click" /></td>
			</tr>
		</table>
		<table class="lineTable ">
			<asp:Repeater runat="server" ID="VillagerDespawnList">
				<HeaderTemplate>
					<tr>
                        <th><asp:CheckBox runat="server" ID="VillagerCheckAll" AutoPostBack="true" OnCheckedChanged="OnVillagerCheckAll"/></th>
						<th><%# WebApp.DisplayString.ServerName %></th>
						<th><%# WebApp.DisplayString.ZoneInfo %></th>
						<th><%# WebApp.DisplayString.NpcInfo %></th>
					</tr>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
                        <td><asp:CheckBox runat="server" ID="VillagerCheck" /><asp:HiddenField runat="server" ID="ElementId" Value='<%# Eval("ServerNo") + " " + Eval("HuntingZoneId") + " " + Eval("TemplateId") %>'/></td>
						<td><%# Eval("ServerName") %></td>
						<td><%# Eval("HuntingZoneStr") %></td>
                        <td><%# Eval("TemplateIdStr") %></td>
					</tr>
				</ItemTemplate>
			</asp:Repeater>
		</table>
        
        <div id="controlButtons">
			<asp:Button runat="server" ID="AddDespawn" CommandName="AddDespawn" OnCommand="OnAddDespawn" Text="<%# WebApp.DisplayString.AddVillagerDespawn %>"/>
            <asp:Button runat="server" ID="DeleteDespawn" CommandName="DeleteDespawn" OnCommand="OnDeleteDespawn" Text="<%# WebApp.DisplayString.DeleteVillagerDespawn %>"/>
		</div>

	</form>
</asp:Content>
