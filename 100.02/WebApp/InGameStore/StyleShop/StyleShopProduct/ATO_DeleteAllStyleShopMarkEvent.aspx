﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/AppCode/DataSetAskDialog.master" CodeBehind="ATO_DeleteAllStyleShopMarkEvent.aspx.cs" Inherits="WebApp.InGameStore.StyleShop.StyleShopProduct.ATO_DeleteAllStyleShopMarkEvent" %>


<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
	<asp:Label runat="server" Text="<%# WebApp.DisplayString.RESET_ALL_PRODUCT_MARK_EVENT %>" />
    <br />

    <div class="ato-serverlist">
		<label><%# WebApp.DisplayString.CheckServer %></label>
        <asp:CheckBox runat="server" Text='<%# WebApp.DisplayString.SelectAll %>' ID="CheckAllServer" AutoPostback="true" OnCheckedChanged="OnSelectAllServers"/>	
		<ul>
			<asp:Repeater runat="server" ID="ServerCheckList">
				<ItemTemplate>
					<li>
                        <asp:CheckBox runat="server" ID="ServerName" Text='<%# Eval("ServerName") %>' AutoPostback="true" OnCheckedChanged="OnSelectServer" />
                        <asp:HiddenField runat="server" ID="ServerNo" Value='<%# Eval("ServerNo") %>' />
					</li>
				</ItemTemplate>
			</asp:Repeater>
		</ul>
		<label><%# WebApp.DisplayString.CheckAllServer %></label>
	</div>
</asp:Content>