﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="VipStoreSlotOnOffControl.aspx.cs" Inherits="WebApp.ContentsControl.VipStoreSlotOnOffControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form id="Form2" runat="server" class="nolineTable">
    
        <table class="nolineTable">
            <tr>
                <td> <%= WebApp.DisplayString.ServerChoice %> </td>
                <td> <asp:DropDownList runat="server" ID="ServNoDDL" DataTextField="Text" DataValueField="Value" /> </td>
                <td> <asp:Button runat="server" ID="SearchButton" OnClick="SearchButton_Click" /> </td>
                <td> &nbsp; </td>
            </tr>
            <tr>
                <td> <%= WebApp.DisplayString.ChooseSlot %> </td>
                <td> <asp:DropDownList runat="server" ID="SlotList" DataTextField="Text" DataValueField="Value" /> </td>
            </tr>
        </table>
        <br/>
        <asp:Button runat="server" ID="SetOnButton" OnClick="SetOnButton_Click" />
        <asp:Button runat="server" ID="SetOffButton" OnClick="SetOffButton_Click" />

        <br />
        <br />
        <asp:GridView CssClass="lineTable" ID="GVSMLList" runat="server" AutoGenerateColumns="False" ShowHeader="true" ShowHeaderWhenEmpty="true">
        <Columns>
            <asp:TemplateField   HeaderStyle-Width="100">
                <HeaderTemplate><%=WebApp.DisplayString.ServerName%></HeaderTemplate>
                <ItemTemplate><%# Eval("serverName").ToString()%></ItemTemplate>
            </asp:TemplateField>
                <asp:TemplateField >
                <HeaderTemplate><%=WebApp.DisplayString.SlotId%></HeaderTemplate>
                <ItemTemplate><%# Eval("slotId").ToString()%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField >
                <HeaderTemplate><%=WebApp.DisplayString.State%></HeaderTemplate>
                <ItemTemplate><%# Eval("onOff").ToString()%></ItemTemplate>
            </asp:TemplateField>
        </Columns>
        </asp:GridView>
    </form>
</asp:Content>