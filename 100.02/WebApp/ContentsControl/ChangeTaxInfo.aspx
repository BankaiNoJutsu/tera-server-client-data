﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="ChangeTaxInfo.aspx.cs" Inherits="WebApp.ContentsControl.ChangeTaxInfo" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">
     <form runat="server" id="form">
        <table>
            <tr>
                <td>
                    <table>
                    <tr>
                        <td> <%= WebApp.DisplayString.ServerChoice %> </td>
                        <td> <asp:DropDownList runat="server" ID="ServNoDDLForView" DataTextField="Text" DataValueField="Value" /> </td>
                    </tr>
                    <tr>
                        <td> <%= WebApp.DisplayString.SelectCityWar %> </td>
                        <td> <asp:DropDownList runat="server" ID="SelectCityWar" DataTextField="mId" DataValueField="mId" /> </td>
                        <td> <asp:Button runat="server" ID="SearchButton" OnClick="SearchButton_OnClick" /> </td>
                     </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Panel runat="server" ID="ResultPanel">
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <br />
        <br />
       
    </form>
</asp:Content>