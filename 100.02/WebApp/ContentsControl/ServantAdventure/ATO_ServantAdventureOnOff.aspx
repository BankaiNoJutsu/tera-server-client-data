﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_ServantAdventureOnOff.aspx.cs" Inherits="WebApp.ContentsControl.ServantAdventure.ATO_ServantAdventureOnOff" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">
    <table class="lineTable">
        <asp:Repeater runat="server" ID="OnOffStateList">
            <HeaderTemplate>
                <tr>
                    <th><%# WebApp.DisplayString.ServerName %></th>
                    <th><%# WebApp.DisplayString.BeforeChange %></th>
                    <th><%# WebApp.DisplayString.AfterChange %></th>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><%# Eval("ServerName") %></td>
                    <td><%# (bool)Eval("BeforeChange") ? WebApp.DisplayString.On : WebApp.DisplayString.Off %></td>
                    <td><%# (bool)Eval("AfterChange") ? WebApp.DisplayString.On : WebApp.DisplayString.Off %></td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
    <script type="text/javascript">
        $(document).ready(function () {
            $('input#ContentBody_Submit').val('<%=this.Request["isEnabled"].ToLower()== "true" ? WebApp.DisplayString.EnableControl : WebApp.DisplayString.DisableControl %>')
        })
    </script>
</asp:Content>
