<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/AppCode/DataSetAskDialog.master" CodeBehind="ATO_GmEventAddReward.aspx.cs" Inherits="WebApp.ContentsControl.GmEvent.ATO_GmEventAddReward" %>
<asp:Content ID="Content0" ContentPlaceHolderID="ContentHeader" runat="server">
	<style type="text/css">
		.li-rewardItem {
			display: none;
		}
		.li-rewardTypeItem {
			display: none;
		}
	</style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
	<div class="ato-serverlist">
		<label><%# WebApp.DisplayString.CheckServer %></label>
		<ul>
			<asp:Repeater runat="server" ID="ServerCheckList">
				<ItemTemplate>
					<li><asp:CheckBox runat="server" ID="ServerName" Text='<%# Eval("ServerName") %>' /><asp:HiddenField runat="server" ID="ServerNo" Value='<%# Eval("ServerNo") %>' /></li>
				</ItemTemplate>
			</asp:Repeater>
		</ul>
		<label><%# WebApp.DisplayString.CheckAllServer %></label>
	</div>
	<div class="ato-main">
		<ul class="form-style">
			<li><label class="width100"><%# WebApp.DisplayString.RewardCategory %></label><asp:DropDownList runat="server" ID="RewardCategory" class="rewardCategory" DataTextField="Text" DataValueField="Value" /></li>
			<li class="li-rewardTypeItem" id="li-rewardId"><label class="width100"><%# WebApp.DisplayString.AdminRewardId %></label><asp:TextBox runat="server" ID="RewardId" AutoPostBack="true" OnTextChanged="RewardId_TextChanged"/></li>
			<li class="li-rewardTypeItem" id="li-rewardName"><label class="width100"><%# WebApp.DisplayString.RewardName %></label><asp:TextBox runat="server" ID="RewardName" placeholder="<%# WebApp.DisplayString.ErrorMsg_EnterRewardName %>"/></li>
			<li class="li-rewardTypeItem" id="li-eventName"><label class="width100"><%# WebApp.DisplayString.EventName %></label><asp:DropDownList runat="server" ID="GmEventSelect" DataTextField="EventName" DataValueField="EventId"/></li>
			<li class="li-rewardTypeItem" id="li-point"><label class="width100"><%# WebApp.DisplayString.Point %></label><asp:TextBox runat="server" ID="Point" type="number" /><asp:Label runat="server" ID="PointDesc"/></li>
			<li class="li-rewardTypeItem" id="li-rewardType"><label class="width100"><%# WebApp.DisplayString.RewardType %></label><asp:DropDownList runat="server" ID="RewardTypeList" class="rewardTypeList" DataValueField="TypeId" DataTextField="TypeName"/></li>
			<li class="li-rewardItem" id="li-itemTemplateId"><label class="width100"><%# WebApp.DisplayString.ItemTemplateId %></label><asp:TextBox runat="server" ID="ItemTemplateId" type="number" class="itemTemplateId" /><span id="itemName"></span></li>
			<li class="li-rewardItem" id="li-itemAmount"><label class="width100"><%# WebApp.DisplayString.ItemAmount %></label><asp:TextBox runat="server" ID="ItemAmount" type="number" /></li>
			<li class="li-rewardItem" id="li-money"><label class="width100"><%# WebApp.DisplayString.Gold %></label><asp:TextBox runat="server" ID="Money" type="number"/></li>
			<li class="li-rewardItem" id="li-exp"><label class="width100"><%# WebApp.DisplayString.Exp %></label><asp:TextBox runat="server" ID="Exp" type="number"/></li>
			<li class="li-rewardItem" id="li-valkyon"><label class="width100"><%# WebApp.DisplayString.ValkyonReputationPoint %></label><asp:TextBox runat="server" ID="Valkyon" type="number"/></li>
		</ul>

		<asp:Button runat="server" ID="AddReward" Text="<%# WebApp.DisplayString.AddReward %>" OnClick="AddReward_Click" style="margin-left: 100px;"/>

		<table class="lineTable">
			<asp:Repeater runat="server" ID="AddRewardList">
				<HeaderTemplate>
					<tr>
						<th><%# WebApp.DisplayString.RewardCategory %></th>
						<th><%# WebApp.DisplayString.AdminRewardId %></th>
						<th><%# WebApp.DisplayString.EventName %></th>
						<th><%# WebApp.DisplayString.Point %></th>
						<th><%# WebApp.DisplayString.RewardType %></th>
						<th><%# WebApp.DisplayString.RewardInfo %></th>
						<th><%# WebApp.DisplayString.DeleteFromList %></th>
					</tr>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<td><%# Eval("Category") %></td>
						<td><%# (int)Eval("RewardId") == 0 ? "" : Eval("RewardIdName") %></td>
						<td><%# (int)Eval("EventId") == 0 ? "" : Eval("EventIdName") %></td>
						<td><%# (int)Eval("EventId") == 0 ? "" : Eval("PointRange") %></td>
						<td><%# Eval("RewardTypeName") %></td>
						<td><%# Eval("RewardInfo") %></td>
						<td><asp:Button runat="server" ID="DeleteReward" CommandArgument='<%# Eval("Index") %>' OnCommand="DeleteReward_Command" Text="<%# WebApp.DisplayString.DeleteFromList %>" /></td>
					</tr>
				</ItemTemplate>
			</asp:Repeater>
		</table>
	</div>

	<script type="text/javascript">
		$(document).ready(function () {
			ShowRewardTypeItem();
			ShowRewardItem();
		})
		
		function GetItemName(templateId) {
			return InvokeWebServiceSyncByUri('/api/TeraData/GetItemName', 'id=' + templateId);
		}

		$('.itemTemplateId').keyup(function () {
			var itemName = GetItemName($(this).val())
			if (itemName == undefined || itemName == null)
				itemName = ''
			$('#itemName').text(itemName)
		})

		$('.rewardCategory').change(function () {
			ShowRewardTypeItem();
		})

		$('.rewardTypeList').change(function () {
			ShowRewardItem();
		})

		function ShowRewardTypeItem() {
			$('.li-rewardTypeItem').hide()

			switch ($('.rewardCategory').val()) {
				case '0':
					break;
				case '1':
					$('#li-rewardId').show();
					$('#li-rewardName').show();
					$('#li-rewardType').show();
					break;
				case '2':
					$('#li-eventName').show();
					$('#li-point').show();
					$('#li-rewardType').show();
					break;
			}
		}

		function ShowRewardItem()
		{
			$('.li-rewardItem').hide()

			switch ($('.rewardTypeList').val()) {
				case '0':
					break;
				case '1':
					$('#li-itemTemplateId').show()
					$('#li-itemAmount').show()
					break;
				case '2':
					$('#li-money').show()
					break;
				case '3':
					$('#li-exp').show()
					break;
				case '4':
					$('#li-valkyon').show()
					break;
			}
		}
	</script>
</asp:Content>
