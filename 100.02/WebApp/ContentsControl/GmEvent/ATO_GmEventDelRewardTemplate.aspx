<%@ Page Language="C#" MasterPageFile="~/AppCode/CustomAskDialog.Master" AutoEventWireup="true" CodeBehind="ATO_GmEventDelRewardTemplate.aspx.cs" Inherits="WebApp.ContentsControl.GmEvent.ATO_GmEventDelRewardTemplate" %>

<asp:Content ID="ContentBody" ContentPlaceHolderID="ContentBody" runat="server">
	<form id="Form2" runat="server" class="nolineTable">   
		<!-- 잘못 입력 된 값에 대한 안내 메세지  -->
		<p style="color:black; font-weight:bold"><asp:Label ID="ErrorInfo" runat="server" visible="false"/></p><br />

		<asp:Panel id="EditPanel" runat="server" visible="true">
			<table class="lineTable width-full">
				<asp:Repeater runat="server" ID="RewardList">
					<HeaderTemplate>
						<tr>
							<th><%# WebApp.DisplayString.RewardCategory %></th>
							<th><%# WebApp.DisplayString.ID %></th>
							<th><%# WebApp.DisplayString.Point %></th>
							<th><%# WebApp.DisplayString.RewardType %></th>
							<th><%# WebApp.DisplayString.RewardInfo %></th>
						</tr>
					</HeaderTemplate>
					<ItemTemplate>
						<tr>
							<td><%# Eval("Category") %></td>
							<td><%# Eval("RewardId") %></td>
							<td><%# Eval("PointRange") %></td>
							<td><%# Eval("RewardTypeName") %></td>
							<td><%# Eval("RewardInfo") %></td>
						</tr>
					</ItemTemplate>
				</asp:Repeater>
			</table>

			<!-- 메모 입력란 -->
			<p style="font-weight:bold">[<%= WebApp.DisplayString.MemoStr %>]</p><br />
			<asp:TextBox runat="server" width="250px" height="100px" ID="MemoBox" TextMode="MultiLine" style="overflow:auto"/><br /><br />    

			<!-- 확인 <%# WebApp.DisplayString.Button %> -->
			<asp:Button runat="server" ID="ConfirmButton" Text="<%# WebApp.DisplayString.DeleteItem %>" OnClick="ConfirmButton_Click" />
		</asp:Panel>

		<asp:Button runat="server" ID="CloseButton" Text="<%# WebApp.DisplayString.Close%>" Visible="false" OnClick="CloseButton_Click"/><br />
	</form>
</asp:Content>
