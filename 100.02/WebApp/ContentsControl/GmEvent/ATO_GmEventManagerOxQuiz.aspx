﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_GmEventManagerOxQuiz.aspx.cs" Inherits="WebApp.ContentsControl.GmEvent.ATO_GmEventManagerOxQuiz" %>
<asp:Content ID="Content0" ContentPlaceHolderID="ContentHeader" runat="server">
	<style type="text/css">
		.width50p {
			width: 49%;
			display: inline-block;
		}

		.td-quiz {
			max-width: 400px;
		}
	</style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
	<div class="ato-serverlist">
		<label><%# WebApp.DisplayString.CheckServer %></label>
		<ul>
			<asp:Repeater runat="server" ID="ServerCheckList">
				<ItemTemplate>
					<li><asp:CheckBox runat="server" ID="ServerName" Text='<%# Eval("ServerName") %>' /><asp:HiddenField runat="server" ID="ServerNo" Value='<%# Eval("ServerNo") %>' /></li>
				</ItemTemplate>
			</asp:Repeater>
		</ul>
		<label><%# WebApp.DisplayString.CheckAllServer %></label>
	</div>
	<div class="ato-main">
		<table class="lineTable">
			<tr>
				<th><%# WebApp.DisplayString.EventType %></th>
				<th><%# WebApp.DisplayString.EventId %></th>
				<th><%# WebApp.DisplayString.EventName %></th>
			</tr>
			<tr>
				<td><asp:Label runat="server" ID="EventType" /></td>
				<td><asp:Label runat="server" ID="EventId" /></td>
				<td><asp:Label runat="server" ID="EventName" /></td>
			</tr>
		</table>

		<div style="margin-top: 20px;">
			<div style="display:inline-block; vertical-align:top; width:600px; height:450px; max-height: 450px; overflow-y: auto; margin-right: 20px;">
				<div>
					<span><%# WebApp.DisplayString.OxQuizMainQuiz %></span>
				</div>
				<div>
					<table class="lineTable" style="width: 600px;">
						<asp:Repeater runat="server" ID="QuizList" OnItemDataBound="QuizList_ItemDataBound">
							<HeaderTemplate>
								<tr>
									<th><%# WebApp.DisplayString.QuizIndex %></th>
									<th class="td-quiz"><%# WebApp.DisplayString.QuizString %></th>
									<th><%# WebApp.DisplayString.Answer %></th>
									<th title="<%# WebApp.DisplayString.SetQuizRewardInRewardSettingPage %>">
										<%# WebApp.DisplayString.QuizReward %>
										<svg xmlns="http://www.w3.org/2000/svg" width="11" height="13" viewBox="40 30 100 100">
											<path fill="none" stroke="#000" stroke-width="16" d="M60,67c0-13 1-19 8-26c7-9 18-10 28-8c10,2 22,12 22,26c0,14-11,19-15,22c-7,2-10,6-11,11v20m0,12v16"/>
										</svg>
									</th>
									<th>Action</th>
								</tr>
							</HeaderTemplate>
							<ItemTemplate>
								<tr>
									<td><asp:TextBox runat="server" ID="QuizIndex" Text='<%# ((int)Eval("QuizIndex")) + 1 %>' style="width: 30px;" AutoPostBack="true" OnTextChanged="QuizIndex_TextChanged"/></td>
									<td class="td-quiz" title="<%# Eval("AnswerString") %>"><%# Eval("QuizString") %></td>
									<td><%# Eval("AnswerOX") %></td>
									<td><asp:DropDownList runat="server" ID="QuizRewardId" style="width: 50px;" /></td>
									<td><asp:Button runat="server" ID="DeleteQuiz" Text="<%# WebApp.DisplayString.DeleteFromList %>" CommandArgument='<%# Eval("QuizString") %>' OnCommand="DeleteQuiz_Command" /></td>
								</tr>
							</ItemTemplate>
						</asp:Repeater>
					</table>
				</div>
				<div>
					<span><%# WebApp.DisplayString.OxQuizRepechageQuiz %></span>
				</div>
				<div>
					<table class="lineTable" style="width: 600px;">
						<asp:Repeater runat="server" ID="QuizListRepe">
							<HeaderTemplate>
								<tr>
									<th class="td-quiz"><%# WebApp.DisplayString.QuizString %></th>
									<th><%# WebApp.DisplayString.Answer %></th>
									<th>Action</th>
								</tr>
							</HeaderTemplate>
							<ItemTemplate>
								<tr>
									<td class="td-quiz" title="<%# Eval("AnswerString") %>"><%# Eval("QuizString") %></td>
									<td><%# Eval("AnswerOX") %></td>
									<td><asp:Button runat="server" ID="DeleteRepeQuiz" Text="<%# WebApp.DisplayString.DeleteFromList %>" CommandArgument='<%# Eval("QuizIndex") %>' OnCommand="DeleteRepeQuiz_Command" /></td>
								</tr>
							</ItemTemplate>
						</asp:Repeater>
					</table>
				</div>
			</div>
			<div style="display:inline-block; vertical-align:top; width:600px;">
				<div style="display:block; width:600px; max-height: 400px; min-height: 400px; overflow-y: auto; margin-bottom: 10px;">
					<!-- <%# WebApp.DisplayString.NAVMENU_OxQuizTemplate_Short %>이 있는곳 -->
					<div>
						<span><%# WebApp.DisplayString.OxQuizAllList %></span>
					</div>

					<table class="lineTable" style="width: 500px; margin-right: 10px;">
						<asp:Repeater runat="server" ID="QuizTemplateList">
							<HeaderTemplate>
								<tr>
									<th><%# WebApp.DisplayString.Selected %></th>
									<th style="display:none;">Check</th>
									<th class="td-quiz"><%# WebApp.DisplayString.QuizString %></th>
									<th><%# WebApp.DisplayString.Answer %></th>
								</tr>
							</HeaderTemplate>
							<ItemTemplate>
								<tr>
									<td>
										<asp:CheckBox runat="server" ID="QuizTemplateCheckBox" />
										<asp:HiddenField runat="server" ID="QuizTemplateDbId" Value='<%# Eval("DbId") %>' />
									</td>
									<td style="display:none;"><asp:CheckBox runat="server" ID="CheckQuizTemplate"/></td>
									<td class="td-quiz" title="<%# Eval("AnswerString") %>"><%# Eval("QuizString") %></td>
									<td><%# Eval("AnswerOX") %></td>
								</tr>
							</ItemTemplate>
						</asp:Repeater>
					</table>
				</div>
				<div>
					<asp:Button runat="server" ID="AddMainQuizFromTemplate" Text="<%# WebApp.DisplayString.AddToMainQuiz %>" style="display:inline;" OnClick="AddMainQuizFromTemplate_Click" />
					<asp:Button runat="server" ID="AddRepeQuizFromTemplate" Text="<%# WebApp.DisplayString.AddToRepechageQuiz %>" style="display:inline;" OnClick="AddRepeQuizFromTemplate_Click" />
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		$(function () {
			$('.webapp_table').css('width', '100%')
		})
	</script>
</asp:Content>
