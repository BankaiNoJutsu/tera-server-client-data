<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="GmEventRewardSetting.aspx.cs" Inherits="WebApp.ContentsControl.GmEvent.GmEventRewardSetting" %>
<asp:Content ID="Content3" ContentPlaceHolderID="headerContentHolder" runat="server">
	<style type="text/css">
		div#controlButtons {
			margin: 5px 0;
		}

		div#controlButtons input[type=submit] {
			float: right;
		}
	</style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="mainContentHolder" runat="server">
	<form id="Form1" runat="server">
		<table class="lineTable">
			<tr>
				<th style="display:none;"><%# WebApp.DisplayString.RewardCategory %></th>
				<th><%# WebApp.DisplayString.ID %></th>
				<th><%# WebApp.DisplayString.Point %></th>
				<th><%# WebApp.DisplayString.Search %></th>
			</tr>
			<tr>
				<td style="display:none;"><asp:DropDownList runat="server" ID="RewardCategory" /></td>
				<td><asp:DropDownList runat="server" ID="RewardIdDesc" AutoPostBack="true" /></td>
				<td><asp:DropDownList runat="server" ID="PointList" /></td>
				<td><asp:Button runat="server" ID="SelectOption" Text="Search" OnClick="SelectOption_Click" /></td>
			</tr>
		</table>
		<div id="controlButtons">
			<asp:Button runat="server" ID="DeleteReward" CommandName="DeleteReward" OnCommand="DeleteReward_Command" Text="<%# WebApp.DisplayString.DeleteReward %>"/>
			<asp:Button runat="server" ID="AddReward" CommandName="AddReward" OnCommand="AddReward_Command" Text="<%# WebApp.DisplayString.AddReward %>"/>
		</div>
		<table class="lineTable width-full">
			<asp:Repeater runat="server" ID="RewardList">
				<HeaderTemplate>
					<tr>
						<th><asp:CheckBox runat="server" ID="RewardCheckAll" AutoPostBack="true" OnCheckedChanged="RewardCheckAll_CheckedChanged"/></th>
						<%--<th><%# WebApp.DisplayString.RewardCategory %></th>--%>
						<th><%# WebApp.DisplayString.ID %></th>
						<th><%# WebApp.DisplayString.Point %></th>
						<th><%# WebApp.DisplayString.RewardType %></th>
						<th><%# WebApp.DisplayString.RewardInfo %></th>
					</tr>
				</HeaderTemplate>
				<ItemTemplate>
					<tr>
						<td><asp:CheckBox runat="server" ID="RewardCheck" /><asp:HiddenField runat="server" ID="RewardDbId" Value='<%# Eval("DbId") %>'/></td>
						<%--<td><%# Eval("Category") %></td>--%>
						<td><%# Eval("RewardId") %></td>
						<td><%# Eval("PointRange") %></td>
						<td><%# Eval("RewardTypeName") %></td>
						<td><%# Eval("RewardInfo") %></td>
					</tr>
				</ItemTemplate>
			</asp:Repeater>
		</table>
	</form>
</asp:Content>
