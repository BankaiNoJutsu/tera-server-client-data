﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/AppCode/DataSetAskDialog.master" CodeBehind="ATO_GmEventEditEvent.aspx.cs" Inherits="WebApp.ContentsControl.GmEvent.ATO_GmEventEditEvent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
	<div class="ato-main">
		<ul class="form-style">
			<li><label class="width100"><%# WebApp.DisplayString.ServerName %></label><asp:Label runat="server" ID="ServerName" /></li>
			<li><label class="width100"><%# WebApp.DisplayString.EventList %></label><asp:Label runat="server" ID="EventTypeLabel" /><%--<asp:TextBox runat="server" ID="EventType" Visible="false" />--%></li>
			<li><label class="width100"><%# WebApp.DisplayString.EventId %></label><asp:Label runat="server" ID="EventId" /></li>
			<li><label class="width100"><%# WebApp.DisplayString.EventName %></label><asp:TextBox runat="server" ID="EventName" Enabled="false"/></li>
			<li><label class="width100"><%# WebApp.DisplayString.ChannelCount %></label><asp:DropDownList runat="server" ID="ChannelCount" /></li>
			<li><label class="width100"><%# WebApp.DisplayString.UserLimitPerChannel %></label><asp:TextBox runat="server" ID="MaxUserCount" /></li>
			<li><label class="width100"><%# WebApp.DisplayString.StartTime %></label><asp:TextBox runat="server" ID="StartTime" type="date" /></li>
			<li><label class="width100"><%# WebApp.DisplayString.WaitingTime %></label><asp:DropDownList runat="server" ID="WaitingSeconds" /></li>
			<li><label class="width100"><%# WebApp.DisplayString.LevelLimit %></label><asp:DropDownList runat="server" ID="MinLevel" /><span>~</span><asp:DropDownList runat="server" ID="MaxLevel" /></li>
			<li><label class="width100"><%# WebApp.DisplayString.AutoSystem %></label><asp:DropDownList runat="server" ID="AutoSystem" class="SelectAutoSystem" /></li>
			<li id="li-repeQuizStartRatio">
				<label class="width100"><%# WebApp.DisplayString.RepeQuizStartRatio %></label>
				<asp:TextBox runat="server" ID="RepeQuizStartRatio" />
				<span>값(r): (현재살아있는유저수)가 (시작유저수) * (r) 보다 작으면 패자부활 문제 시작</span>
			</li>
		</ul>
	</div>
	<script type="text/javascript">
		$(function () {
			$('[type=date]').each(function(){
				$(this).appendDtpicker({ "dateOnly": false, "minuteInterval": 5 });
			})
			$('[type=date]').keypress(function () { return false; })
			$('[type=date]').keydown(function () { return false; })

			if ($('.SelectAutoSystem').val() == '0') {
				// 수동진행
				$('#li-repeQuizStartRatio').hide()
			}

			$('.SelectAutoSystem').change(function () {
				var li = $('#li-repeQuizStartRatio');
				if ($(this).val() == '0') { // 수동진행
					$(li).find('input').val(0);
					li.hide();
				} else {
					li.show();
				}
			})
		});
	</script>
</asp:Content>
