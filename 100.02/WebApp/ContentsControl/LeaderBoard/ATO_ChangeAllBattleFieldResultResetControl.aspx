﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/ServerSelectAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_ChangeAllBattleFieldResultResetControl.aspx.cs" Inherits="WebApp.ContentsControl.LeaderBoard.ATO_ChangeAllBattleFieldResultResetControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
    <table style="margin-top: 20px" class="lineTable">
        <!-- 초기화 주기(일단위) -->
        <tr>
            <td><%# WebApp.DisplayString.ResetPeriod %></td>
            <td><asp:TextBox runat="server" ID="ResetPeriod" style="ime-mode:disabled"/></td>
        </tr>
    </table>
    <table style="margin-top: 20px" class="lineTable">
        <!-- 초기화 요일 -->
        <tr>
            <td><%# WebApp.DisplayString.ResetDay %></td>
            <td><asp:DropDownList runat="server" ID="ResetDaySelect" DataValueField="Item1" DataTextField="Item2" /></td>
        </tr>
    </table>
    <table style="margin-top: 20px" class="lineTable">
        <!-- 초기화 시간 -->
        <tr>
            <td><%# WebApp.DisplayString.ResetHour %></td>
            <td><asp:DropDownList runat="server" ID="ResetHourSelect" DataValueField="Item1" DataTextField="Item2" /></td>
        </tr>
    </table>

</asp:Content>
