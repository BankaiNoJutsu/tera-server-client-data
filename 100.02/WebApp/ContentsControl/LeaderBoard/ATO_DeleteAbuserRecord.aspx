﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_DeleteAbuserRecord.aspx.cs" Inherits="WebApp.ContentsControl.LeaderBoard.ATO_DeleteAbuserRecord" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">

    	
      <table class="lineTable">
        <asp:Repeater runat="server" ID="ControlList">
            <HeaderTemplate>
                <tr>
                    <th><%# WebApp.DisplayString.ServerName %></th>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><%# Eval("ServerName") %></td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
    <table style="margin-top: 20px" class="lineTable">
        <tr>
            <td><%# WebApp.DisplayString.UserName %></td>
            <td><asp:TextBox runat="server" ID="UserName" /></td>
        </tr>
    </table>
</asp:Content>
