﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="BattleFieldResultResetControl.aspx.cs" Inherits="WebApp.ContentsControl.LeaderBoard.BattleFieldResultResetControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headerContentHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">
    <form id="Form1" runat="server">
        <asp:Button runat="server" ID="ChangeAllServer" Text="<%# WebApp.DisplayString.ChangeAllServer %>" OnClick="ChangeAllServer_Click" />
        <table class="lineTable">
        <asp:Repeater runat="server" ID="ControlList">
            <HeaderTemplate>
                <tr>
                    <th><%# WebApp.DisplayString.ServerName %></th>
                    <th><%# WebApp.DisplayString.ResetPeriod %></th>
                    <th><%# WebApp.DisplayString.ResetDay %></th>
                    <th><%# WebApp.DisplayString.ResetHour %></th>
                    <th><%# WebApp.DisplayString.Control %></th>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><%# Eval("ServerName") %></td>
                    <td><asp:Label runat="server" ID="ResetPeriod" Text='<%# Eval("ResetPeriod") %>' /> <%# WebApp.DisplayString.Day %></td>
                    <td><asp:Label runat="server" ID="ResetDay" Text='<%# Eval("ResetDayString") %>' /> </td>
                    <td><asp:Label runat="server" ID="ResetHour" Text='<%# Eval("ResetHour") %>' /> <%# WebApp.DisplayString.Hour %></td>
                    <td><asp:Button runat="server" ID="ChangeServer" Text="<%# WebApp.DisplayString.Modify %>" CommandName="ChangeBFResultResetTime" CommandArgument='<%# Eval("ServerNo") + " " + Eval("ResetPeriod")+ " " + Eval("ResetDay")+ " " + Eval("ResetHour")%>' OnCommand="ChangeBFResultResetTime_Command" /></td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
        </table>
    </form>
</asp:Content>