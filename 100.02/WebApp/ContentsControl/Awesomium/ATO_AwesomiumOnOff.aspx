﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_AwesomiumOnOff.aspx.cs" Inherits="WebApp.ContentsControl.ATO_AwesomiumOnOff" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">
    
    <asp:Table runat="server" CssClass="lineTable" style="margin-bottom:20px;">
        <asp:TableRow>
            <asp:TableHeaderCell><%# WebApp.DisplayString.ServerName %></asp:TableHeaderCell>
            <asp:TableHeaderCell><%# WebApp.DisplayString.State %></asp:TableHeaderCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><asp:Label runat="server" ID="ServerName" /></asp:TableCell>
            <asp:TableCell><asp:Label runat="server" ID="OnOff" /></asp:TableCell>
        </asp:TableRow>
    </asp:Table>

</asp:Content>
