﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_InsertAwesomiumUrls.aspx.cs" Inherits="WebApp.ContentsControl.ATO_InsertAwesomiumUrls" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">

    <div class="ato-serverlist">
        <asp:Label runat="server" Text="<%# WebApp.DisplayString.CheckServer %>" />
		<ul>
			<asp:Repeater runat="server" ID="ServerCheckList">
				<ItemTemplate>
					<li><asp:CheckBox runat="server" ID="ServerName" Text='<%# Eval("ServerName") %>' /><asp:HiddenField runat="server" ID="ServerNo" Value='<%# Eval("ServerNo") %>' /></li>
				</ItemTemplate>
			</asp:Repeater>
		</ul>
	</div>
    <asp:Table runat="server" CssClass="lineTable">
        <asp:TableRow>
            <asp:TableHeaderCell><%# WebApp.DisplayString.Title %></asp:TableHeaderCell>
            <asp:TableHeaderCell><%# WebApp.DisplayString.Url %></asp:TableHeaderCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><asp:TextBox runat="server" ID="AwesomiumTitle" /></asp:TableCell>
            <asp:TableCell><asp:TextBox runat="server" ID="AwesomiumUrl" /></asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    
</asp:Content>
