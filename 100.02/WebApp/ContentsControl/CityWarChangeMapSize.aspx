﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="CityWarChangeMapSize.aspx.cs" Inherits="WebApp.ContentsControl.CityWarChangeMapSize" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headerContentHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">
    <form id="Form1" runat="server">
            <asp:DropDownList runat="server" ID="ServerDDL" DataSource="<%# WebApp.WebAdminDB.ServerListWithAll.Select(x => new {Text=x.mName, Value=x.mNo}) %>" DataTextField="Text" DataValueField="Value" />
            <asp:Button runat="server" ID="ServerChoice" Text="<%# WebApp.DisplayString.Search %>" OnClick="ServerChoice_Click" />
            <table class="lineTable">
            <asp:Repeater runat="server" ID="StateRepeater">
                <HeaderTemplate>
                    <tr>
                        <th><%# WebApp.DisplayString.ServerName %></th>
                        <th><%# WebApp.DisplayString.CityWarLeagueId %></th>
                        <th><%# WebApp.DisplayString.CityWarMapStateId %></th>
                        <th><%# WebApp.DisplayString.ChangeCityWarMapStateId %></th>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><%# Eval("ServerName") %> <asp:HiddenField runat="server" ID="ServerNo" Value='<%# Eval("ServerNo") %>' /></td>
                        <td><asp:Label runat="server" ID="LeagueId" Text='<%# Eval("LeagueId") %>' /> </td>
                        <td><asp:Label runat="server" ID="StateId" Text='<%# Eval("StateId") %>' /> </td>
                        <td><asp:Button runat="server" ID="ChangeStateId" Text="<%# WebApp.DisplayString.ChangeId %>" OnClick="StateChange_Click" /></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            </table>
    </form>
</asp:Content>
