﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="StoreBuyLimitControl.aspx.cs" Inherits="WebApp.ContentsControl.StoreBuyLimit.StoreBuyLimitControl" %>
<asp:Content ID="Content1" ContentPlaceHolderID="headerContentHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">
    <form runat="server" id="form">
    <table class="lineTable" style="margin-bottom:10px;">
	<tr>
		<th><%# WebApp.DisplayString.ServerChoice %></th>
		<th><%# WebApp.DisplayString.Search %></th>
	</tr>
	<tr>
		<td><asp:DropDownList runat="server" ID="ServerSelect" DataSource="<%# WebApp.WebAdminDB.ServerListWithAll.Select(x => new { ServerName = x.mName, ServerNo = x.mNo }) %>" DataTextField="ServerName" DataValueField="ServerNo"/></td>
		<td><asp:Button runat="server" ID="SelectOption" Text="Search" OnClick="SelectOption_Click" /></td>
	</tr>
	</table>  
    <table class="lineTable">
		<asp:Repeater runat="server" ID="StoreBuyLimitInfoList">
			<HeaderTemplate>
				<tr>
                    <th>
                        <asp:CheckBox runat="server" ID="AllCheckBox" AutoPostBack="true" OnCheckedChanged="AllCheckBox_CheckedChanged" />
                    </th>
					<th><%# WebApp.DisplayString.ServerName %></th>
                    <th><%# WebApp.DisplayString.StoreMenuId %></th>
					<th><%# WebApp.DisplayString.StoreBuyLimitResetTime %></th>
				</tr>
			</HeaderTemplate>
			<ItemTemplate>
				<tr>
					<td><asp:CheckBox runat="server" ID="LimitCheck" />
                        <asp:HiddenField runat="server" ID="LimitCheckHiddenField" Value='<%# Eval("ServerNo") %>'/>
					</td>
					<td><%# Eval("ServerName") %></td>
   					<td><%# Eval("StoreMenuId") %></td>
                    <td><%# Eval("StoreBuyLimitResetTime") %></td>
				</tr>
			</ItemTemplate>
		</asp:Repeater>
	</table>
    <table class="lineTable">
        <tr>
		    <td><asp:Button runat="server" ID="SetAllBuy" Text="<%# WebApp.DisplayString.StoreBuyLimitResetBtnStr %>" OnClick="SetAllBuy_Click" /></td>
            <td><asp:Button runat="server" ID="SetOff" Text="<%# WebApp.DisplayString.StoreBuyLimitOffBtnStr %>" OnClick="SetOff_Click" /></td>
		    <td><asp:Button runat="server" ID="SetResetTime" Text="<%# WebApp.DisplayString.StoreBuyLimitReservationResetBtnStr %>" OnClick="SetResetTime_Click" /></td>
        </tr>
        <tr>
            <td> <asp:DropDownList runat="server" ID="BuyMenuDataList" DataTextField="Text" DataValueField="Value" /> </td>
            <td> <asp:Button runat="server" ID="BuyMenuDataSearchButton"  Text="<%# WebApp.DisplayString.SelectBuyMenuList %>" OnClick="BuyMenuDataSearchButton_Click" /> </td>
        </tr>
    </table>
   <asp:Table runat="server" CssClass="lineTable" ID="BuyMenuListTable">
              <asp:TableRow>
                  <asp:TableHeaderCell> <%# WebApp.DisplayString.StoreType %> </asp:TableHeaderCell>
                  <asp:TableHeaderCell> <%# WebApp.DisplayString.ItemName %> </asp:TableHeaderCell>
                  <asp:TableHeaderCell> <%# WebApp.DisplayString.ItemID %> </asp:TableHeaderCell>
                  <asp:TableHeaderCell> <%# WebApp.DisplayString.StorePrice %> </asp:TableHeaderCell>
                  <asp:TableHeaderCell> <%# WebApp.DisplayString.StoreInfo %> </asp:TableHeaderCell>
              </asp:TableRow>
   </asp:Table>
    </form>
    
</asp:Content>
