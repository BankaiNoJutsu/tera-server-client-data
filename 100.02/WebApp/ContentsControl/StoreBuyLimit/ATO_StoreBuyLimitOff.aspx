﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_StoreBuyLimitOff.aspx.cs" Inherits="WebApp.ContentsControl.StoreBuyLimit.ATO_StoreBuyLimitOff" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">
    <table class="lineTable">
		<asp:Repeater runat="server" ID="StoreBuyLimitInfoList">
			<HeaderTemplate>
				<tr>
                    <th>
                        <asp:CheckBox runat="server" ID="AllCheckBox" AutoPostBack="true" OnCheckedChanged="AllCheckBox_CheckedChanged" />
                    </th>
					<th><%# WebApp.DisplayString.ServerName %></th>
                    <th><%# WebApp.DisplayString.StoreMenuId %></th>
                    <th><%# WebApp.DisplayString.State %></th>
				</tr>
			</HeaderTemplate>
			<ItemTemplate>
				<tr>
					<td><asp:CheckBox runat="server" ID="LimitCheck" />
                        <asp:HiddenField runat="server" ID="LimitCheckHiddenField" Value='<%# Eval("ServerNo") + " " + Eval("PlanetId") + " " + Eval("StoreMenuId") + " " + Eval("IsOff") %>'/>
					</td>
					<td><%# Eval("ServerName") %></td>
   					<td><%# Eval("StoreMenuId") %></td>
                    <td><%# Eval("ShowIsOff") %></td>
				</tr>
			</ItemTemplate>
		</asp:Repeater>
	</table>
</asp:Content>
