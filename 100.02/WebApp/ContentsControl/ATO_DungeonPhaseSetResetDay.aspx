﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/AppCode/DataSetAskDialog.master" CodeBehind="ATO_DungeonPhaseSetResetDay.aspx.cs" Inherits="WebApp.ContentsControl.ATO_DungeonPhaseSetResetDay" %>
<asp:Content ID="Content0" ContentPlaceHolderID="ContentHeader" runat="server">
	<style type="text/css">
		ul#setting-dayhour {
			list-style-type: none;
			padding: 5px 0;
		}

		ul#setting-dayhour li {
			padding: 5px 0;
		}

		ul#setting-dayhour li span {
			margin-right: 10px;
		}
	</style>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
	<div>
		<strong><%# WebApp.DisplayString.ServerName %></strong>
		<strong><%# WebApp.DisplayString.All %></strong>
	</div>

	<ul id="setting-dayhour">
		<li><span><%# WebApp.DisplayString.ResetDay %></span><asp:DropDownList runat="server" ID="DropDownResetDay" AutoPostBack="true" OnSelectedIndexChanged="DropDownResetDay_SelectedIndexChanged"/> </li>
		<li><span><%# WebApp.DisplayString.ResetHour %></span><asp:DropDownList runat="server" ID="DropDownResetHour" AutoPostBack="true" OnSelectedIndexChanged="DropDownResetHour_SelectedIndexChanged"/> </li>
	</ul>

</asp:Content>
