﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_DeletePremiumSlotReserveData.aspx.cs" Inherits="WebApp.ContentsControl.ATO_DeletePremiumSlotReserveData" %>
<%@ MasterType VirtualPath="~/AppCode/DataSetAskDialog.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">

    <asp:Label runat="server"><%# WebApp.DisplayString.WarningEditPremiumSlot %></asp:Label>
    <br />
    <br />

    <table class="lineTable">
            <asp:Repeater runat="server" ID="SlotReserveDataRepeater">
                <HeaderTemplate>
                    <tr>
                        <th><asp:CheckBox runat="server" ID="ReserveDataAllCheck" OnCheckedChanged="ReserveDataAllCheck_OnCheckedChanged" /></th>
                        <th><%# WebApp.DisplayString.ServerName %></th>
                        <th><%# WebApp.DisplayString.SlotSetId %></th>
                        <th><%# WebApp.DisplayString.SlotPos %></th>
                        <th><%# WebApp.DisplayString.LimitType %></th>
                        <th><%# WebApp.DisplayString.SlotType %></th>
                        <th><%# WebApp.DisplayString.IsCoolTimeAlwaysSpend %></th>
                        <th><%# WebApp.DisplayString.TemplateId2 %></th>
                        <th><%# WebApp.DisplayString.Amount %></th>
                        <th><%# WebApp.DisplayString.CoolTime_MilliSecond %></th>
                        <th><%# WebApp.DisplayString.GiveItemTemplateId %></th>
                        <th><%# WebApp.DisplayString.GiveItemAmount %></th>
                        <th><%# WebApp.DisplayString.StartTime %></th>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" ID="ReserveDataCheck" />
                            <asp:HiddenField runat="server" ID="ReserveDataHidden" Value='<%# Eval("IdentityNo") %>' />
                        </td>
                        <td><%# Eval("SlotSetId") %></td>
                        <td><%# Eval("SlotPos") %></td>
                        <td><%# Eval("LimitType") %></td>
                        <td><%# Eval("SlotType") %></td>
                        <td><%# (bool)Eval("IsRealTime") ? WebApp.DisplayString.On : WebApp.DisplayString.Off %></td>
                        <td><%# Eval("TemplateId") %></td>
                        <td><%# Eval("Amount") %></td>
                        <td><%# Eval("CoolTimeMilliSecond") %></td>
                        <td><%# Eval("GiveItemName") %></td>
                        <td><%# Eval("GiveItemAmount") %></td>
                        <td><%# Eval("StartTime") %></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>

</asp:Content>
