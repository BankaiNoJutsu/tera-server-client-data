﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_InsertPremiumSlotReserveData.aspx.cs" Inherits="WebApp.ContentsControl.ATO_InsertPremiumSlotReserveData" %>
<%@ MasterType VirtualPath="~/AppCode/DataSetAskDialog.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">

    <asp:Label runat="server"><%# WebApp.DisplayString.WarningEditPremiumSlot %></asp:Label>
    <br />
    <br />

    <div class="ato-serverlist">
		<label><%# WebApp.DisplayString.CheckServer %></label>
		<ul>
			<asp:Repeater runat="server" ID="ServerCheckList">
				<ItemTemplate>
					<li><asp:CheckBox runat="server" ID="ServerName" Text='<%# Eval("ServerName") %>' /><asp:HiddenField runat="server" ID="ServerNo" Value='<%# Eval("ServerNo") %>' /></li>
				</ItemTemplate>
			</asp:Repeater>
		</ul>
	</div>

    <asp:Table runat="server">
        <asp:TableRow>
            <asp:TableCell><asp:Label runat="server" Text="<%# WebApp.DisplayString.SlotSetId %>" /></asp:TableCell>
            <asp:TableCell><asp:DropDownList runat="server" ID="SlotSetIdDDL" /></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><asp:Label runat="server" Text="<%# WebApp.DisplayString.SlotPos %>" /></asp:TableCell>
            <asp:TableCell><asp:DropDownList runat="server" ID="SlotPosDDL" /></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><asp:Label runat="server" Text="<%# WebApp.DisplayString.LimitType %>" /></asp:TableCell>
            <asp:TableCell><asp:DropDownList runat="server" ID="LimitTypeDDL" DataTextField="Text" DataValueField="Value" /></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><asp:Label runat="server" Text="<%# WebApp.DisplayString.SlotType %>" /></asp:TableCell>
            <asp:TableCell><asp:DropDownList runat="server" ID="SlotTypeDDL" DataTextField="Text" DataValueField="Value" /></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><asp:Label runat="server" Text="<%# WebApp.DisplayString.IsCoolTimeAlwaysSpend %>" /></asp:TableCell>
            <asp:TableCell><asp:DropDownList runat="server" ID="OnOffDDL" DataTextField="Text" DataValueField="Value" /></asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><asp:Label runat="server" Text="<%# WebApp.DisplayString.TemplateId2 %>" /></asp:TableCell>
            <asp:TableCell>
                <asp:TextBox runat="server" ID="TemplateId" /> &nbsp
                <asp:Button runat="server" ID="CheckItemNameButton" Text="<%# WebApp.DisplayString.OK %>" OnClick="CheckItemNameButton_OnClick" /> &nbsp;
                <asp:Label runat="server" ID="ItemName" /> &nbsp;
                <asp:RegularExpressionValidator runat="server" ControlToValidate="TemplateId" ErrorMessage='<%# WebApp.DisplayString.ErrorMsg_OnlyNumbers %>' ValidationExpression="\d+" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><asp:Label runat="server" Text="<%# WebApp.DisplayString.Amount %>" /></asp:TableCell>
            <asp:TableCell>
                <asp:TextBox runat="server" ID="Amount" />
                <asp:RegularExpressionValidator runat="server" ControlToValidate="Amount" ErrorMessage='<%# WebApp.DisplayString.ErrorMsg_OnlyNumbers %>' ValidationExpression="\d+" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><asp:Label runat="server" Text="<%# WebApp.DisplayString.CoolTime_MilliSecond %>" /></asp:TableCell>
            <asp:TableCell>
                <asp:TextBox runat="server" ID="CoolTimeMilliSecond" />
                <asp:RegularExpressionValidator runat="server" ControlToValidate="CoolTimeMilliSecond" ErrorMessage='<%# WebApp.DisplayString.ErrorMsg_OnlyNumbers %>' ValidationExpression="\d+" />
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><asp:Label runat="server" Text="<%# WebApp.DisplayString.GiveItemTemplateId %>" /></asp:TableCell>
            <asp:TableCell>
                <asp:TextBox runat="server" ID="GetTemplateId" /> &nbsp;
                <asp:Button runat="server" ID="CheckGetItemNameButton" Text="<%# WebApp.DisplayString.OK %>" OnClick="CheckGetItemNameButton_OnClick" /> &nbsp;
                <asp:Label runat="server" ID="GetItemName" /> &nbsp;
                <asp:RegularExpressionValidator runat="server" ControlToValidate="GetTemplateId" ErrorMessage='<%# WebApp.DisplayString.ErrorMsg_OnlyNumbers %>' ValidationExpression="\d+" />     
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><asp:Label runat="server" Text="<%# WebApp.DisplayString.GiveItemAmount %>" /></asp:TableCell>
            <asp:TableCell>
                <asp:TextBox runat="server" ID="GetAmount" />
                <asp:RegularExpressionValidator runat="server" ControlToValidate="GetAmount" ErrorMessage='<%# WebApp.DisplayString.ErrorMsg_OnlyNumbers %>' ValidationExpression="\d+" />                
            </asp:TableCell>
        </asp:TableRow>
        <asp:TableRow>
            <asp:TableCell><asp:Label runat="server" Text="<%# WebApp.DisplayString.StartTime %>" /></asp:TableCell>
            <asp:TableCell>
                <asp:DropDownList runat="server" ID="StartTimeYear" /> &nbsp;
                <asp:DropDownList runat="server" ID="StartTimeMonth" /> &nbsp;
                <asp:DropDownList runat="server" ID="StartTimeDay" />
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>

</asp:Content>
