﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="DungeonOnOff.aspx.cs" Inherits="WebApp.Dungeon.DungeonOnOff" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">
    <form runat="server" id="form">
        <table class="outerlineTable">
        <tr>
            <td width="80"> <%= WebApp.DisplayString.ServerChoice %> </td>
            <td colspan="3"> 
                <asp:DropDownList ID="ServerNoDDL" runat="server" DataTextField="Text" DataValueField="Value" />
            </td>
        </tr>
        <tr>
            <td width="80"> <%= WebApp.DisplayString.DungeonName %> </td>
            <td width="400"> <asp:DropDownList runat="server" ID="DungeonDDL" DataTextField="Text" DataValueField="Value" /> </td>
            <td width="20">&nbsp;</td>
            <td> <asp:Button runat="server" ID="SearchButton" OnClick="SearchButton_OnClick" /> </td>
        </tr>
        </table>
        <br />
        <br />
        <asp:Panel runat="server" ID="ResultPanel">
        </asp:Panel>
    </form>

</asp:Content>
