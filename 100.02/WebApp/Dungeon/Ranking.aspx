﻿<%@ Page MasterPageFile="~/AppCode/WebAppPage.Master" Language="C#" AutoEventWireup="true" CodeBehind="Ranking.aspx.cs" Inherits="WebApp.Dungeon.Ranking" %>
<%@ Register TagPrefix="webApp" TagName="SearchForm" Src="~/SearchForm.ascx" %>
<asp:Content ID="Content1" runat="server" 
    contentplaceholderid="mainContentHolder">

    <form runat="server" id ="resultForm" >
    
    <table class="nolineTable">
        <tr>
            <td> <%= WebApp.DisplayString.ServerChoice %> </td>
            <td> <asp:DropDownList runat="server" ID="ServNoDDL" DataTextField="Text" DataValueField="Value" /> </td>
        </tr>
        <tr>
            <td> <%= WebApp.DisplayString.DungeonChoice %> </td>
            <td> <asp:DropDownList runat="server" ID="DungeonDDL" DataTextField="Text" 
                    DataValueField="Value" onselectedindexchanged="SeasonDDL_SelectedIndexChanged" AutoPostBack="true"><asp:ListItem Value="-1">------</asp:ListItem></asp:DropDownList> </td>
        </tr>
        <tr>
            <td> <%= WebApp.DisplayString.SeasonChoice %> </td>
            <td> <asp:DropDownList runat="server" ID="SeasonDDL" DataTextField="Text" DataValueField="Value" /> </td>
        </tr>
        <tr>
            <td> <asp:Button runat="server" ID="SearchButton" OnClick="SearchButton_Click" /></td>
        </tr>
    </table>
    <br />
        [<%= WebApp.DisplayString.SearchRankResult%>]
    <br />   
    <asp:GridView CssClass="lineTable" ID="pvpRanking" runat="server" AutoGenerateColumns="False" ShowHeader="true" ShowHeaderWhenEmpty="true">
    <Columns>
        <asp:TemplateField HeaderStyle-Width="40">
            <HeaderTemplate><%= WebApp.DisplayString.Ranking %></HeaderTemplate>
            <ItemTemplate><%# SetRank(Container.DataItemIndex + 1)%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="100">
            <HeaderTemplate><%=WebApp.DisplayString.UserDBID %></HeaderTemplate>
            <ItemTemplate>                
                <a href='<%# string.Format("../users/default.aspx?{0}={1}&{2}={3}",
                    WebApp.ParamDefine.Common.ServerNo,ServNoDDL.SelectedValue,
                    WebApp.ParamDefine.User.UserDBId, Eval("userDbId"))%>'>
                    <%# Eval("UserDBID")%>
                </a>
            </ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="200">
            <HeaderTemplate><%=WebApp.DisplayString.UserName%>/<%=WebApp.DisplayString.GuildName%>/<%=WebApp.DisplayString.Race%>/<%=WebApp.DisplayString.Class%></HeaderTemplate>
            <ItemTemplate><%# SetUserInfo(Convert.ToInt32(Eval("userDbId")))%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="100">
            <HeaderTemplate><%=WebApp.DisplayString.DungeonBestPoint%></HeaderTemplate>
            <ItemTemplate><%# Eval("bestRecord").ToString()%></ItemTemplate>
        </asp:TemplateField>
        <asp:TemplateField HeaderStyle-Width="200">
            <HeaderTemplate><%=WebApp.DisplayString.DungeonBestScoreDate%></HeaderTemplate>
            <ItemTemplate><%# SetUTCTime(Convert.ToDateTime(Eval("bestRecordPlayDate")))%></ItemTemplate>
        </asp:TemplateField>
    </Columns>
    </asp:GridView>
    <%=RankingPaging%>
    <br />
    <br />
    <table>
    <tr>
    <td>
        <br />
            [<%= WebApp.DisplayString.DungeonBestRank%>]
        <br />
        <asp:GridView CssClass="lineTable" ID="GVBestRecord" runat="server" AutoGenerateColumns="False" ShowHeader="true" ShowHeaderWhenEmpty="true">
        <Columns>
            <asp:TemplateField HeaderStyle-Width="40">
                <HeaderTemplate><%= WebApp.DisplayString.Ranking %></HeaderTemplate>
                <ItemTemplate><%# Container.DataItemIndex + 1%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-Width="100">
                <HeaderTemplate><%=WebApp.DisplayString.UserDBID%></HeaderTemplate>
                <ItemTemplate>                
                    <a href='<%# string.Format("../users/default.aspx?{0}={1}&{2}={3}",
                        WebApp.ParamDefine.Common.ServerNo,ServNoDDL.SelectedValue,
                        WebApp.ParamDefine.User.UserDBId, Eval("userDbId"))%>'>
                        <%# Eval("userDbId") %>
                    </a>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-Width="200">
                <HeaderTemplate><%=WebApp.DisplayString.UserName%>/<%=WebApp.DisplayString.GuildName%>/<%=WebApp.DisplayString.Race%>/<%=WebApp.DisplayString.Class%></HeaderTemplate>
                <ItemTemplate><%# SetUserInfo(Convert.ToInt32(Eval("userDbId")))%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-Width="100">
                <HeaderTemplate><%=WebApp.DisplayString.DungeonBestPoint%></HeaderTemplate>
                <ItemTemplate><%# Eval("bestPoint").ToString()%></ItemTemplate>
            </asp:TemplateField>
        </Columns>
        </asp:GridView>
    </td>
    <td>&nbsp;&nbsp;&nbsp;
    </td>
    <td>
        <br />
            [<%= WebApp.DisplayString.DungeonaccPointBestRank%>]
        <br />
        <asp:GridView CssClass="lineTable" ID="GVaccBestRecord" runat="server" AutoGenerateColumns="False" ShowHeader="true" ShowHeaderWhenEmpty="true">
        <Columns>
            <asp:TemplateField HeaderStyle-Width="40">
                <HeaderTemplate><%= WebApp.DisplayString.Ranking %></HeaderTemplate>
                <ItemTemplate><%# Container.DataItemIndex + 1%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-Width="100">
                <HeaderTemplate><%=WebApp.DisplayString.UserDBID%></HeaderTemplate>
                <ItemTemplate>                
                    <a href='<%# string.Format("../users/default.aspx?{0}={1}&{2}={3}",
                        WebApp.ParamDefine.Common.ServerNo,ServNoDDL.SelectedValue,
                        WebApp.ParamDefine.User.UserDBId, Eval("userDbId"))%>'>
                        <%# Eval("userDbId")%>
                    </a>
                </ItemTemplate>
                 </asp:TemplateField>
                <asp:TemplateField HeaderStyle-Width="200">
                    <HeaderTemplate><%=WebApp.DisplayString.UserName%>/<%=WebApp.DisplayString.GuildName%>/<%=WebApp.DisplayString.Race%>/<%=WebApp.DisplayString.Class%></HeaderTemplate>
                    <ItemTemplate><%# SetUserInfo(Convert.ToInt32(Eval("userDbId")))%></ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField HeaderStyle-Width="100">
                    <HeaderTemplate><%=WebApp.DisplayString.DungeonBestAccPoint%></HeaderTemplate>
                    <ItemTemplate><%# Eval("bestPoint").ToString()%></ItemTemplate>
                </asp:TemplateField>
        </Columns>
        </asp:GridView>
    </td>
    </tr>
    </table>
    </form>
</asp:Content>


