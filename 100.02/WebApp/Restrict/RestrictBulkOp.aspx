﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="RestrictBulkOp.aspx.cs" Inherits="WebApp.Restrict.RestrictBulkOp" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">
    <form runat="server" id="form">
    <table class="outerlineTable">
        <tr>
            <td colspan="3" style="padding-top: 5px;">
                <strong>[ <%= WebApp.DisplayString.RestrictBulkOp %> ]</strong>
            </td>
        </tr>
        <tr>
            <td colspan="3">
                &nbsp;<br />
            </td>
        </tr>
        <tr>
            <td width="80">
                <%= WebApp.DisplayString.RestrictType %>
            </td>
            <td>
                <asp:DropDownList runat="server" ID="RestrictTypeDDL" DataTextField="Text" DataValueField="Value"/>
            </td>
            <td>
                <asp:Button runat="server" ID="ViewButton" OnClick="ViewButton_Click" />
            </td>
        </tr>
        <tr>
            <td width="80">
                <%= WebApp.DisplayString.AddNewFile %>
            </td>
            <td>
                <asp:FileUpload runat="server" ID="DataFileUpload"/>
            </td>
            <td>
                <asp:Button runat="server" ID="AddNewFileButton" OnClick="AddNewFileButton_Click" />
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Panel runat="server" ID="ResultPanel">
                </asp:Panel>
            </td>
        </tr>
    </table>
    </form>
</asp:Content>
