﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="FestivalSeason.aspx.cs" Inherits="WebApp.WorldFestival.FestivalSeason" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">
    <form id="Form2" runat="server" class="nolineTable">
        <table class="nolineTable">
            <tr>
                <td> <%= WebApp.DisplayString.FestivalName %> </td>
                <td> <asp:DropDownList Width="200" runat="server" ID="FestivalDDL" DataTextField="Text" DataValueField="Value" /> </td>
                <td> <asp:Button runat="server" Text="<%# WebApp.DisplayString.Search %>" ID="SearchButton" OnClick="SearchButton_Click" /> </td>
                <td> <asp:Button runat="server" Text="<%# WebApp.DisplayString.AddFestival %>" ID="AddButton" OnClick="AddButton_Click" />  </td>
                <td> <asp:Button runat="server" Text="<%# WebApp.DisplayString.ModifyFestival %>" ID="ModifyButton" OnClick="ModifyButton_Click" />  </td>
                <td> <asp:Button runat="server" Text="<%# WebApp.DisplayString.DeleteFestival %>" ID="DeleteButton" OnClick="DeleteButton_Click" />  </td>
            </tr>
        </table><br/>

        <asp:GridView CssClass="lineTable" ID="FestivalList" runat="server" AutoGenerateColumns="False" ShowHeader="true" ShowHeaderWhenEmpty="true">
        <Columns>
            <asp:TemplateField   HeaderStyle-Width="50">
                <HeaderTemplate></HeaderTemplate>
                <ItemTemplate><asp:CheckBox runat="server" ID="checkbox" OnCheckedChanged="CheckBox_CheckedChanged" Set='<%#Eval("setId").ToString()%>' Festival='<%#Eval("festivalId").ToString()%>' AutoPostBack="true" /></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField   HeaderStyle-Width="100">
                <HeaderTemplate><%=WebApp.DisplayString.ServerNo%></HeaderTemplate>
                <ItemTemplate><%# Eval("serverNo").ToString()%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField   HeaderStyle-Width="100">
                <HeaderTemplate><%=WebApp.DisplayString.ServerName%></HeaderTemplate>
                <ItemTemplate><%# Eval("serverName").ToString()%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-Width="200">
                <HeaderTemplate><%=WebApp.DisplayString.FestivalName%></HeaderTemplate>
                <ItemTemplate><%# Eval("festivalName").ToString()%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-Width="100">
                <HeaderTemplate><%=WebApp.DisplayString.NpcStatus%></HeaderTemplate>
                <ItemTemplate><%# Eval("npcStatus").ToString()%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-Width="100">
                <HeaderTemplate><%=WebApp.DisplayString.ObjectStatus%></HeaderTemplate>
                <ItemTemplate><%# Eval("objectStatus").ToString()%></ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-Width="200">
                <HeaderTemplate><%=WebApp.DisplayString.FestivalPeriod%></HeaderTemplate>
                <ItemTemplate><%# Eval("festivalPeriod").ToString()%></ItemTemplate>
            </asp:TemplateField>
        </Columns>
        </asp:GridView>
    </form>
</asp:Content>
