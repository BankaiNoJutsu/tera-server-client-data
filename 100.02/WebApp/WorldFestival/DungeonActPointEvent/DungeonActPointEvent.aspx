﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="DungeonActPointEvent.aspx.cs" Inherits="WebApp.WorldFestival.DungeonActPointEvent" %>

<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form runat="server">

        <asp:Label runat="server"><%# WebApp.DisplayString.ServerChoice %></asp:Label>
        <asp:DropDownList runat="server" ID="ServerListDDL" DataTextField="Text" DataValueField="Value"></asp:DropDownList>
        <asp:Button runat="server" ID="SelectServerButton" Text="<%# WebApp.DisplayString.ServerChoice %>" OnClick="SelectServerButton_OnClick" />

        <br /> <br />

        <asp:Button runat="server" ID="AddDungeonActPointEventButton" Text="<%# WebApp.DisplayString.ADD_DUNGEON_ACTPOINT_EVENT %>" OnClick="AddDungeonActPointEventButton_OnClick" />
        <asp:Button runat="server" ID="DeleteDungeonActPointEventButton" Text="<%# WebApp.DisplayString.DeleteItem %>" OnClick="DeleteDungeonActPointEventButton_OnClick" />
        <br /> <br />

        <table class="lineTable">
            <asp:Repeater runat="server" ID="ActPointEventList">
                <HeaderTemplate>
                    <tr>
                        <th><asp:CheckBox runat="server" ID="ActPointEventCheckAll" AutoPostBack="true" OnCheckedChanged="ActPointEventCheckAll_OnCheckedChanged" /></th>
                        <th><%# WebApp.DisplayString.EventId %></th>
                        <th><%# WebApp.DisplayString.ServerName %></th>
                        <th><%# WebApp.DisplayString.DungeonName %></th>
                        <th><%# WebApp.DisplayString.DungeonActPoint %></th>
                        <th><%# WebApp.DisplayString.StartTime %></th>
                        <th><%# WebApp.DisplayString.EndTime %></th>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><asp:CheckBox runat="server" ID="ActPointEventCheck" /><asp:HiddenField runat="server" ID="CheckedDungeonActPointEvent" Value='<%# Eval("ServerNo") + " " + Eval("EventIndex") + " " + Eval("DungeonContinentId") %>'/></td>
                        <td><%# Eval("EventIndex") %></td>                        
                        <td><%# Eval("ServerName") %></td>
                        <td><%# WebApp.DungeonDatasheet.GetDungeonData((int)Eval("DungeonContinentId")).name %></td>
                        <td><%# Eval("RequiredActPoint") %></td>
                        <td><%# Eval("StartTime") %></td>
                        <td><%# Eval("EndTime") %></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </form>

</asp:Content>
