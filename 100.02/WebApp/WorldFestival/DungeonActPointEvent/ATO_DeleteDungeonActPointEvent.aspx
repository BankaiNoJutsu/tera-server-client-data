﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_DeleteDungeonActPointEvent.aspx.cs" Inherits="WebApp.WorldFestival.ATO_DeleteDungeonActPointEvent" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">

    <div class="ato-serverlist">
		<label><%# WebApp.DisplayString.CheckServer %></label>
		<ul>
			<asp:Repeater runat="server" ID="ServerCheckList">
				<ItemTemplate>
					<li><asp:CheckBox runat="server" ID="ServerName" Text='<%# Eval("ServerName") %>' /><asp:HiddenField runat="server" ID="ServerNo" Value='<%# Eval("ServerNo") %>' /></li>
				</ItemTemplate>
			</asp:Repeater>
		</ul>
	</div>

    <div class="ato-main">
        <table class="lineTable">
            <asp:Repeater runat="server" ID="DungeonActPointEventList">
                <HeaderTemplate>
                    <tr>
                        <th><%# WebApp.DisplayString.EventId %></th>
                        <th><%# WebApp.DisplayString.ServerName %></th>
                        <th><%# WebApp.DisplayString.DungeonId %></th>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><%# Eval("EventIndex") %></td>
                        <td><%# Eval("ServerName") %></td>
                        <td><%# WebApp.DungeonDatasheet.GetDungeonData((int)Eval("DungeonContinentId")).name %></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>

</asp:Content>
