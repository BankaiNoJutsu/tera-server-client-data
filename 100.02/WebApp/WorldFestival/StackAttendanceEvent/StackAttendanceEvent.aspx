﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="StackAttendanceEvent.aspx.cs" Inherits="WebApp.WorldFestival.StackAttendanceEvent.StackAttendanceEvent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headerContentHolder" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">
    <br />
   	<strong><%# WebApp.DisplayString.SelectServer %></strong>
    <form id="Form1" runat="server">
		<table class="lineTable">
			<tr>
				<th><%# WebApp.DisplayString.ServerChoice %></th>
				<th><%# WebApp.DisplayString.Search %></th>
			</tr>
            <tr>
				<td><asp:DropDownList runat="server" ID="ServerSelect" DataSource="<%# WebApp.WebAdminDB.ServerListWithAll.Select(x => new { ServerName = x.mName, ServerNo = x.mNo }) %>" DataTextField="ServerName" DataValueField="ServerNo"/></td>
				<td><asp:Button runat="server" ID="SelectOption" Text="Search" OnClick="SelectOption_Click" /></td>
			</tr>
        </table>
        <br />
      	<strong><%# WebApp.DisplayString.ReserveEvent %></strong>
        <div id="controlButtons" style="float:right">
            <asp:Button runat="server" ID="AddEvent" CommandName="AddEvent" OnCommand="Add_Event_Command"  Text="<%# WebApp.DisplayString.AddEvent %>"/>
			<asp:Button runat="server" ID="DeleteEvent" CommandName="DeleteEvent" OnCommand="Delete_Event_Command" Text="<%# WebApp.DisplayString.DeleteEvent %>"/>
            <asp:Button runat="server" ID="SetEventReward" CommandName="SetEventReward" OnCommand="Set_Event_Reward_Command"  Text="<%# WebApp.DisplayString.SetReward %>"/>
            <asp:Button runat="server" ID="RewardOnOff" CommandName="RewardOnOff" OnCommand="RewardOnOffCommand" Text="<%# WebApp.DisplayString.RewardOnOff %>"/>
		</div>
        <table class="lineTable width-full">
     		<asp:Repeater runat="server" ID="StackAttendanceEventList">
                 <HeaderTemplate>
                   <tr>
                       <th> <asp:CheckBox runat="server" ID="ServerCheckAll" AutoPostBack="true" OnCheckedChanged="ServerCheckAll_CheckedChanged" /> </th>
                       <th><%# WebApp.DisplayString.ServerName %> </th>
                       <th><%# WebApp.DisplayString.StartTime %> </th>
                       <th><%# WebApp.DisplayString.EndTime %> </th>
                       <th><%# WebApp.DisplayString.AttendanceCondition%> </th>
                       <th><%# WebApp.DisplayString.ResetHour %> </th>
                       <th><%# WebApp.DisplayString.RewardInfo %> </th>
                       <th><%# WebApp.DisplayString.RewardOnOff %> </th>
                   </tr>
                 </HeaderTemplate>
                 <ItemTemplate>
					<tr>
						<td><asp:CheckBox runat="server" ID="ServerCheck" /><asp:HiddenField runat="server" ID="ServerID" Value='<%# Eval("ServerNo") + " " + Eval("EventId") %>'/></td>
						<td><%# Eval("ServerName") %></td>
                        <td><%# Eval("StartTime") %></td>
                        <td><%# Eval("EndTime") %> </td>
					    <td><%# Eval("ConditionType") %> </td>
                        <td><%# Eval("ResetHour") %> </td>
                        <td><%# Eval("RewardInfoStr") %> </td>
                        <td><%# Eval("RewardOnOff") %></td>
					</tr>
				</ItemTemplate>
            </asp:Repeater>
        </table>
    </form>

</asp:Content>
