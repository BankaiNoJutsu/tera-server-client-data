﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="LimitedGacha.aspx.cs" Inherits="WebApp.WorldFestival.LimitedGacha" %>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">
    <form runat="Server" id="form">
        <table class="outerlineTable">
            <tr>
                <td>
                    <table border="0" cellspacing="0" cellpadding="0" width="100%">
                        <tr>
                	        <td>
                                <%= WebApp.DisplayString.ServerChoice %>
                                <asp:DropDownList runat="server" ID="ServNoDDL" DataTextField="Text" 
                                    DataValueField="Value" 
                                    onselectedindexchanged="ServNoDDL_SelectedIndexChanged" 
                                    AutoPostBack="True"/>
                            </td>
                            <td>
                                <%= WebApp.DisplayString.Limited_Gacha_Select_Gacha %>
                                <asp:DropDownList runat="server" ID="GachaNoDDL" DataTextField="Text" 
                                    DataValueField="Value" 
                                    onselectedindexchanged="GachaNoDDL_SelectedIndexChanged" 
                                    AutoPostBack="True" />
                            </td>
                            <td>
                                <asp:Button id="refreshBt" runat="server" onclick="refreshBt_Click"/>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table id="totalInfo">
                        <tr>
                            <td  colspan="6">
                                <strong><%= WebApp.DisplayString.Limited_Gacha_Total_Status %></strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= WebApp.DisplayString.Limited_Gacha_Total_Bucket_Number%>
                            </td>
                            <td>
                                <asp:Label ID="totalBucketNumberlbl" runat="Server"/>
                            </td>
                            <td>
                                <%= WebApp.DisplayString.Limited_Gacha_Left_Bucket_Number%>
                            </td>
                            <td>
                                <asp:Label ID="totalLeftBucketNumberlbl" runat="Server"/>
                            </td>
                            <td>
                                <%= WebApp.DisplayString.Limited_Gacha_Total_Left_Grace_Time %>
                            </td>
                            <td>
                                <asp:Label ID="totalLeftGraceTimelbl" runat="Server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <%= WebApp.DisplayString.Limited_Gacha_Target_Item_List %>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <asp:GridView ID="totalTargetItemListGV" runat="Server" AutoGenerateColumns="false">
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table>
                        <tr>
                            <td colspan="6">
                                <strong><%= WebApp.DisplayString.Limited_Gacha_Current_Bucket%></strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= WebApp.DisplayString.Limited_Gacha_Current_Bucket%>
                            </td>
                            <td>
                                <asp:Label ID="currentBucketIdlbl" runat="Server" />
                            </td>
                            <td>
                                <%= WebApp.DisplayString.Limited_Gacha_Use_Status%>
                            </td>
                            <td colspan="3">
                                <asp:Label ID="useStatuslbl" runat="Server" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%= WebApp.DisplayString.StartTime %>
                            </td>
                            <td>
                                <asp:label ID="startTimelbl" runat="server" />
                            </td>
                            <td>
                                <%= WebApp.DisplayString.EndTime %>
                            </td>
                            <td>
                                <asp:label Id="endTimeLb" runat="Server"/>
                            </td>
                            <td>
                                <%= WebApp.DisplayString.Limited_Gacha_Count %>
                            </td>
                            <td>
                                <asp:label Id="countLb" runat="server"/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <%= WebApp.DisplayString.Limited_Gacha_Target_Item_List%>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="6">
                                <asp:GridView ID="targetItemListGV" runat="Server" AutoGenerateColumns="false" AutoPostBack="true">
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <center>
                        <asp:Button runat="Server" ID="startGachaBt" onclick="StartGacha_Click" />
                        <asp:Button runat="Server" ID="stopGachaBt" onclick="StopGacha_Click" />
                        <asp:Button runat="Server" ID="modifyGachaBt" onclick="ModifyGacha_Click" />
                    </center>
                </td>
            </tr>
        </table>
    </form>
</asp:Content>
