﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="NpcSpawnEventNpc.aspx.cs" Inherits="WebApp.WorldFestival.NpcSpawnEventNpcSet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form runat="server" id="form">
        <table>
            <tr>
                <td colspan="2">
                    <asp:Panel runat="server" ID="ResultPanel">
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <br />
        <br />
        <table>
            <tr>
                <td> <asp:Button runat="server" ID="DelAllEventButton" OnClick="DelAllEventButton_OnClick" /> </td>
                <td> <asp:Panel runat="server" ID="ButtonPannel"> </asp:Panel> </td>
                <td> <asp:Button runat="server" ID="AddEventButton" OnClick="AddEventButton_OnClick" /> </td>
            </tr>
        </table>
    </form>

</asp:Content>