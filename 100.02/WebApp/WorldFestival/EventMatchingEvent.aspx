﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="EventMatchingEvent.aspx.cs" Inherits="WebApp.WorldFestival.EventMatchingEvent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form runat="server" id="form">
        <table>
        <tr>
            <td> <strong><%= WebApp.DisplayString.NAVMENU_EventMatchingEvent_Short%></strong></td>
            <td>
                <table>
                <tr>
                    <td> <%= WebApp.DisplayString.ServerChoice %> </td>
                    <td> <asp:DropDownList runat="server" ID="ServNoDDLForView" DataTextField="Text" DataValueField="Value" /> </td>
                    <td> <asp:Button runat="server" ID="SearchButton" OnClick="SearchButton_OnClick" /> </td>
                </tr>
                </table>
            </td>
        </tr>
        </table>
        <br />
        <br />
        <table>
            <tr> 
                <td> <strong><%= WebApp.DisplayString.PlayGuideEventList %> </strong></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Panel runat="server" ID="PlayGuideEventResult">
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <br />
        <table>
            <tr>
                <td> <asp:Button runat="server" ID="AddEventBtn" OnClick="AddEventBtn_OnClick" /> </td>
                <td> <asp:Panel runat="server" ID="DelEventBtn"> </asp:Panel> </td>
                <td> <asp:Button runat="server" ID="DelAllEventBtn" OnClick="DelAllEventBtn_OnClick" /> </td>
            </tr>
        </table>
        <table>
            <tr>
                <td> <asp:Panel runat="server" ID="eventPageNo"> </asp:Panel> </td>
                <td> <asp:Panel runat="server" ID="preEventButton"> </asp:Panel> </td>
                <td> <asp:Panel runat="server" ID="nextEventButton"> </asp:Panel> </td>
            </tr>
        </table>
        <br />
        <br />
        <table>
            <tr> 
                <td> <strong><%= WebApp.DisplayString.PlayGuideExtraReward %> </strong></td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Panel runat="server" ID="PlayGuideExtraRewardResult">
                    </asp:Panel>
                </td>
            </tr>
        </table>
        <br />
        <table>
            <tr>
                <td> <asp:Button runat="server" ID="AddRewardBtn" OnClick="AddExtraRewardBtn_OnClick" /> </td>
                <td> <asp:Panel runat="server" ID="DelRewardBtn"> </asp:Panel> </td>
                <td> <asp:Button runat="server" ID="DelAllRewardBtn" OnClick="DelAllRewardBtn_OnClick" /> </td>
            </tr>
        </table>
        <table>
            <tr>
                <td> <asp:Panel runat="server" ID="rewardPageNo"> </asp:Panel> </td>
                <td> <asp:Panel runat="server" ID="preRewardButton"> </asp:Panel> </td>
                <td> <asp:Panel runat="server" ID="nextRewardButton"> </asp:Panel> </td>
            </tr>
        </table>
    </form>

</asp:Content>