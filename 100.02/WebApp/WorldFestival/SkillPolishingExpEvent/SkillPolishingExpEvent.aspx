﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="SkillPolishingExpEvent.aspx.cs" Inherits="WebApp.WorldFestival.SkillPolishingExpEvent.SkillPolishingExpEvent" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headerContentHolder" runat="server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">
    <br />
   	<strong><%# WebApp.DisplayString.SelectServer %></strong>
    <form id="Form1" runat="server">
		<table class="lineTable">
			<tr>
				<th><%# WebApp.DisplayString.ServerChoice %></th>
				<th><%# WebApp.DisplayString.Search %></th>
			</tr>
            <tr>
				<td><asp:DropDownList runat="server" ID="ServerSelect" DataSource="<%# WebApp.WebAdminDB.ServerListWithAll.Select(x => new { ServerName = x.mName, ServerNo = x.mNo }) %>" DataTextField="ServerName" DataValueField="ServerNo"/></td>
				<td><asp:Button runat="server" ID="SelectOption" Text="Search" OnClick="SelectOption_Click" /></td>
			</tr>
        </table>
        <br />
      	<strong><%# WebApp.DisplayString.ReserveEvent %></strong>
        <div id="controlButtons" style="float:right">
            <asp:Button runat="server" ID="AddEvent" CommandName="AddEvent" OnCommand="AddEvent_OnClick"  Text="<%# WebApp.DisplayString.AddEvent %>"/>
			<asp:Button runat="server" ID="DeleteEvent" CommandName="DeleteEvent" OnCommand="DeleteEvent_OnClick" Text="<%# WebApp.DisplayString.DeleteEvent %>"/>
		</div>
        <table class="lineTable width-full">
     		<asp:Repeater runat="server" ID="SkillPolishingExpEventList">
                 <HeaderTemplate>
                   <tr>
                       <th> <asp:CheckBox runat="server" ID="ServerCheckAll" AutoPostBack="true" OnCheckedChanged="ServerCheckAll_CheckedChanged" /> </th>
                       <th><%# WebApp.DisplayString.ServerName %> </th>
                       <th><%# WebApp.DisplayString.EventId %> </th>
                       <th><%# WebApp.DisplayString.EventType %> </th>
                       <th><%# WebApp.DisplayString.EventValue %> </th>
                       <th><%# WebApp.DisplayString.StartTime %> </th>
                       <th><%# WebApp.DisplayString.EndTime %> </th>
                   </tr>
                 </HeaderTemplate>
                 <ItemTemplate>
					<tr>
						<td><asp:CheckBox runat="server" ID="ServerCheck" /><asp:HiddenField runat="server" ID="ServerID" Value='<%# Eval("ServerNo") + " " + Eval("EventId") %>'/></td>
						<td><%# Eval("ServerName") %></td>
                        <td><%# Eval("EventId") %></td>
                        <td><%# Eval("EventType") %></td>
                        <td><%# Eval("EventValue") %> </td>
					    <td><%# Eval("StartTime") %> </td>
                        <td><%# Eval("EndTime") %> </td>
					</tr>
				</ItemTemplate>
            </asp:Repeater>
        </table>
    </form>

</asp:Content>
