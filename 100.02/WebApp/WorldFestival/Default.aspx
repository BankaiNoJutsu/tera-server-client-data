﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebApp.WorldFestival.Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form runat="server" class="nolineTable">
    
        <table class="nolineTable">
            <tr>
                <td> <%= WebApp.DisplayString.ServerChoice %> </td>
                <td> <asp:DropDownList runat="server" ID="ServNoDDL" DataTextField="Text" DataValueField="Value" /> </td>
            </tr>
            <tr>
                <td> <%= WebApp.DisplayString.EventChoice%> </td>
                <td> <asp:DropDownList runat="server" ID="EventNoDDL" DataTextField="Text" DataValueField="Value" /> </td>
            </tr>
            <tr>
                <td> <asp:Button runat="server" ID="SearchButton" OnClick="SearchButton_Click" /> </td>
            </tr>
        </table>
                
        <asp:Panel runat="server" ID="AllServerPanel" Visible="false">
            <table class="inlineTable">
                <tr>
                    <td width="200" />
                    <td> <asp:Button runat="server" ID="AllServerToggleButton" OnClick="AllServerToggleButton_Click" /> </td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel runat="server" ID="resultPanel">
        </asp:Panel>
    </form>
</asp:Content>
