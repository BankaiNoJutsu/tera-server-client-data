﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_ModifyPlayTimeEvent.aspx.cs" Inherits="WebApp.WorldFestival.ATO_ModifyPlayTimeEvent" %>
<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
    
    <table style="margin-bottom: 10px; border: none;">
        <tr style="margin: 0px;">
            <td><%# WebApp.DisplayString.EventId %></td>
            <td><%# ((WebApp.AppCode.DataSetAskDialog)Master).FetchDataValue("EventId") %></td>
        </tr>

        <tr style="margin: 0px;">
            <td><%# WebApp.DisplayString.ServerName %></td>
            <td><asp:Label runat="server" ID="ServerName" Width="50"/></td>
        </tr>

        <tr style="margin: 0px;">
            <td><%# WebApp.DisplayString.AccountTrait %></td>
            <td><asp:RadioButtonList ID="AccountTraitId" runat="server"  DataTextField="Text" DataValueField="Value" RepeatColumns="5" RepeatLayout="Table" /></td>
        </tr>
            
        <tr style="margin: 0px;">
            <td><%# WebApp.DisplayString.EventPeriod %></td>
            <td><asp:TextBox runat="server" Rows="1" ID="StartTime" type="date" onkeypress="return false;" onkeydown="return false;" /> ~ <asp:TextBox runat="server" Rows="1" ID="EndTime" type="date"  onkeypress="return false;" onkeydown="return false;" />
                <script type="text/javascript">
                    $(function () {
                        $('*[type=date]').appendDtpicker();
                    });
                </script>
            </td>
        </tr>
    </table>
    <br />
    <p style="font-weight:bold"><%# WebApp.DisplayString.DailyPlayTimeReward %> </p>
    <table style="margin-bottom: 10px; border: none;">
        <asp:Repeater runat="server" ID="DailyRewardData">
            <HeaderTemplate>
                <tr>
                    <th><%# WebApp.DisplayString.AdminRewardId %></th>
                    <th><%# WebApp.DisplayString.PlayTime %></th>
                    <th><%# WebApp.DisplayString.GiveItemTemplateId %></th>
                    <th><%# WebApp.DisplayString.GiveItemAmount %></th>
                    <th><%# WebApp.DisplayString.LinkToAwsomium %></th>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><asp:Label runat="server" ID="RewardId" Text='<%# Eval("RewardId") %>' /> </td>
                    <td><asp:Label runat="server" ID="PlayTime" Text='<%# Eval("PlayTime") %>' /></td>
                    <td><asp:TextBox runat="server" Rows="1" ID="ItemTemplateId" Text='<%# Eval("ItemTemplateId") %>'  Width="150" /></td>
                    <td><asp:TextBox runat="server" Rows="1" ID="ItemCount" Text='<%# Eval("ItemCount") %>' Width="150" /></td>
                    <td><asp:TextBox runat="server" Rows="1" ID="Awsomium" Text='<%# Eval("Awsomium") %>' Width="150" /></td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>

    <p style="font-weight:bold"><%# WebApp.DisplayString.TotalPlayTimeReward %> </p>
    <table style="margin-bottom: 10px; border: none;">
        <asp:Repeater runat="server" ID="TotalRewardData" >
            <HeaderTemplate>
                <tr>
                    <th><%# WebApp.DisplayString.AdminRewardId %></th>
                    <th><%# WebApp.DisplayString.PlayTime %></th>
                    <th><%# WebApp.DisplayString.GiveItemTemplateId %></th>
                    <th><%# WebApp.DisplayString.GiveItemAmount %></th>
                    <th><%# WebApp.DisplayString.LinkToAwsomium %></th>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td><asp:Label runat="server" ID="RewardId" Text='<%# Eval("RewardId") %>' /> </td>
                    <td><asp:Label runat="server" ID="PlayTime" Text='<%# Eval("PlayTime") %>' /> </td>
                    <td><asp:TextBox runat="server" Rows="1" ID="ItemTemplateId" Text='<%# Eval("ItemTemplateId") %>'  Width="150" /></td>
                    <td><asp:TextBox runat="server" Rows="1" ID="ItemCount" Text='<%# Eval("ItemCount") %>' Width="150" /></td>
                    <td><asp:TextBox runat="server" Rows="1" ID="Awsomium" Text='<%# Eval("Awsomium") %>' Width="150" /></td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>
</asp:Content>
