﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_DelAdventureEvent.aspx.cs" Inherits="WebApp.WorldFestival.ServantAdventureEvent.ATO_DelAdventureEvent" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">
    <div id="ato-main">
        <table class="lineTable">
            <asp:Repeater runat="server" ID="DelEventList">
                <ItemTemplate>
                    <tr>
                        <th><%# WebApp.DisplayString.ServerName %></th>
                        <th><%# Convert.ToInt32(Eval("EventType")) == 0 ? WebApp.DisplayString.Multiple : WebApp.DisplayString.TimeReduction %></th>
                        <%# Convert.ToInt32(Eval("EventType")) == 0 ? "<th>" + WebApp.DisplayString.ItemGrade + "</th>" : "" %>
                        <th><%# WebApp.DisplayString.DateStart %></th>
                        <th><%# WebApp.DisplayString.DateEnd %></th>
                    </tr>
                    <tr>
                        <td><%# Eval("ServerName") %></td>
                        <td><%# Eval("EventValue") %></td>
                        <%# Convert.ToInt32(Eval("EventType")) == 0 ? "<td>" + Eval("ItemGrade") + "</td>" : "" %>
                        <td><%# Eval("StartTime") %></td>
                        <td><%# Eval("EndTime") %></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </div>
</asp:Content>
