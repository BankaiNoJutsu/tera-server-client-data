﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_AllDelAdventureEvent.aspx.cs" Inherits="WebApp.WorldFestival.ServantAdventureEvent.ATO_AllDelAdventureEvent" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">
    <asp:Repeater runat="server" ID="AllDelType">
        <ItemTemplate>
            <p><%# WebApp.CommonEnum.ServantAdventureEventEnum.GetEventTypeString(Convert.ToInt32(Eval("EventType"))) %></p>
            <br />
        </ItemTemplate>
    </asp:Repeater>
</asp:Content>
