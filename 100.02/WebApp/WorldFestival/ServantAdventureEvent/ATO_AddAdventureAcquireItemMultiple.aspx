﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_AddAdventureAcquireItemMultiple.aspx.cs" Inherits="WebApp.WorldFestival.ServantAdventureEvent.ATO_AddAdventureAcquireItemMultiple" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
    <div class="ato-serverlist">
		<label><%# WebApp.DisplayString.CheckServer %></label>
		<ul>
			<asp:Repeater runat="server" ID="ServerCheckList">
				<ItemTemplate>
					<li><asp:CheckBox runat="server" ID="ServerName" Text='<%# Eval("ServerName") %>' /><asp:HiddenField runat="server" ID="ServerNo" Value='<%# Eval("ServerNo") %>' /></li>
				</ItemTemplate>
			</asp:Repeater>
		</ul>
	</div>
     <div class="ato-main">
		<ul class="form-style">
            <li>
                <label class="width150"><%# WebApp.DisplayString.EventPeriod %></label>
                <asp:TextBox runat="server" ID="StartTime" type="date" /> ~
			    <asp:TextBox runat="server" ID="EndTime" type="date" />
            </li>
            <li>
                <label class="width150"><%# WebApp.DisplayString.ItemGrade %></label>
                <asp:DropDownList runat="server" ID="ItemGrade" class="itemGradeList" DataValueField="GradeId" DataTextField="GradeName" OnSelectedIndexChanged="DropDownItemGrade_SelectedIndexChanged"/>
            </li>
            <li>
                <label class="width150"><%# WebApp.DisplayString.EventValue %></label>
                <asp:TextBox runat="server" class="eventValue" ID="EventValue" />
                <span id="eventValueAlertMsg" style="color:red"></span>
            </li>
            <li>
                <table class="lineTable">
                    <th><%# WebApp.DisplayString.AddAdventureAcquireItemMultipleEvent %></th>
                </table>
            </li>
		</ul>
	</div>
    <script type="text/javascript">

        $(function () {
            $('[type=date]').each(function () {
                $(this).appendDtpicker({ "dateOnly": false, "minuteInterval": 1 });
            })
            $('[type=date]').keypress(function () { return false; })
            $('[type=date]').keydown(function () { return false; })
        });

        $('.eventValue').keyup(function () {
            this.value = this.value.replace(/[^0-9\.]/g, '');

            var eventValue = $(this).val();

            if (eventValue === undefined || eventValue === "")
                return;

            if (eventValue > 3)
                eventValue = 3;

            var textStr1 = '<%# WebApp.DisplayString.CurrentSettingValueIs %>';
            var textStr2 = '<%# WebApp.DisplayString.AppliedToMultipleValue %>';
            $('#eventValueAlertMsg').text(textStr1 + eventValue + textStr2);
        });

	</script>

</asp:Content>