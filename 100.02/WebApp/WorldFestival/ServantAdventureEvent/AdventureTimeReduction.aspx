﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="AdventureTimeReduction.aspx.cs" Inherits="WebApp.WorldFestival.ServantAdventureEvent.AdventureTimeReduction" %>

<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form runat="server">
        <asp:Label runat="server" Text="<%# WebApp.DisplayString.ServerChoice %>" />
        <asp:DropDownList runat="server" ID="ServerListDDL" DataTextField="Text" DataValueField="Value" />
        <asp:Button runat="server" Text="<%# WebApp.DisplayString.ServerChoice %>" OnClick="ServerChoice_OnClick" />

        <br />
        <br />

        <asp:Button runat="server" ID="AddTimeReductionEvent" Text="<%# WebApp.DisplayString.AddEvent %>" OnClick="AddTimeReductionEvent_OnClick" />
        <asp:Button runat="server" ID="DelTimeReductionEvent" Text="<%# WebApp.DisplayString.DeleteEvent %>" OnClick="DelTimeReductionEvent_OnClick" />
        <asp:Button runat="server" ID="DelAllTimeReductionEvent" Text="<%# WebApp.DisplayString.DeleteAllEvent %>" OnClick="DelAllTimeReductionEvent_OnClick" />

        <br />
        <br />

        <table class="lineTable">
            <asp:Repeater runat="server" ID="TimeReductionInfos">
                <HeaderTemplate>
                    <tr>
                        <th>
                            <asp:CheckBox runat="server" ID="AllCheckBox" AutoPostBack="true" OnCheckedChanged="AllCheckBox_CheckedChanged" /></th>
                        <th><%# WebApp.DisplayString.ServerName %></th>
                        <th><%# WebApp.DisplayString.TimeReduction %></th>
                        <th><%# WebApp.DisplayString.DateStart %></th>
                        <th><%# WebApp.DisplayString.DateEnd %></th>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" ID="ServerCheck" />
                            <asp:HiddenField runat="server" ID="EventHiddenField" Value='<%# Eval("ServerNo") + "|" + Eval("EventId") + "|" + Eval("EventType") %>' />
                        </td>
                        <td><%# Eval("ServerName") %></td>
                        <td><%# Eval("EventValue") %></td>
                        <td><%# Eval("StartTime") %></td>
                        <td><%# Eval("EndTime") %></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </form>

</asp:Content>
