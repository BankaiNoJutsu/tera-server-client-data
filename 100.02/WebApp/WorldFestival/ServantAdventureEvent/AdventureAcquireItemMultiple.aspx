﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="AdventureAcquireItemMultiple.aspx.cs" Inherits="WebApp.WorldFestival.ServantAdventureEvent.AdventureAcquireItemMultiple" %>

<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form runat="server">
        <asp:Label runat="server" Text="<%# WebApp.DisplayString.ServerChoice %>" />
        <asp:DropDownList runat="server" ID="ServerListDDL" DataTextField="Text" DataValueField="Value" />
        <asp:Button runat="server" Text="<%# WebApp.DisplayString.ServerChoice %>" OnClick="ServerChoice_OnClick" />

        <br />
        <br />

        <asp:Button runat="server" ID="AddMultipleItemEvent" Text="<%# WebApp.DisplayString.AddEvent %>" OnClick="AddMultipleItemEvent_OnClick" />
        <asp:Button runat="server" ID="DelMultipleItemEvent" Text="<%# WebApp.DisplayString.DeleteEvent %>" OnClick="DelMultipleItemEvent_OnClick" />
        <asp:Button runat="server" ID="DelAllMultipleItemEvent" Text="<%# WebApp.DisplayString.DeleteAllEvent %>" OnClick="DelAllMultipleItemEvent_OnClick" />

        <br />
        <br />

        <table class="lineTable">
            <asp:Repeater runat="server" ID="AcquireItemMultipleInfos">
                <HeaderTemplate>
                    <tr>
                        <th>
                            <asp:CheckBox runat="server" ID="AllCheckBox" AutoPostBack="true" OnCheckedChanged="AllCheckBox_CheckedChanged" /></th>
                        <th><%# WebApp.DisplayString.ServerName %></th>
                        <th><%# WebApp.DisplayString.Multiple %></th>
                        <th><%# WebApp.DisplayString.ItemGrade %></th>
                        <th><%# WebApp.DisplayString.DateStart %></th>
                        <th><%# WebApp.DisplayString.DateEnd %></th>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" ID="ServerCheck" />
                            <asp:HiddenField runat="server" ID="EventHiddenField" Value='<%# Eval("ServerNo") + "|" + Eval("EventId") + "|" + Eval("EventType") %>' />
                        </td>
                        <td><%# Eval("ServerName") %></td>
                        <td><%# Eval("EventValue") %></td>
                        <td><%# Eval("ItemGrade") %></td>
                        <td><%# Eval("StartTime") %></td>
                        <td><%# Eval("EndTime") %></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>
    </form>

</asp:Content>
