﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="DarkRiftRewardEvent.aspx.cs" Inherits="WebApp.WorldFestival.DarkRiftRewardEventSet" %>

<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form runat="server" id="form">
        <table>
        <tr>
            <td>
                <table>
                <tr>
                    <td> <%= WebApp.DisplayString.ServerChoice %> </td>
                    <td> <asp:DropDownList runat="server" ID="ServNoDDLForView" DataTextField="Text" DataValueField="Value" /> </td>
                    <td> <asp:Button runat="server" ID="SearchButton" OnClick="SearchButton_OnClick" /> </td>
                </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel runat="server" ID="ResultPanel">
                </asp:Panel>
            </td>
        </tr>
        </table>
        <br />
        <br />
        <table>
            <tr>
                <td> <asp:Button runat="server" ID="DelAllEventButton" OnClick="DelAllEventButton_OnClick" /> </td>
                <td> <asp:Panel runat="server" ID="ButtonPannel"> </asp:Panel> </td>
                <td> <asp:Button runat="server" ID="AddAdditionalEventButton" OnClick="AddAddtionEventButton_OnClick" /> </td>
                <td> <asp:Button runat="server" ID="AddIncreaseEventButton" OnClick="AddIncreaseEventButton_OnClick" /> </td>
            </tr>
        </table>
    </form>

</asp:Content>