﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_DeleteHuntingEventValue.aspx.cs" Inherits="WebApp.WorldFestival.HuntingEvent.ATO_DeleteHuntingEventValue" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">

    <table class="lineTable">
        <asp:Repeater runat="server" ID="HuntingZoneValueList">
            <HeaderTemplate>
                <tr>
                    <th><asp:CheckBox runat="server" ID="AllCheck" AutoPostBack="true" OnCheckedChanged="AllCheck_OnCheckedChanged" /></th>
                    <th><%# WebApp.DisplayString.EventName %></th>
                    <th><%# WebApp.DisplayString.EventType %></th>
                    <th><%# WebApp.DisplayString.HuntingZoneName %></th>
                    <th><%# WebApp.DisplayString.HuntingzoneEventValue %></th>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="EventCheck" />
                        <asp:HiddenField runat="server" ID="EventHiddenField" Value='<%# Eval("EventIndex") + "," + Eval("EventType") + "," + Eval("FieldType") + "," + Eval("HuntingZoneId") + "," + Eval("EventValue") %>' />
                    </td>
                    <td><%# Eval("EventName") %></td>
                    <td><%# Eval("EventTypeName") %></td>
                    <td><%# Eval("HuntingZoneName") %></td>
                    <td><%# Eval("EventValue") %></td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
    </table>

</asp:Content>
