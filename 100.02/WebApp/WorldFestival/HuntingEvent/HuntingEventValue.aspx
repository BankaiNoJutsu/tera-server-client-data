﻿<%@ Page Title="" Language="C#" EnableSessionState="True" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="HuntingEventValue.aspx.cs" Inherits="WebApp.WorldFestival.HuntingEvent.HuntingEventValue" %>

<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form runat="server">
        <asp:Label runat="server" Text="<%# WebApp.DisplayString.ServerChoice %>" /> &nbsp;
        <asp:DropDownList runat="server" ID="ServerListDDL" DataTextField="ServerName" DataValueField="ServerNo" /> &nbsp;
        <asp:Button runat="server" ID="ServerChoiceButton" Text="<%# WebApp.DisplayString.ServerChoice %>" OnClick="ServerChoiceButton_OnClick" />

        <br /> <br />

        <table class="lineTable">
            <asp:Repeater runat="server" ID="HuntingZoneValueList">
                <HeaderTemplate>
                    <tr>
                        <th><asp:CheckBox runat="server" ID="AllCheck" AutoPostBack="true" OnCheckedChanged="AllCheck_OnCheckedChanged" /></th>
                        <th><%# WebApp.DisplayString.EventName %></th>
                        <th><%# WebApp.DisplayString.EventType %></th>
                        <th><%# WebApp.DisplayString.HuntingZoneName %></th>
                        <th><%# WebApp.DisplayString.HuntingzoneEventValue %></th>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" ID="EventCheck" />
                            <asp:HiddenField runat="server" ID="EventHiddenField" Value='<%# Eval("EventIndex") + "," + Eval("EventType") + "," + Eval("FieldType") + "," + Eval("HuntingZoneId") + "," + Eval("EventValue") %>' />
                        </td>
                        <td><%# Eval("EventName") %></td>
                        <td><%# Eval("EventTypeName") %></td>
                        <td><%# Eval("HuntingZoneName") %></td>
                        <td><%# Eval("EventValue") %></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>

        <asp:Button runat="server" ID="AddEventButton" Text="<%# WebApp.DisplayString.AddEvent %>" OnClick="AddEventButton_OnClick" />
        <asp:Button runat="server" ID="DeleteEventButton" Text="<%# WebApp.DisplayString.DeleteEvent %>" OnClick="DeleteEventButton_OnClick" />
    </form>

</asp:Content>
