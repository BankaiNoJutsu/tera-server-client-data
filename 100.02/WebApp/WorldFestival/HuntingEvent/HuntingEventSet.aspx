﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="HuntingEventSet.aspx.cs" Inherits="WebApp.WorldFestival.HuntingEvent.HuntingEventSet" %>

<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">

    <form runat="server">
        <asp:Label runat="server" Text="<%# WebApp.DisplayString.ServerChoice %>" /> &nbsp;
        <asp:DropDownList runat="server" ID="ServerListDDL" DataTextField="ServerName" DataValueField="ServerNo" /> &nbsp;
        <asp:Button runat="server" ID="ServerChoiceButton" Text="<%# WebApp.DisplayString.ServerChoice %>" OnClick="ServerChoiceButton_OnClick" />

        <br /> <br />

        <table class="lineTable">
            <asp:Repeater runat="server" ID="HuntingEventSetList">
                <HeaderTemplate>
                    <tr>
                        <th><asp:CheckBox runat="server" ID="AllCheckBox" AutoPostBack="true" OnCheckedChanged="AllCheckBox_OnCheckedChanged" /></th>
                        <th><%# WebApp.DisplayString.ServerName %></th>
                        <th><%# WebApp.DisplayString.EventName %></th>
                        <th><%# WebApp.DisplayString.StartTime %></th>
                        <th><%# WebApp.DisplayString.EndTime %></th>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" ID="EventCheck" />
                            <asp:HiddenField runat="server" ID="EventHiddenField" Value='<%# Eval("ServerNo") + "|" + Eval("EventIndex") %>' />
                        </td>
                        <td><%# Eval("ServerName") %></td>
                        <td><%# Eval("EventName") %></td>
                        <td><%# Eval("StartTime") %></td>
                        <td><%# Eval("EndTime") %></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>

        <asp:Button runat="server" ID="AddEventButton" Text="<%# WebApp.DisplayString.AddEvent %>" OnClick="AddEventButton_OnClick" />
        <asp:Button runat="server" ID="DeleteEventButton" Text="<%# WebApp.DisplayString.DeleteEvent %>" OnClick="DeleteEventButton_OnClick" />
        <asp:Button runat="server" ID="DeleteAllEventButton" Text="<%# WebApp.DisplayString.DeleteAllEvent %>" OnClick="DeleteAllEventButton_OnClick" />
    </form>

</asp:Content>
