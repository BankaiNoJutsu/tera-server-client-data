﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_DeleteHuntingEventSet.aspx.cs" Inherits="WebApp.WorldFestival.HuntingEvent.ATO_DeleteHuntingEventSet" %>

<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">

    <div id="ato-main">

        <table class="lineTable">
            <asp:Repeater runat="server" ID="HutingEventList">
                <HeaderTemplate>
                    <tr>
                        <th><asp:CheckBox runat="server" ID="AllCheckBox" AutoPostBack="true" OnCheckedChanged="AllCheckBox_OnCheckedChanged" /></th>
                        <th><%# WebApp.DisplayString.ServerName %></th>
                        <th><%# WebApp.DisplayString.EventName %></th>
                        <th><%# WebApp.DisplayString.StartTime %></th>
                        <th><%# WebApp.DisplayString.EndTime %></th>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td>
                            <asp:CheckBox runat="server" ID="EventCheck" />
                            <asp:HiddenField runat="server" ID="EventHiddenField" Value='<%# Eval("ServerNo") + "|" + Eval("PlanetId") + "|" + Eval("EventIndex") %>' />
                        </td>
                        <td><%# Eval("ServerName") %></td>
                        <td><%# Eval("EventName") %></td>
                        <td><%# Eval("StartTime") %></td>
                        <td><%# Eval("EndTime") %></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
        </table>

    </div>

</asp:Content>
