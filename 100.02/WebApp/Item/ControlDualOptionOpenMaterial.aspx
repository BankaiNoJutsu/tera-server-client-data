﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="ControlDualOptionOpenMaterial.aspx.cs" Inherits="WebApp.Item.ControlDualOptionOpenMaterial" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headerContentHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">
    <form id="Form1" runat="server">
        <table>
            <tr>
                <td>
                    <asp:DropDownList runat="server" ID="ServerDDL" DataSource="<%# WebApp.WebAdminDB.ServerListWithAll.Select(x => new {Text=x.mName, Value=x.mNo}) %>" DataTextField="Text" DataValueField="Value" />
                </td>
                <td>
                    <asp:Button runat="server" ID="ServerChoice" Text="<%# WebApp.DisplayString.Search %>" OnClick="ServerChoice_Click" />
                </td>
            </tr>
            <tr>
                <td><asp:Button runat="server" ID="DeleteAllEvent" Text="<%# WebApp.DisplayString.DeleteAllSetting %>" OnClick="DeleteAllMaterialControlInfo_Click" /></td>
                <td><asp:Button runat="server" ID="DeleteEvent" Text="<%# WebApp.DisplayString.DeleteSetting %>" OnClick="DeleteMaterialControlInfo_Click" /></td>
                <td><asp:Button runat="server" ID="AddEvent" Text="<%# WebApp.DisplayString.AddSetting %>" OnClick="AddEvent_Click" /></td>
            </tr>
        </table>
        <table class="lineTable">
        <asp:Repeater runat="server" ID="EventList">
            <HeaderTemplate>
                <tr>
                    <th><asp:CheckBox runat="server" ID="ServerCheckAll" AutoPostBack="true" OnCheckedChanged="ServerCheckAll_OnClick" /></th>
                    <th><%# WebApp.DisplayString.ServerName %></th>
                    <th><%# WebApp.DisplayString.ItemWearingPart %></th>
                    <th><%# WebApp.DisplayString.Step %></th>
                    <th><%# WebApp.DisplayString.Grade %></th>
                    <th><%# WebApp.DisplayString.Item %></th>
                    <th><%# WebApp.DisplayString.StartDate %></th>
                    <th><%# WebApp.DisplayString.EndDate %></th>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="EventCheck" />
                        <asp:HiddenField runat="server" ID="ServerEvent" Value='<%# Eval("ServerNo") + "_" + Eval("SettingId")%>' />
                    </td>
                    <td><%# Eval("ServerName") %></td>
                    <td><asp:Label runat="server" ID="CombatItemType" Text='<%# Eval("CombatItemTypeString") %>' /> </td>
                    <td><asp:Label runat="server" ID="ItemRank" Text='<%# Eval("ItemRank") %>' /> </td>
                    <td><asp:Label runat="server" ID="ItemGrade" Text='<%# Eval("ItemGradeTypeString") %>' /> </td>
                    <td><%# Eval("MaterialItemListString") %></td>
                    <td><asp:Label runat="server" ID="StartDate" Text='<%# Eval("StartTime") %>' /> </td>
                    <td><asp:Label runat="server" ID="EndDate" Text='<%# Eval("EndTime") %>' /> </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
        </table>
    </form>
</asp:Content>