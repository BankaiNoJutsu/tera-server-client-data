﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="~/Item/ItemRestriction/ATO_DeleteItemRestriction.aspx.cs" Inherits="WebApp.Item.ATO_DeleteItemRestriction" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
	<table class="lineTable" style="margin-bottom:20px;">
		<tr>
			<th><%# WebApp.DisplayString.ServerName %></th>
			<th><%# WebApp.DisplayString.ItemTemplateId %></th>
            <th><%# WebApp.DisplayString.ItemName %></th>
		</tr>
		<tr>
			<td><asp:Label runat="server" ID="ServerName"/></td>
			<td><asp:Label runat="server" ID="ItemTemplateId"/></td>
            <td><asp:Label runat="server" ID="ItemName"/></td>
		</tr>
	</table>
</asp:Content>

