﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="CashItemLog.aspx.cs" Inherits="WebApp.Item.CashItemLog" %>


<asp:Content ID="Content1" runat="server" 
    contentplaceholderid="mainContentHolder">

    <form runat="server" id ="resultForm" >
  

    <table class="nolineTable">        
        <tr>
            <td> <%= WebApp.DisplayString.ServerChoice %> </td>
            <td> <asp:DropDownList runat="server" ID="ServNoDDL" DataTextField="Text" DataValueField="Value" /> </td>
        </tr>
        <tr>
            <td>
            <asp:RadioButton runat="Server" ID="AccountNameRadio" Checked = "true" GroupName="SearchType"/>
            <%= WebApp.DisplayString.AccountName %>
            </td>
            <td><asp:TextBox runat="Server" ID="AccountName"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
            <asp:RadioButton runat="Server" ID="UserNameRadio" GroupName="SearchType"/>
            <%= WebApp.DisplayString.UserName %>
            </td>
            <td><asp:TextBox runat="Server" ID="UserName"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
            <asp:RadioButton runat="Server" ID="ItemDBIDRadio" GroupName="SearchType"/>
            <%= WebApp.DisplayString.ItemDBID %>
            </td>
            <td><asp:TextBox runat="Server" ID="ItemDBID"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
            <asp:RadioButton runat="Server" ID="CouponIDRadio" GroupName="SearchType"/>
            <%= WebApp.DisplayString.CouponID %>
            </td>
            <td><asp:TextBox runat="Server" ID="CouponID"></asp:TextBox></td>
        </tr>
        <tr>
            <td> <asp:Button runat="server" ID="SearchButton" OnClick="SearchButton_Click" /> </td>
        </tr>
        
    </table>
    <br />
    

    <asp:GridView ID="CashItemLogGridView" runat="server" ShowHeaderWhenEmpty="true" class="lineTable" AutoGenerateColumns="false">
    <Columns >
    <asp:TemplateField>
        <HeaderTemplate> <%=WebApp.DisplayString.TimeStamp %> </HeaderTemplate>
        <ItemTemplate> <%# ((DateTime)Eval("timeStamp")).ToLocalTime()%></ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField>
        <HeaderTemplate> <%=WebApp.DisplayString.LogName %> </HeaderTemplate>
        <ItemTemplate> <%# WebApp.CommonEnum.CashItemLogType.ToString((byte)Eval("logType"))%></ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField>
        <HeaderTemplate> <%=WebApp.DisplayString.AccountName%> </HeaderTemplate>
        <ItemTemplate> <%# Eval("accountName") %></ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField>
        <HeaderTemplate> <%=WebApp.DisplayString.ServerName%> </HeaderTemplate>
        <ItemTemplate> <%# GetSeverName(Convert.ToInt32(Eval("serverNo")))%></ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField>
        <HeaderTemplate> <%=WebApp.DisplayString.UserName %> </HeaderTemplate>
        <ItemTemplate> 
        <a href='<%# string.Format("../users/default.aspx?{0}={1}&{2}={3}",
                    WebApp.ParamDefine.Common.ServerNo, Convert.ToInt32(Eval("serverNo")),
                    WebApp.ParamDefine.User.UserDBId, Eval("ownerDBID"))%>'>
                    <%# GetUserName(Convert.ToInt32(Eval("serverNo")), (long)Eval("ownerDBID")) ?? ""%>
        </a>
        </ItemTemplate>
    </asp:TemplateField>       
    <asp:TemplateField>
        <HeaderTemplate> <%=WebApp.DisplayString.ItemName %> </HeaderTemplate>
        <ItemTemplate> <%# WebApp.DataSheetManager.GetItemName((int)Eval("templateID")) %></ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField>
        <HeaderTemplate> <%=WebApp.DisplayString.ItemDBID %> </HeaderTemplate>
        <ItemTemplate>
        <a href='<%# string.Format("../item/searchitemdbid.aspx?{0}={1}&{2}={3}",
                    WebApp.ParamDefine.Common.ServerNo, Convert.ToInt32(Eval("serverNo")),
                    WebApp.ParamDefine.Item.ItemDBID, Eval("itemDBID"))%>'>
                    <%# Eval("itemDBID")%>
        </a>
        </ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField>
        <HeaderTemplate> <%=WebApp.DisplayString.Stack %> </HeaderTemplate>
        <ItemTemplate><%# Eval("stackCount")%></ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField>
        <HeaderTemplate> <%=WebApp.DisplayString.InvenType %> </HeaderTemplate>
        <ItemTemplate><%# WebApp.CommonEnum.Inven.ToString((int)Eval("invenType"))%></ItemTemplate>
    </asp:TemplateField>
    <asp:TemplateField>
        <HeaderTemplate> <%=WebApp.DisplayString.CouponID %> </HeaderTemplate>
        <ItemTemplate>
        <a href='<%# string.Format("../item/CashItemLog.aspx?{0}={1}&{2}={3}&{4}={5}",
                    WebApp.ParamDefine.Common.ServerNo, Convert.ToInt32(Eval("serverNo")),
                    WebApp.ParamDefine.Item.CouponID, Eval("couponID"),
                    WebApp.ParamDefine.Common.SearchType,3)%>'>
                    <%# Eval("couponID")%>
        </a>
        </ItemTemplate>
    </asp:TemplateField> 
    </Columns>
    </asp:GridView>

    
    </form>


</asp:Content>