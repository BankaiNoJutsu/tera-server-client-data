﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_ChangeLimitStoreItem.aspx.cs" Inherits="WebApp.Item.LimitStore.ATO_ChangeLimitStoreItem" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentHeader" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="TERA_ATO" runat="server">
    <table class="lineTable">
		<tr>
            <th><%# WebApp.DisplayString.ServerName %></th>
        </tr>
        <tr>
            <td><asp:Label runat="server" ID="CurServerName"/> </td>
        </tr>
        <tr>
            <th><%# WebApp.DisplayString.LimitStoreInfo %></th>
        </tr>
        <tr>
            <td><asp:Label runat="server" ID="LimitStoreInfo"/> </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel runat="server" ID="BuyMenuPanel">
                </asp:Panel>
                <asp:Table runat="server" CssClass="lineTable" ID="BuyMenuDataTable">
                    <asp:TableRow>
                        <asp:TableHeaderCell> <%# WebApp.DisplayString.StoreMenuId %> </asp:TableHeaderCell>
                        <asp:TableHeaderCell> <%# WebApp.DisplayString.TabInfo %> </asp:TableHeaderCell>
                        <asp:TableHeaderCell> <%# WebApp.DisplayString.ProbInfo %> </asp:TableHeaderCell>
                    </asp:TableRow>
                </asp:Table>
            </td>
        </tr>
        <tr>
            <td colspan="2">
                <asp:Panel runat="server" ID="ResultPanel">
                </asp:Panel>
                <table id="BuyMenuListTable">
                    <tr>
                        <th> <%# WebApp.DisplayString.ServerName %> </th>
                        <th> <%# WebApp.DisplayString.TabInfo %> </th>
                        <th> <%# WebApp.DisplayString.StoreType %> </th>
                        <th> <%# WebApp.DisplayString.ItemName %> </th>
                        <th> <%# WebApp.DisplayString.ItemID %> </th>
                        <th> <%# WebApp.DisplayString.StorePrice %> </th>
                        <th> <%# WebApp.DisplayString.StoreInfo %> </th>
                        <th> <%# WebApp.DisplayString.SellLimitType %> </th>
                        <th> <%# WebApp.DisplayString.WorldLimitCount %> </th>
                        <th> <%# WebApp.DisplayString.AccountLimitCount %> </th>
                        <th> <%# WebApp.DisplayString.DeleteFromList %></th>
                    </tr>
                    <tr>
                        <asp:Repeater runat="server" ID="BuyMenuRepeater">
                            <ItemTemplate>
                                <tr>  
                                    <td><%# Eval("ServerName") %></td>
                                    <td><%# Eval("TabInfo") %>
                                        <asp:HiddenField runat="server" ID="LimitCheckHiddenField" Value='<%# Eval("TabId") %>'/>
                                    </td>
                                    <td><%# Eval("StoreType") %></td>
                                    <td><%# Eval("ItemName") %></td>
                                    <td><%# Eval("ItemID") %></td>
                                    <td><%# Eval("StorePrice") %></td>
                                    <td><%# Eval("StoreInfo") %></td>
                                    <td><%# Eval("SellLimitType") %></td>
                                    <td><%# Eval("WorldLimitCount") %></td>
                                    <td><%# Eval("AccountLimitCount") %></td>
                                    <td>
                                        <asp:Button runat="server" ID="BuyMenuDeleteButton" Text='<%# WebApp.DisplayString.DeleteFromList %>'
                                           CommandArgument='<%# Eval("TabId") + " " + Eval("ItemID") %>' OnCommand="BuyMenuDelete_OnCommand" />
                                    </td>
                                </tr>
                                
                            </ItemTemplate>
                        </asp:Repeater>
                    </tr>
                </table>
            </td>
        </tr>  
    </table>
            <asp:Label runat="server" ID="npcGuildLabel" Text="<%# WebApp.DisplayString.NeedNpcGuildId%>" />
            <asp:DropDownList runat="server" ID="NeedNpcGuildDDL" DataTextField="Text" DataValueField="Value"/>
        <br />
            <asp:Label runat="server" ID="NeedNpcGuildIdLable" Text="<%# WebApp.DisplayString.NeedReputationGrade%>" />
            <asp:DropDownList runat="server" ID="NeedReputationGradeDDL" DataTextField="Text" DataValueField="Value"/>
        <br />
            <asp:Label runat="server" ID="NeedMedalItemTemplateIdLabel" Text="<%# WebApp.DisplayString.NeedMedalItemTemplateId%>" />
            <asp:TextBox runat="server" ID="NeedMedalItemTemplateIdTb" /> 
        <br />
            <asp:Label runat="server" ID="TabSelectLabel" Text="<%# WebApp.DisplayString.TabSelect%>" />
            <asp:DropDownList runat="server" ID="TabSelectDDL" DataTextField="Text" DataValueField="Value"/>
        <br />
            <asp:Label runat="server" ID="ItemTemplateIdLabel" Text="<%# WebApp.DisplayString.ItemTemplateId%>" />
            <asp:TextBox runat="server" ID="ItemTemplateIdTb" />
        <br />
            <asp:Label runat="server" ID="BuyListPriceLable" Text="<%# WebApp.DisplayString.BuyListPrice%>" />
            <asp:TextBox runat="server" ID="BuyListPriceTb" />
        <br />
            <asp:Label runat="server" ID="SellLimitTypeLable" Text="<%# WebApp.DisplayString.SellLimitType%>" />
            <asp:DropDownList runat="server" ID="SellLimitTypeDDL" DataTextField="Text" DataValueField="Value"/>
        <br />
            <asp:Label runat="server" ID="WorldLimitCountLabel" Text="<%# WebApp.DisplayString.WorldLimitCount%>" />
            <asp:TextBox runat="server" ID="WorldLimitCountTb" /> 
        <br />
            <asp:Label runat="server" ID="AccountLimitCountLable" Text="<%# WebApp.DisplayString.AccountLimitCount%>" />
            <asp:TextBox runat="server" ID="AccountLimitCountTb" /> 
        <br />
            <asp:Button runat="server" ID="InsertButton" Text="<%# WebApp.DisplayString.InsertItem%>" OnClick="InsertButton_Click" />
        <br />
</asp:Content>
