﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_ChangeGuildWarMaintainCost.aspx.cs" Inherits="WebApp.Guild.ATO_ChangeGuildWarMaintainCost" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
    <p style="font-weight:bold"><%# WebApp.DisplayString.CurrentGuildWarMaintainCost %> </p>
    <table class="lineTable" style="margin-bottom: 10px;">
        <tr>
            <th><%# WebApp.DisplayString.ServerName %></th>
            <th><%# WebApp.DisplayString.GuildSizeSmall %></th>
            <th><%# WebApp.DisplayString.GuildSizeMedium %></th>
            <th><%# WebApp.DisplayString.GuildSizeLarge %></th>
            <th><%# WebApp.DisplayString.GuildSizeXLarge %></th>
        </tr>
        <tr>
            <td><asp:Label runat="server" ID="CurServerName"/> </td>
            <td><asp:Label runat="server" ID="CurGuildSizeSmall"/> </td>
            <td><asp:Label runat="server" ID="CurGuildSizeMedium"/></td>
            <td><asp:Label runat="server" ID="CurGuildSizeLarge"/></td>
            <td><asp:Label runat="server" ID="CurGuildSizeXLarge"/></td>
        </tr>
    </table>
    <br />
    <br />
    <p style="font-weight:bold"><%# WebApp.DisplayString.NAVMENU_ChangeGuildWarMaintainCost_Short %> </p>
    <table class="lineTable" style="margin-bottom: 10px;">
        <tr>
            <th><%# WebApp.DisplayString.ServerName %></th>
            <th><%# WebApp.DisplayString.GuildSizeSmall %></th>
            <th><%# WebApp.DisplayString.GuildSizeMedium %></th>
            <th><%# WebApp.DisplayString.GuildSizeLarge %></th>
            <th><%# WebApp.DisplayString.GuildSizeXLarge %></th>
        </tr>
        <tr>
            <td><asp:Label runat="server" ID="NewServerName"/> </td>
            <td><asp:TextBox runat="server" Rows="1" ID="SmallMaintainCost" /></td>
            <td><asp:TextBox runat="server" Rows="1" ID="MediumMaintainCost"/></td>
            <td><asp:TextBox runat="server" Rows="1" ID="LargeMaintainCost"/></td>
            <td><asp:TextBox runat="server" Rows="1" ID="XLargeMaintainCost"/></td>
        </tr>
    </table>
</asp:Content>
