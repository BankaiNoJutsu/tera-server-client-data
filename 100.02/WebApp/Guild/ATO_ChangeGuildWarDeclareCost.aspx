﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_ChangeGuildWarDeclareCost.aspx.cs" Inherits="WebApp.Guild.ATO_ChangeGuildWarDeclareCost" ValidateRequest="false" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
    <p style="font-weight:bold"><%# WebApp.DisplayString.CurrentGuildWarDeclareCost %> </p>
    <table class="lineTable" style="margin-bottom: 10px;">
        <tr>
            <th><%# WebApp.DisplayString.ServerName %></th>
            <th><%# WebApp.DisplayString.GuildSizeSmall %></th>
            <th><%# WebApp.DisplayString.GuildSizeMedium %></th>
            <th><%# WebApp.DisplayString.GuildSizeLarge %></th>
            <th><%# WebApp.DisplayString.GuildSizeXLarge %></th>
        </tr>
        <tr>
            <td><asp:Label runat="server" ID="CurServerName"/> </td>
            <td><asp:Label runat="server" ID="CurGuildSizeSmall"/> </td>
            <td><asp:Label runat="server" ID="CurGuildSizeMedium"/></td>
            <td><asp:Label runat="server" ID="CurGuildSizeLarge"/></td>
            <td><asp:Label runat="server" ID="CurGuildSizeXLarge"/></td>
        </tr>
    </table>
    <br />
    <br />
    <p style="font-weight:bold"><%# WebApp.DisplayString.NAVMENU_ChangeGuildWarDeclareCost_Short %> </p>
    <table class="lineTable" style="margin-bottom: 10px;">
        <tr>
            <th><%# WebApp.DisplayString.ServerName %></th>
            <th><%# WebApp.DisplayString.GuildSizeSmall %></th>
            <th><%# WebApp.DisplayString.GuildSizeMedium %></th>
            <th><%# WebApp.DisplayString.GuildSizeLarge %></th>
            <th><%# WebApp.DisplayString.GuildSizeXLarge %></th>
        </tr>
        <tr>
            <td><asp:Label runat="server" ID="NewServerName"/> </td>
            <td><asp:TextBox runat="server" Rows="1" ID="SmallDeclareCost" /></td>
            <td><asp:TextBox runat="server" Rows="1" ID="MediumDeclareCost"/></td>
            <td><asp:TextBox runat="server" Rows="1" ID="LargeDeclareCost"/></td>
            <td><asp:TextBox runat="server" Rows="1" ID="XLargeDeclareCost"/></td>
        </tr>
    </table>
</asp:Content>
