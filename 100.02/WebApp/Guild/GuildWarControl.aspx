﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="GuildWarControl.aspx.cs" Inherits="WebApp.Guild.GuildWarControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headerContentHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">
    <form id="Form1" runat="server">
            <asp:DropDownList runat="server" ID="ServerDDL" DataSource="<%# WebApp.WebAdminDB.ServerListWithAll.Select(x => new {Text=x.mName, Value=x.mNo}) %>" DataTextField="Text" DataValueField="Value" />
            <asp:Button runat="server" ID="ServerChoice" Text="<%# WebApp.DisplayString.Search %>" OnClick="ServerChoice_Click" />
            <table class="lineTable">
            <asp:Repeater runat="server" ID="StateRepeater">
                <HeaderTemplate>
                    <tr>
                        <th><%# WebApp.DisplayString.ServerName %></th>
                        <th><%# WebApp.DisplayString.GuildWarOnOff %></th>
                        <th><%# WebApp.DisplayString.Open %></th>
                        <th><%# WebApp.DisplayString.Close%></th>
                    </tr>
                </HeaderTemplate>
                <ItemTemplate>
                    <tr>
                        <td><%# Eval("ServerName") %> <asp:HiddenField runat="server" ID="ServerNo" Value='<%# Eval("ServerNo") %>' /></td>
                        <td><asp:Label runat="server" ID="State" Text='<%# Eval("State") %>' /> </td>
                        <td><asp:Button runat="server" ID="GuildWarOn" Text="<%# WebApp.DisplayString.GuildWar_on %>" OnClick="GuildWarOn_Click" /></td>
                        <td><asp:Button runat="server" ID="GuildWarOff" Text="<%# WebApp.DisplayString.GuildWar_off %>" OnClick="GuildWarOff_Click" /></td>
                    </tr>
                </ItemTemplate>
            </asp:Repeater>
            </table>
    </form>
</asp:Content>
