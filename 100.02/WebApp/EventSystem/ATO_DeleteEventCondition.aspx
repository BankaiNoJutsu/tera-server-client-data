﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_DeleteEventCondition.aspx.cs" Inherits="WebApp.EventSystem.ATO_DeleteEventCondition" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
    <table class="lineTable">
        <asp:Repeater runat="server" ID="EventList">
            <HeaderTemplate>
                <tr>
                    <th><asp:CheckBox runat="server" ID="ServerCheckAll" AutoPostBack="true" OnCheckedChanged="ServerCheckAll_OnClick" /></th>
                    <th><%# WebApp.DisplayString.ServerName %></th>
                    <th><%# WebApp.DisplayString.EventId %></th>
                    <th><%# WebApp.DisplayString.EventSystemUnit %></th>
                    <th><%# WebApp.DisplayString.AccountSetting %></th>
                    <th><%# WebApp.DisplayString.EventCondition %></th>
                    <th><%# WebApp.DisplayString.RaceSetting %></th>
                    <th><%# WebApp.DisplayString.EventTypeId %></th>
                    <th><%# WebApp.DisplayString.EventSystemRewardId %></th>
                    <th><%# WebApp.DisplayString.StartDate %></th>
                    <th><%# WebApp.DisplayString.EndDate %></th>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="EventCheck" checked="true" Enabled="false"/>
                        <asp:HiddenField runat="server" ID="ServerEvent" Value='<%# Eval("ServerNo") + " " + Eval("EventId") %>' />
                    </td>
                    <td><%# Eval("ServerName") %></td>
                    <td><asp:Label runat="server" ID="EventId" Text='<%# Eval("EventId") %>' /> </td>
                    <td><asp:Label runat="server" ID="EventSystemUnit" Text='<%# Eval("EventSystemUnit") %>' /> </td>
                    <td><asp:Label runat="server" ID="ConditionAccountType" Text='<%# Eval("ConditionAccountType") %>' /> </td>
                    <td><asp:Label runat="server" ID="EventCondition" Text='<%# Eval("EventCondition") %>' /> </td>
                    <td><asp:Label runat="server" ID="ConditionRaceGenderList" Text='<%# Eval("ConditionRaceGenderList") %>' /> </td>
                    <td><asp:Label runat="server" ID="EventTypeId" Text='<%# Eval("EventTypeId") %>' /> </td>
                    <td><asp:Label runat="server" ID="EventRewardId" Text='<%# Eval("EventRewardId") %>' /> </td>
                    <td><asp:Label runat="server" ID="StartDate" Text='<%# Eval("StartTime") %>' /> </td>
                    <td><asp:Label runat="server" ID="EndDate" Text='<%# Eval("EndTime") %>' /> </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
        </table>
</asp:Content>