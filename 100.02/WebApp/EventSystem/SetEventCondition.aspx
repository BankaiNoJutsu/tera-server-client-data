﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="SetEventCondition.aspx.cs" Inherits="WebApp.EventSystem.SetEventCondition" %>

<asp:Content ID="Content1" ContentPlaceHolderID="headerContentHolder" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="mainContentHolder" runat="server">
    <form id="Form1" runat="server">
        <table>
            <tr>
                <td>
                    <asp:DropDownList runat="server" ID="ServerDDL" DataSource="<%# WebApp.WebAdminDB.ServerListWithAll.Select(x => new {Text=x.mName, Value=x.mNo}) %>" DataTextField="Text" DataValueField="Value" />
                </td>
                <td>
                    <asp:Button runat="server" ID="ServerChoice" Text="<%# WebApp.DisplayString.Search %>" OnClick="ServerChoice_Click" />
                </td>
            </tr>
            <tr>
                <td><asp:Button runat="server" ID="DeleteAllEvent" Text="<%# WebApp.DisplayString.DeleteAllSetting %>" OnClick="DeleteAllEvent_Click" /></td>
                <td><asp:Button runat="server" ID="DeleteEvent" Text="<%# WebApp.DisplayString.DeleteSetting %>" OnClick="DeleteEvent_Click" /></td>
                <td><asp:Button runat="server" ID="AddEvent" Text="<%# WebApp.DisplayString.AddSetting %>" OnClick="AddEvent_Click" /></td>
            </tr>
        </table>
        <table class="lineTable">
        <asp:Repeater runat="server" ID="EventList">
            <HeaderTemplate>
                <tr>
                    <th><asp:CheckBox runat="server" ID="ServerCheckAll" AutoPostBack="true" OnCheckedChanged="ServerCheckAll_OnClick" /></th>
                    <th><%# WebApp.DisplayString.ServerName %></th>
                    <th><%# WebApp.DisplayString.EventId %></th>
                    <th><%# WebApp.DisplayString.EventSystemUnit %></th>
                    <th><%# WebApp.DisplayString.AccountSetting %></th>
                    <th><%# WebApp.DisplayString.EventCondition %></th>
                    <th><%# WebApp.DisplayString.RaceSetting %></th>
                    <th><%# WebApp.DisplayString.EventTypeId %></th>
                    <th><%# WebApp.DisplayString.EventSystemRewardId %></th>
                    <th><%# WebApp.DisplayString.StartDate %></th>
                    <th><%# WebApp.DisplayString.EndDate %></th>
                </tr>
            </HeaderTemplate>
            <ItemTemplate>
                <tr>
                    <td>
                        <asp:CheckBox runat="server" ID="EventCheck" />
                        <asp:HiddenField runat="server" ID="ServerEvent" Value='<%# Eval("ServerNo") + " " + Eval("EventId") %>' />
                    </td>
                    <td><%# Eval("ServerName") %></td>
                    <td><asp:Label runat="server" ID="EventId" Text='<%# Eval("EventId") %>' /> </td>
                    <td><asp:Label runat="server" ID="EventSystemUnit" Text='<%# Eval("EventSystemUnit") %>' /> </td>
                    <td><asp:Label runat="server" ID="ConditionAccountType" Text='<%# Eval("ConditionAccountType") %>' /> </td>
                    <td><asp:Label runat="server" ID="EventCondition" Text='<%# Eval("EventCondition") %>' /> </td>
                    <td><asp:Label runat="server" ID="ConditionRaceGenderList" Text='<%# Eval("ConditionRaceGenderList") %>' /> </td>
                    <td><asp:Label runat="server" ID="EventTypeId" Text='<%# Eval("EventTypeId") %>' /> </td>
                    <td><asp:Label runat="server" ID="EventRewardId" Text='<%# Eval("EventRewardId") %>' /> </td>
                    <td><asp:Label runat="server" ID="StartDate" Text='<%# Eval("StartTime") %>' /> </td>
                    <td><asp:Label runat="server" ID="EndDate" Text='<%# Eval("EndTime") %>' /> </td>
                </tr>
            </ItemTemplate>
        </asp:Repeater>
        </table>
    </form>
</asp:Content>