﻿<%@ Page Title="" Language="C#" MasterPageFile="~/AppCode/DataSetAskDialog.master" AutoEventWireup="true" CodeBehind="ATO_AddEventCondition.aspx.cs" Inherits="WebApp.EventSystem.ATO_AddEventCondition" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TERA_ATO" runat="server">
    <table class="lineTable">
        <tr style="margin: 0px;">
            <td><%# WebApp.DisplayString.ServerChoice %></td>
            <td><asp:CheckBoxList ID="Servers" runat="server" DataSource="<%#WebApp.WebAdminDB.ServerListWithAll.Select(x => new { Text = x.mName, Value = x.mNo })%>" DataTextField="Text" DataValueField="Value" RepeatColumns="5" RepeatLayout="Table" /></td>
        </tr>
    </table>
    <table style="margin-top: 20px" class="lineTable">
        <!-- 이벤트 대상 단위 설정 -->
        <tr>
            <td><%# WebApp.DisplayString.EventSystemUnit %></td>
            <td><asp:DropDownList runat="server" ID="EventUnitId" DataTextField="Text" DataValueField="Value" /></td>
        </tr>
    </table>
    <table style="margin-top: 20px" class="lineTable">
        <!-- 계정 설정 -->
        <tr>
            <td><%# WebApp.DisplayString.AccountSetting %></td>
            <td>
                <asp:CheckBox runat="server" ID="AllAccount" Text='<%# WebApp.DisplayString.All %>' AutoPostBack="true" OnCheckedChanged="AccountCheckAll_OnClick" />
                <asp:HiddenField runat="server" ID="AccountType" Value='<%# (int)WebApp.CommonEnum.AccountUserType.Type.AUT_ALL %>' />
                <asp:Repeater runat="server" ID="AccountTypeList">
				    <ItemTemplate>
					    <asp:CheckBox runat="server" ID="AccountTypeName" AutoPostBack="true" OnCheckedChanged="AccountCheck_OnClick" Text='<%# Eval("AccountTypeName") %>' />
                        <asp:HiddenField runat="server" ID="AccountType" Value='<%# Eval("AccountType") %>' />
				    </ItemTemplate>
			    </asp:Repeater>
            </td>
        </tr>
        <p style="color:red; font-weight:bold"><%# WebApp.DisplayString.NewCharacterWarning %> </br><%# WebApp.DisplayString.NewCharacterWarningEx %></p>
    </table>
    <table style="margin-top: 20px" class="lineTable">
        <!-- 클래스 설정 -->
        <tr>
            <td><%# WebApp.DisplayString.ClassSetting %></td>
            <td>
                <asp:CheckBox runat="server" ID="AllClass" Text='<%# WebApp.DisplayString.All %>' AutoPostBack="true" OnCheckedChanged="ClassCheckAll_OnClick" />
                <asp:HiddenField runat="server" ID="ClassNo" Value='<%# (int)WebApp.CommonEnum.Class.Type.CT_COMMON %>' />
                <asp:Repeater runat="server" ID="ClassCheckList">
				    <ItemTemplate>
					    <asp:CheckBox runat="server" ID="ClassName" AutoPostBack="true" OnCheckedChanged="ClassCheck_OnClick" Text='<%# Eval("ClassName") %>' />
                        <asp:HiddenField runat="server" ID="ClassNo" Value='<%# Eval("ClassNo") %>' />
				    </ItemTemplate>
			    </asp:Repeater>
            </td>
        </tr>
    </table>
    <table style="margin-top: 20px" class="lineTable">
        <!-- 종족, 성별 설정 -->
        <tr>
            <td><%# WebApp.DisplayString.RaceGenderSetting %></td>
            <td>
                <asp:CheckBox runat="server" ID="AllRaceGender" Text='<%# WebApp.DisplayString.All %>' AutoPostBack="true" OnCheckedChanged="RaceGenderCheckAll_OnClick" />
                <asp:HiddenField runat="server" ID="RaceNo" Value='<%# (int)WebApp.CommonEnum.Race.Type.RT_COMMON %>' />
                <asp:HiddenField runat="server" ID="GenderNo" Value='<%# (int)WebApp.CommonEnum.Gender.Type.GT_COMMON %>' />
                <asp:Repeater runat="server" ID="RaceGenderCheckList">
				    <ItemTemplate>
					    <asp:CheckBox runat="server" ID="RaceGenderName" AutoPostBack="true" OnCheckedChanged="RaceGenderCheck_OnClick" Text='<%# Eval("RaceGenderName") %>' />
                        <asp:HiddenField runat="server" ID="RaceNo" Value='<%# Eval("RaceNo") %>' />
                        <asp:HiddenField runat="server" ID="GenderNo" Value='<%# Eval("GenderNo") %>' />
				    </ItemTemplate>
			    </asp:Repeater>
            </td>
        </tr>
    </table>
    <table style="margin-top: 20px" class="lineTable">
        <!-- 레벨 설정 -->
        <tr>
            <td><%# WebApp.DisplayString.SetLevel %></td>
            <td><asp:TextBox runat="server" ID="ConditionMinLevel"/> ~ <asp:TextBox runat="server" ID="ConditionMaxLevel"/></td>
        </tr>
    </table>
    <table style="margin-top: 20px" class="lineTable">
        <!-- 이벤트 ID -->
        <tr>
            <td><%# WebApp.DisplayString.EventTypeId %></td>
            <td><asp:DropDownList runat="server" ID="EventTypeId" DataTextField="Text" DataValueField="Value" OnSelectedIndexChanged="EventTypeId_SelectedIndexChanged" AutoPostBack="true" /></td>
        </tr>
    </table>
    <table style="margin-top: 20px" class="lineTable">
        <!-- 보상 ID -->
        <tr>
            <td><%# WebApp.DisplayString.EventRewardId %></td>
            <td><asp:DropDownList runat="server" ID="EventRewardId" DataTextField="Text" DataValueField="Value" /></td>
        </tr>
    </table>
    <table style="margin-top: 20px;margin-bottom: 20px" class="lineTable">
        <!-- 기간 설정 -->
        <tr>
            <td><%# WebApp.DisplayString.EventPeriod %></td>
            <td><asp:TextBox runat="server" Rows="1" ID="StartTime" type="date" onkeypress="return false;" onkeydown="return false;" /> ~ <asp:TextBox runat="server" Rows="1" ID="EndTime" type="date"  onkeypress="return false;" onkeydown="return false;" />
                <script type="text/javascript">
                    $(function () {
                        $('*[type=date]').appendDtpicker({ "minuteInterval": 1 });
                    });
                </script>
            </td>
        </tr>
    </table>

</asp:Content>
