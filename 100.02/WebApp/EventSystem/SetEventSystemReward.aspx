﻿<%@ Page Language="C#" MasterPageFile="~/AppCode/WebAppPage.Master" AutoEventWireup="true" CodeBehind="SetEventSystemReward.aspx.cs" Inherits="WebApp.EventSystem.SetEventSystemReward" %>


<asp:Content ID="Content1" ContentPlaceHolderID="mainContentHolder" runat="server">
     <form runat="server" id="form2">
        <table>
            <tr>
                <td colspan="2">
                    <asp:Panel runat="server" ID="ResultPanel" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:Panel runat="server" ID="ButtonPannel"/>
                </td>
            </tr>
        </table>
        <br />
        <br />
    </form>
</asp:Content>